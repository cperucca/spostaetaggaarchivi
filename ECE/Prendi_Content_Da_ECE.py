import os 

# -*- coding: utf-8 -*-

import logging
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import Helpers
import shutil
import glob
import json
from datetime import datetime, timedelta
import pySpin
import ast

import Put_Content_In_ECE as Put_Content_In_ECE

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

logger = logging.getLogger('pyECE')

for entry in namespaces:
	#logger.debug(entry, namespaces[entry])
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry


def Dump_ET_Link ( id, filename ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(id)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	tree = ET.parse(result)
	tree.write(filename)

	return



def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	return

def Put_Link( link, filename ):
	
	logger.debug('------------------------ inizia Put_Link -------------- ')

        base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
        file = open(filename)
        dati = file.read()

        request = urllib2.Request(link, data=dati)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'PUT'
        result = urllib2.urlopen(request)

        #logger.debug(result.read())
        return
        xml = minidom.parse(result)
        fout = codecs.open(filename, 'w', 'utf-8')
        fout.write( xml.toxml() )
        fout.close()


        return

def Put_IdProd( idx, filename ):

	logger.debug('------------------------ inizia Put_IdProd Prendi -------------- ')
	try:
		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		link = prod_ece  + str(idx)
		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.info('Put_Id : ' + str(result.getcode()))
		
	
	except Exception as e:
		logger.error('PROBLEMI in Put_Id : ' + str(e))
		logger.error('NON HO FATTO  : ' + str(idX) )
		logger.error('PROBLEMI in Put_Id : ' + str(e))
		print 'PROBLEMI in Put_Id : ' + str(e)
		print 'NON HO FATTO  : ' + str(idX) 
		print 'PROBLEMI in Put_Id : ' + str(e)
		return False

	logger.debug('------------------------ finisce Put_IdProd Prendi -------------- ')
        return True




def Put_Id( idx, filename ):

	logger.debug('------------------------ inizia Put_Id -------------- ')
	try:
		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.debug('Put_Id' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print 'PROBLEMI in Put_Id : ' + str(e)
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
        return True


#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/22742", 'jonas_section')
#exit(0)


#Dump_Link("http://localhost/webservice/escenic/section/ROOT/subsections", 'jonas_1')

#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8730301", '8730301.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8709154", '8709154.xml')
#Put_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '../_cambiamento_')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '8568391.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8730301", '8730301.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8675248", 'gallery_8675248.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8948557", 'gallery_8948557.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/9013267", '9013267.xml')
#Dump_ET_Link('8845033', '8845033_et.xml')
#exit(0)

#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/7963/subsections", 'jonas_tvs')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/8599/subsections", 'jonas_tvsplayer')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/4/subsections", 'jonas_rsi')


def Dump_Rows ( link, filename ):

	logger.debug(' in Dump Rows ')

	#base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)
	
	tree = ET.parse(result)


	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	logger.debug('  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page))

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result_xml = urllib2.urlopen(request)
	xml = minidom.parse(result_xml)
	fout = codecs.open(filename, 'a', 'utf-8')
	fout.write( xml.toxml() )
	if  totresult > items_per_page:
		#logger.debug(' giro sui next e prev ')
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			link_next = PrendiLink(tree, 'next')
			#logger.debug(link_next)
			request = urllib2.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			tree = ET.parse( result )
			request = urllib2.Request(link_next)
			request.add_header("Authorization", "Basic %s" % base64string)
			result_xml = urllib2.urlopen(request)
			xml = minidom.parse(result_xml)
			fout = codecs.open(filename, 'a', 'utf-8')
			fout.write( xml.toxml() )
			fout.close()

	exit(0)

	return

def PrendiState( entry , rel):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['name'])
		return lis.attrib['name']
	return None

def PrendiEdited_Time( entry , rel):

	list =  entry.findall(namespaces['app'] + "edited/" )
	
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis.text)
		#logger.debug(lis.attrib['name'])
		return lis.text

	return None

def PrendiSection( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis.attrib['title']
	return None


def aggiungiSection( entry , rel, value, title):

	list =  entry.findall(namespaces['metadata'] + "publication")
	
	logger.debug(list)
	print list

	if len(list) < 0 :
		return entry
	pub = list[0]
	sections = pub.findall(namespaces['atom'] + "link")
	

	for idx , lis in enumerate(sections):
		#logger.debug(idx, lis)
		print idx, lis
		print lis.attrib['rel']
		# qui se trovo gia quella di  value 
		# return entry
	# altrimenti creo un nuovo nodo
	lis = ET.Element(namespaces['atom'] + "link" )
	lis.attrib['href'] = os.environ['ECE_SECTION'] + value
	lis.attrib['title'] = title
	lis.attrib['rel'] = "http://www.vizrt.com/types/relation/section"
	lis.attrib['type'] = "application/atom+xml; type=entry"	
	pub.insert( 1, lis )
	return entry

	return None


def CambiaSection( entry , rel, value, title):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(list)
	#print(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#print(lis.attrib['href'])
			#print(lis.attrib['title'])
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			lis.attrib['href'] = os.environ['ECE_SECTION'] + value
			lis.attrib['title'] = title
			return entry
	return None

def RimuoviArchiviSection( entry, rel, value):

	toDel = []

	list =  entry.findall(namespaces['metadata'] + "publication")
	
	#logger.debug(list)
	if len(list) < 1 :
		return entry

	publi = list[0]
	
	list = list[0].findall(namespaces['atom'] + "link")
	print list

	for lis in list:
		logger.debug(lis)
		print lis
		print lis.attrib['href']
		print lis.attrib['title']
		if rel in lis.attrib['rel']:
			logger.debug(lis.attrib['href'])
			logger.debug(lis.attrib['title'])
			if value in lis.attrib['title']:
				# ho trovato value in rel e devo rimuoverla
				print 'trovato ' + value
				toDel.append( lis )


				#list.remove(lis)

	for lis in toDel:
		print 'cancello'
		publi.remove(lis)
				
	return entry

def ControllaHomeSection( entry , rel, value):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#print(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			print(lis.attrib['href'])
			print(lis.attrib['title'])
			if value in lis.attrib['title']:
				return True
	return False




def PrendiSezione( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis.attrib['title']
	return None

def PrendiTipo( entry ):

        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                #logger.debug(lis)
                payload =  lis.findall(namespaces['vdf'] + "payload")
		tipo = payload[0].attrib['model'].split('/')[-1]
		return tipo

	return None


def PrendiPayload( entry , rel):

	list =  entry.findall(namespaces['atom'] + "payload")
	
	logger.debug(len(list))

	for idx , lis in enumerate(list):
		logger.debug(idx, lis)
		logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			logger.debug(lis.attrib['href'])
			logger.debug(lis.attrib['title'])
			#return lis.attrib['href']
	return None


def PrendiLinkRelated( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis
			
	return None


def PrendiLinkRelatedId( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis.attrib['href'].split('/')[-1]
			
	return None

def Trova_KeyFrame( Id_xx ):


	logger.debug(' ------------------- INIZIO Trova_KeyFrame ------------------------- ')
	
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)

		id_keyframe =  PrendiLinkKeyframe( tree , 'related')




	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None']

	logger.debug(' ------------------- FINE Trova_KeyFrame ------------------------- ')
	return id_keyframe

def PrendiLinkKeyframe( entry , rel):
	

        list =  entry.findall(namespaces['atom'] + "link")

        #logger.debug(len(list))
	id_keyframe = ''

        for idx , lis in enumerate(list):
                #logger.debug(idx, lis)
                #logger.debug(lis.attrib['rel'])
                if rel in lis.attrib['rel']:
                        #for idx2, attr in enumerate(lis.attrib):
                                #logger.debug(idx2, attr)
                        logger.debug(lis.attrib[namespaces['metadata'] + 'group'])
                        group =  lis.attrib[namespaces['metadata'] + 'group']
			if 'KEYFRAMES' in group:
				# ho trovato un keyframe
				_payload = lis.findall( namespaces['vdf'] + "payload")
				logger.debug(_payload[0].attrib['model'])
				if 'EDITORIAL' in group:
				
					logger.debug(lis.attrib['href'])
					logger.debug(lis.attrib['title'])
					#  se editorial ritorno con ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# se normale me lo ricordo ma cerco editorial
					id_keyframe = lis.attrib['href'].split('/')[-1] 

        return id_keyframe

def PrendiListaKeyframe( entry , rel):
	

        list =  entry.findall(namespaces['atom'] + "link")
	result = []

        #logger.debug(len(list))
	id_keyframe = ''

        for idx , lis in enumerate(list):
                #logger.debug(idx, lis)
                #logger.debug(lis.attrib['rel'])
                if rel in lis.attrib['rel']:
                        #for idx2, attr in enumerate(lis.attrib):
                                #logger.debug(idx2, attr)
                        logger.debug(lis.attrib[namespaces['metadata'] + 'group'])
                        group =  lis.attrib[namespaces['metadata'] + 'group']
			if 'KEYFRAMES' in group:
				# ho trovato un keyframe
				print lis.attrib[namespaces['dcterms'] + 'identifier' ] 
				result.append(lis.attrib[namespaces['dcterms'] + 'identifier' ])

        return result



def PrendiLinkLeadId( entry , rel):
	
	# al momento prende sempre e solo la prima
	# e assume che sia una immagine

        list =  entry.findall(namespaces['atom'] + "link")

        #logger.debug(len(list))

        for idx , lis in enumerate(list):
                #logger.debug(idx, lis)
                #logger.debug(lis.attrib['rel'])
                if rel in lis.attrib['rel']:
                        #for idx2, attr in enumerate(lis.attrib):
                                #logger.debug(idx2, attr)
                        #logger.debug(lis.attrib[namespaces['metadata'] + 'group'])
                        group =  lis.attrib[namespaces['metadata'] + 'group']
			if 'lead' in group:
				# ho trovato una relazione in lead
				# devo verificare che sia una picture
				_payload = lis.findall( namespaces['vdf'] + "payload")
				#logger.debug(_payload[0].attrib['model'])
				if 'picture' in _payload[0].attrib['model']:
				
					#logger.debug(lis.attrib['href'])
					#logger.debug(lis.attrib['title'])
					# e qui ne prendo ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# devo prendere la immagine del video
					# altrimenti il video stesso
					logger.debug('Trovato VIDEO ? ')
					# devo prendere l id della picture che mi interessa
					# poi dovro prendere il valore del video stesso e buttarlo
					# su
					id_keyframe = Trova_KeyFrame( lis.attrib['href'].split('/')[-1] )
					return id_keyframe

        return None


def PrendiLink( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			
			return lis.attrib['href']
	return None

def PrendiField( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None

def PrendiFieldSocial( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None


def prendiEntryTags( entry , rel):

	rel = 'com.escenic.tags'

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "list") is None) :
						return fiel

	return None




def processElem(elem):
    if elem.text is not None:
        logger.debug(elem.text)
    for child in elem:
        processElem(child)
        if child.tail is not None:
	    logger.debug(' scrivo la coda ' )
            logger.debug(child.tail)


def PrendiFieldTest( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				logger.debug(idx, fiel)
				logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					logger.debug(' trovato ' + rel )
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						val =  fiel.find(namespaces['vdf'] + "value")
						processElem( val )
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return 'None'

	return 'None'


def RiempiListaStream( entry ):
	
	result = []

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT RiempiListaStream -------------- ')
	_list = entry.findall(namespaces['vdf'] + "list")
	logger.debug(_list)
	if len( _list ) > 0 :
		_list = _list[0]
	else:
		return []

	payload =  _list.findall(namespaces['vdf'] + "payload")
	for pay in payload:
		fields = pay.findall(namespaces['vdf'] + "field")
		#logger.debug(fields)

		for idx , fiel in enumerate(fields):
			#logger.debug(idx, fiel)
			#logger.debug(fiel.attrib['name'])
			if not (fiel.find(namespaces['vdf'] + "value") is None) :
				#logger.debug( fiel.find(namespaces['vdf'] + "value").text)
				result.append( fiel.find(namespaces['vdf'] + "value").text )

	#logger.debug(result)
	return result
	

def PrendiStream( entry, ref ):


	stream = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if ref in fiel.attrib['name'] :
					
					#logger.debug(' trovato attr stream ' )
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						#logger.debug(' trovato stream ' )
						stream =  fiel.find(namespaces['vdf'] + "value").text
						return stream
					else:
						#logger.debug("trovato stream Nullo"  )
						#logger.debug("lo setto sul valore 12"  )
						stream = "12"

	
	return stream

	
def PrendiListaStream( entry ,rel ):
	
	
	lista_stream = []

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name'] and not 'audio' in fiel.attrib['name'] :
					

					lista_stream = RiempiListaStream( fiel )
					break
				
	#logger.debug(' lista stream ' )
	#logger.debug(lista_stream)

	return lista_stream


def PrendiContinousLive( entry ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT PrendiContinousLive -------------- ')

	continuous_live = 'false'
	continuous_live = PrendiField( entry, 'continuous_live')
	if continuous_live is None or 'false' in continuous_live:
		continuous_live = 'false'
	else:
		if 'true' in continuous_live:
			continuous_live = 'true'
		
	logger.debug(' continuous_live : ' + str(continuous_live))

	logger.debug(' ---------- FINE PrendiContinousLive -------------- ')
	return continuous_live

def PrendiMinNotifica( accountJson ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT PrendiMin Notifica -------------- ')
	# metto result come data tra un anno
	minData = datetime.now()  + timedelta( days=365 )
	result = ''

	platforms = ['fb' , 'yt' ]
	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		if len( account ) < 1 :
			continue
		logger.debug( account )
		for key,valueUp in account.iteritems():
			#print key, valueUp
			value = valueUp['SocialDetails']
			if ( not 'notificato' in value or not value['notificato'] ) and  minData > datetime.strptime( value['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
				minData =  datetime.strptime( value['notificaTime'] ,  '%Y-%m-%dT%H:%M:%SZ' )
				#print minData
				result = value['notificaTime']
	return result



def SistemaEncoders( accountJson ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Sistema_Encoder -------------- ')

	platforms = ['fb' , 'yt' ]
	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		for key,valueUp in account.iteritems():
			# print key, valueUp
			value = valueUp['SocialDetails']

			if 'encoder' not in value:
				# sono LA1
				logger.debug('Incoming Stream : la1')
				value['encoder'] = '12'

			value['encoders'] = [ False, value['encoder'] ]

			if '12' in value['encoder']:
				# sono LA1
				logger.debug('Incoming Stream : la1')
				enco = 'la1'
			else:
				if '13' in value['encoder'] :
					# sono LA2
					logger.debug('Incoming Stream : la2')
					enco = 'la2'
				else:
					logger.debug('Incoming Stream : enc' + value['encoder'] )
					enco = 'enc' + value['encoder']
			
			value['incoming_stream'] = enco

			if ( 'useOtherEncoder' in value )  and ('true' in value['useOtherEncoder']):
				# sono usato encoder extra
				value['encoders'] = [ True, value['extraStream'] ]
				value['incoming_stream'] = 'custom'

		logger.debug(' ---------- FINE Sistema_Encoder -------------- ')

	return accountJson 




def PrendiEncoders( entry ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Prendi_Encoder -------------- ')
	# la prendo comunque al massimo sara vuota
	stream = PrendiStream( entry , 'stream' )
	logger.debug( 'Stream : ' + stream )
	# e inizializzo la lista_extra
	lista_extra = []


	# verifico se sono usato encoder extra
	customEncoder = PrendiField( entry, 'useOtherEncoder')
	if customEncoder is None or 'false' in customEncoder:
		customEncoder = False
	else:
		if 'true' in customEncoder:
			customEncoder = True
		
	if customEncoder:
		logger.debug(' prendo gli encoder extra')
		lista_extra = PrendiExtraStream( entry )
		logger.debug(lista_extra)
		
		return [ True, lista_extra ]
		

	#logger.debug(' stream : ' + str(stream))
	#logger.debug(' lista_extra : ' + str(lista_extra))

	logger.debug(' ---------- FINE Prendi_Encoder -------------- ')
	return [ False, stream ] 


def PrendiTimeCtrl( entry ):

	
	activation = PrendiActivation(entry, ' activation_time')
	#logger.debug('activation ' + activation)
	published = PrendiPublished( entry, 'updated')
	#logger.debug('published ' + published)
	expires = PrendiExpires( entry, 'expiration' )
	#logger.debug('expires ' + expires)

	return [ activation, published, expires ]

def PrendiPublished( entry , name):

	list =  entry.findall(namespaces['atom'] + name )
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None


def removeExpires( entry , value):

	list =  entry.findall(namespaces['age'] + "expires")
	for lis in list:
		logger.debug(lis.text)
		root = entry.getroot()
		root.remove(lis)


	return entry



def cambiaExpires( entry , value):

	list =  entry.findall(namespaces['age'] + "expires")
	for lis in list:
		logger.debug(lis.text)
		lis.text = value

	return entry




def PrendiExpires( entry , rel):

	list =  entry.findall(namespaces['age'] + "expires")
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None



def PrendiActivation( entry , rel):

	list =  entry.findall(namespaces['dcterms'] + "available")
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None


def CambiaBodyFile( filein ):

	replacements = {'<html:':'<', '</html:':'</','xmlns:html=':'xmlns=' }

	lines = []
	with open(filein) as infile:
	    for line in infile:
		for src, target in replacements.iteritems():
		    line = line.replace(src, target)
		lines.append(line)
	infile.close()
	with open(filein, 'w') as outfile:
	    for line in lines:
		outfile.write(line)	
	outfile.close()
	

def CambiaState( entry , value):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	#logger.debug(' CambiaState ------ ')
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		lis.text = value
		return entry

	#logger.debug(' CambiaState ------ ')
	return None

def CambiaChannel( entry, val ):

	# questa cambia il channel nel field channel
	rel = 'channel'
	# da https://coredelivery.rsi.ch//subt_web/ 
	# a https://coredelivery.rsi.ch/subtitles//subt_web/
        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                payload =  lis.findall(namespaces['vdf'] + "payload")
                for pay in payload:
                        fields = pay.findall(namespaces['vdf'] + "field")
                        #logger.debug(fields)

                        for idx , fiel in enumerate(fields):
                                #logger.debug(idx, fiel)
                                #logger.debug(fiel.attrib['name'])
                                if rel in fiel.attrib['name']:
                                        if fiel.find(namespaces['vdf'] + "value") is None:
						print('non ha valore nel campo ' + rel + ' ESCO !!')
						exit(0)
					#oldVal = fiel.find(namespaces['vdf'] + "value").text
					#value = oldVal.replace('coredelivery.rsi.ch//subt_web','coredelivery.rsi.ch/subtitles/subt_web')
					#print( oldVal, value )
                                        fiel.find(namespaces['vdf'] + "value").text = val
					#logger.debug(fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text)
                                        return entry

        return entry





def CambiaSubtitleUrl( entry ):

	# questa cambia il path nel field subtitleUrl
	rel = 'subtitleUrl'
	# da https://coredelivery.rsi.ch//subt_web/ 
	# a https://coredelivery.rsi.ch/subtitles//subt_web/
        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                payload =  lis.findall(namespaces['vdf'] + "payload")
                for pay in payload:
                        fields = pay.findall(namespaces['vdf'] + "field")
                        #logger.debug(fields)

                        for idx , fiel in enumerate(fields):
                                #logger.debug(idx, fiel)
                                #logger.debug(fiel.attrib['name'])
                                if rel in fiel.attrib['name']:
                                        if fiel.find(namespaces['vdf'] + "value") is None:
						print('non ha valore nel campo ' + rel + ' ESCO !!')
						exit(0)
					oldVal = fiel.find(namespaces['vdf'] + "value").text
					value = oldVal.replace('coredelivery.rsi.ch//subt_web','coredelivery.rsi.ch/subtitles/subt_web')
					print( oldVal, value )
                                        fiel.find(namespaces['vdf'] + "value").text = value
					#logger.debug(fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text)
                                        return entry

        return entry



def CambiaField( entry , rel, value):

        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                payload =  lis.findall(namespaces['vdf'] + "payload")
                for pay in payload:
                        fields = pay.findall(namespaces['vdf'] + "field")
                        #logger.debug(fields)

                        for idx , fiel in enumerate(fields):
                                #logger.debug(idx, fiel)
                                #logger.debug(fiel.attrib['name'])
                                if rel in fiel.attrib['name']:
                                        if fiel.find(namespaces['vdf'] + "value") is None:
                                                fiel.append(ET.Element(namespaces['vdf'] + "value"))
                                        fiel.find(namespaces['vdf'] + "value").text = value
					#logger.debug(fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text)
                                        return entry

        return entry

def Change_Item_Content( title, description, alttext, leadtext, tree ):

        if leadtext is not None:
                # allora devo riempirlo con il leadtext
                logger.debug("leadtext is not None = " + leadtext)
                CambiaField( tree, "alttext", leadtext )
                return tree
        #logger.debug("alttext vuoto")
        else:
                if description is not None:
                        # allora devo riempirlo con la descrizione
                        logger.debug("description is not None = " + description)
                        CambiaField( tree, "alttext", description )
                        return tree
                else:
                        # allora devo riempirlo con il titolo
                        logger.debug("riempio con il titolo = " + title)
                        CambiaField( tree, "alttext", title )
                        return tree


        return tree


def Get_Item_Content( Id_xx ):

        base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

        link = os.environ['ECE_SERVER']  + str(Id_xx)

        request = urllib2.Request(link)

        request.add_header("Authorization", "Basic %s" % base64string)
        try:
                result = urllib2.urlopen(request)
                tree = ET.parse(result)
                #tree.write('x.xml')
                #ET.dump(tree)

                tipo = PrendiTipo( tree )

		logger.debug(tipo)

		return


                if 'keyframe' in tipo:
                        #se e' un keyframe si chiama name
                        campo_titolo = "name"
                else:
                        #altrimenti si chiama title
                        campo_titolo = "title"
                title =  PrendiField( tree, campo_titolo)
                logger.debug(title)
                description =  PrendiField( tree, "description")
                logger.debug(description)
                alttext =  PrendiField( tree, "alttext")
                logger.debug(alttext)
                leadtext =  PrendiField( tree, "leadtext")
                logger.debug(leadtext)
                #programmeId =  PrendiField( tree, "programmeId")
                #logger.debug(programmeId)

                result.close()
        except urllib2.HTTPError, e:
                logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
                return [ u'None' , u'None', u'None']


        return [ title , description, alttext, leadtext, tree ]


def Prendi_Url( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('prendi_URL_ok.xml')
		#ET.dump(tree)
		Url =  PrendiLink( tree, "alternate")
		#print'URL = ' +  Url

		result.close()
	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	return Url

def Check_Data_Range( adesso, Id_xx ):

	# questa server per verificare di essere nel range tra activation
	# e expiration 
	# verifico anche la publishing perche talvolta e piu alta della activation date

	[ activation, published, expires ] =  Prendi_Social_TimeCtrl(Id_xx)
	# logger.debug(activation, published, expires)

	data_limite = adesso
	active = False

	# logger.debug(' adesso = ' + str(adesso))

	# in caso non sia settata alcuna data di attivazione la Prendi_Social_TimeCtrl
	# mi torna None per il campo activate 
	if activation is None:
		# mettendo il seguente aassegnamento sono sicuro poi di confrontare
		# la data attuale con la data  di pubblicazione e dovrei passare sempre
		active = True
	else :
		data_limite = datetime.strptime( activation, '%Y-%m-%dT%X.000Z' )

	# logger.debug('data_limite = ' + str(data_limite))
	
	if adesso >= data_limite:	
		# logger.debug(' Data attuale nel Range ')
		return True
	
	# logger.debug(' Data attuale FUORI Range ')
	return False

	



def Prendi_Social_TimeCtrl( Id_xx ):
	
	flagDiRitorno = {}
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		timeCtrl = PrendiTimeCtrl( tree )

		result.close()
	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ None , None]
		
	
	return timeCtrl

def PrendiExtraStream( entry ):

        result = []
        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                #logger.debug(lis)
                payload =  lis.findall(namespaces['vdf'] + "payload")
                for pay in payload:
                        fields = pay.findall(namespaces['vdf'] + "field")
                        #logger.debug(fields)

                        for idx , fiel in enumerate(fields):
                                #logger.debug(idx, fiel)
                                #logger.debug(fiel.attrib['name'])
                                if 'extraStream' == fiel.attrib['name']:
                                        #logger.debug('passo di qui')
                                        # qui ho preso il field fb-livestreaming-account
                                        # e adesso devo prendere tutti gli account che ho messo
                                        _list = fiel.findall(namespaces['vdf'] + "list")
                                        if len(_list) > 0:
                                                _list = _list[0]
                                        else:
                                                return False
                                        stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
                                        #logger.debug('stre_payloads ')
                                        #logger.debug(stre_payloads)
                                        for stre in stre_payloads:
                                                #logger.debug('giro _payloads')
                                                #logger.debug(stre)
                                                stre_field = stre.find(namespaces['vdf'] + "field")
                                                #logger.debug(stre_field.attrib['name'])
                                                if 'extraStream' in stre_field.attrib['name']:
                                                        if not (stre_field.find(namespaces['vdf'] + "value") is None) :
                                                                #logger.debug(stre_field.find(namespaces['vdf'] + "value").text)
								result.append( stre_field.find(namespaces['vdf'] + "value").text )
                                                                #result.append(acc_field.find(namespaces['vdf'] + "value").text)

        return result




def VerificaExtraStream( entry ):

        result = []
        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                #logger.debug(lis)
                payload =  lis.findall(namespaces['vdf'] + "payload")
                for pay in payload:
                        fields = pay.findall(namespaces['vdf'] + "field")
                        #logger.debug(fields)

                        for idx , fiel in enumerate(fields):
                                #logger.debug(idx, fiel)
                                #logger.debug(fiel.attrib['name'])
                                if 'extraStream' == fiel.attrib['name']:
                                        #logger.debug('passo di qui')
                                        # qui ho preso il field fb-livestreaming-account
                                        # e adesso devo prendere tutti gli account che ho messo
                                        _list = fiel.findall(namespaces['vdf'] + "list")
                                        if len(_list) > 0:
                                                _list = _list[0]
                                        else:
                                                return False
                                        stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
                                        #logger.debug('stre_payloads ')
                                        #logger.debug(stre_payloads)
                                        for stre in stre_payloads:
                                                #logger.debug('giro _payloads')
                                                #logger.debug(stre)
                                                stre_field = stre.find(namespaces['vdf'] + "field")
                                                #logger.debug(stre_field.attrib['name'])
                                                if 'extraStream' in stre_field.attrib['name']:
                                                        if not (stre_field.find(namespaces['vdf'] + "value") is None) :
                                                                #logger.debug(stre_field.find(namespaces['vdf'] + "value").text)
                                                                if 'ch-lh.akamaihd' in stre_field.find(namespaces['vdf'] + "value").text:
                                                                        return True
                                                                #result.append(acc_field.find(namespaces['vdf'] + "value").text)

        return False



def VerificaGeo( entry ):

	result = []

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'stream' == fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) > 0 :
						_list = _list[0]
					else:
						return False
					stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
					#logger.debug('stre_payloads ' )
					#logger.debug(stre_payloads)
					for stre in stre_payloads:
						#logger.debug('giro _payloads')
						#logger.debug(stre)
						stre_field = stre.find(namespaces['vdf'] + "field")
						#logger.debug(stre_field.attrib['name'])
						if 'stream' in stre_field.attrib['name']:
							if not (stre_field.find(namespaces['vdf'] + "value") is None) :
								#logger.debug(stre_field.find(namespaces['vdf'] + "value").text)
								if 'ch-lh.akamaihd' in stre_field.find(namespaces['vdf'] + "value").text:
									#logger.debug( ' Ritorno GEO = True ' )
									return True
								#result.append(acc_field.find(namespaces['vdf'] + "value").text)	

	return False

def PrendiAccounts( entry , type ):

	result = {}
	account = {}
	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if type in fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) < 1 :
						return result
						
					else:

						# faccio cosi perche la lista e unica
						_list = _list[0]
						acc_payloads =  _list.findall(namespaces['vdf'] + "payload")
						
						#print ' acc_payloads lungo : ' + str( len ( acc_payloads))
						for acc in acc_payloads:
							#print '----------------------------giro _payloads'
							acc_fblivesocencchan = acc.find(namespaces['vdf'] + "field")
							acc_fields = acc_fblivesocencchan.findall(namespaces['vdf'] + "field")
							#print ' acc_payloads lungo : ' + str( len ( acc_payloads))
							
							for acc_f in acc_fields:
								if not (acc_f.find(namespaces['vdf'] + "value") is None) :
									#print  acc_f.attrib['name']  + ' = ' + acc_f.find(namespaces['vdf'] + "value").text
									account[ acc_f.attrib['name'] ] =  acc_f.find(namespaces['vdf'] + "value").text	

							result[ account['livesocialid']] =   {'SocialDetails' : account}
							account = {}
	return result

def PrendiSocialTime( entry ):


	start = ''
	end = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'socialStartTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						start =  fiel.find(namespaces['vdf'] + "value").text
	
				if 'socialEndTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						end =  fiel.find(namespaces['vdf'] + "value").text



	return [ start, end ]



def PrendiTime( entry ):


	start = ''
	end = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'startTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						start =  fiel.find(namespaces['vdf'] + "value").text
	
				if 'endTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						end =  fiel.find(namespaces['vdf'] + "value").text


	return [ start, end ]



def SistemaYtAccounts( lista_yt_accounts ):

	result = []
	#da ytl-account-rsi
	#devono diventare una lista cosi ['testare_e_bello', 'linea_rossa', 'patti_chiari', 'rsi_sport', 'rsi_news'] 
	for value in lista_yt_accounts:
		if value is not None:
			# e qui prendo solo la parte descrittiva della chiave dell account
			result.append(value.split('account-')[-1])


	return result

def SistemaTimeOld( Item_Json ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(" ---------------------- INIZIO SistemaTempo   ------------------ ")


	# prende il piu' basso tra i tempi di inizio e terminazione
	# prendendo in considerazione activation e starttime per inizio
	# e expiratione e endTime per la fine

	# dato che poi FB vuole il tempo timezonato aggiundo la timedifference
	delta = datetime.now() - datetime.utcnow()

	start = Item_Json['startTime']
	end = Item_Json['endTime']

	if ((Item_Json['notificaTime'] is None) or len(Item_Json['notificaTime'])< 1 ) :
		Item_Json['setNotifTime'] = False
		notif = datetime.strftime(datetime.strptime( Item_Json['published'], '%Y-%m-%dT%X.000Z' ) - delta,'%Y-%m-%dT%H:%M:%SZ' )
	else:
		Item_Json['setNotifTime'] = True
		notif = Item_Json['notificaTime']
		

	logger.debug(' sistema tempo : start, end, notifica ')
	logger.debug(start +' '+ end +' '+ notif)

	if (not (Item_Json['activation'] is None) ) :
		activa = datetime.strptime( Item_Json['activation'], '%Y-%m-%dT%X.000Z' )
		_tmp_start = datetime.strptime( Item_Json['startTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if activa > _tmp_start:
			start = datetime.strftime( Item_Json['activation'], '%Y-%m-%dT%H:%M:%SZ' )
	
	if (not (Item_Json['expiration'] is None) ) :
		expira = datetime.strptime( Item_Json['expiration'], '%Y-%m-%dT%X.000Z' )
		_tmp_end = datetime.strptime( Item_Json['endTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if  expira < _tmp_end:
			end = datetime.strftime( Item_Json['expiration'], '%Y-%m-%dT%H:%M:%SZ' )



	start = datetime.strptime( start,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	start = datetime.strftime( start, '%Y-%m-%dT%H:%M:%SZ')
	end = datetime.strptime( end,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	end = datetime.strftime( end, '%Y-%m-%dT%H:%M:%SZ')
	notif = datetime.strptime( notif,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	notif = datetime.strftime( notif, '%Y-%m-%dT%H:%M:%SZ')

	logger.debug(' sistema tempo : start, end, notifica ')
	logger.debug(start +' '+ end +' '+ notif)
	# ritorno nella forma
	# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
	
	logger.debug(" ---------------------- END SistemaTempo   ------------------ ")
	return [ start, end, notif ]


def SistemaTime( Item_Json ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(" ---------------------- INIZIO SistemaTempo   ------------------ ")
	logger.debug( Item_Json )

	# prende il piu' basso tra i tempi di inizio e terminazione
	# prendendo in considerazione activation e starttime per inizio
	# e expiratione e endTime per la fine

	# dato che poi FB vuole il tempo timezonato aggiundo la timedifference
	delta = datetime.now() - datetime.utcnow()
	logger.debug( ' delta = ' + str(delta))
	start = Item_Json['startTime']
	end = Item_Json['endTime']

	# sistemo il tempo di notifica dei vari account
	platforms = ['fb' , 'yt' ]
	for plat in platforms:
		account = Item_Json[ plat + '-livestreaming-account' ]
		for key,valueUp in account.iteritems():
			logger.debug( key )
			logger.debug( valueUp )
			value = valueUp['SocialDetails']
			logger.debug( value )

			if ((value['notificaTime'] is None) or len(value['notificaTime'])< 1 ) :
				value['setNotifTime'] = False
				# value['notificaTime'] = datetime.strftime(datetime.strptime( Item_Json['published'], '%Y-%m-%dT%X.000Z' ) - delta,'%Y-%m-%dT%H:%M:%SZ' )
				value['notificaTime'] = datetime.strftime(datetime.strptime( Item_Json['published'], '%Y-%m-%dT%X.000Z' ) + delta,'%Y-%m-%dT%H:%M:%SZ' )
			else:
				logger.debug( 'PASSSO DI QUI ! ' )
				logger.debug( value['notificaTime'] )
				value['setNotifTime'] = True
				value['notificaTime'] = datetime.strftime(datetime.strptime( value['notificaTime'], '%Y-%m-%dT%H:%M:%SZ' ) + delta,'%Y-%m-%dT%H:%M:%SZ' )
				logger.debug( value['notificaTime'] )

	if (not (Item_Json['activation'] is None) ) :
		activa = datetime.strptime( Item_Json['activation'], '%Y-%m-%dT%X.000Z' )
		_tmp_start = datetime.strptime( Item_Json['startTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if activa > _tmp_start:
			start = datetime.strftime( activa, '%Y-%m-%dT%H:%M:%SZ' )
	
	if (not (Item_Json['expiration'] is None) ) :
		expira = datetime.strptime( Item_Json['expiration'], '%Y-%m-%dT%X.000Z' )
		_tmp_end = datetime.strptime( Item_Json['endTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if  expira < _tmp_end:
			end = datetime.strftime( expira, '%Y-%m-%dT%H:%M:%SZ' )

	start = datetime.strptime( start,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	start = datetime.strftime( start, '%Y-%m-%dT%H:%M:%SZ')
	end = datetime.strptime( end,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	end = datetime.strftime( end, '%Y-%m-%dT%H:%M:%SZ')

	logger.debug(' sistema tempo : start, end ')
	logger.debug(start +' '+ end )
	# ritorno nella forma
	# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
	
	logger.debug(" ---------------------- END SistemaTempo   ------------------ ")
	return [ start, end ]

def SistemaEncodersOld( Tab_Json ):

	# devo guardare se hanno scelto encoder 12 o 13 che sono mappati su LA1 e LA2
	# altrimenti sono = custom

	
	logger = logging.getLogger('pySub_ECE')

	logger.debug( " ---------------------- END SistemaEncoders   ------------------ ")
	logger.debug( Tab_Json['encoders'])
	
	enco  = Tab_Json['encoders']
	logger.debug('Encoders = ' + str( enco))

	if enco[0] : 
		logger.debug('Incoming Stream : custom')
		return 'custom'

	if '12' in enco:
		# sono LA1
		logger.debug('Incoming Stream : la1')
		return 'la1'
	else:
		if '13' in enco:
			# sono LA2
			logger.debug('Incoming Stream : la2')
			return 'la2'
		else:
			logger.debug('Incoming Stream : enc' + enco[-1] )
			return 'enc' + enco[-1]
	

	return Null

def SistemaResto( accountJson ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT SistemaResto -------------- ')

	platforms = ['fb' , 'yt' ]

	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		for key,valueUp in account.iteritems():
			#print key, valueUp
			value = valueUp['SocialDetails']

			if 'isGeoBlocked' not in value or value['isGeoBlocked'] is None :
				value['isGeoBlocked'] = 'false'
			if 'continuousLive' not in value or value['continuousLive'] is None :
				value['continuousLive'] = 'false'

			if 'notificato' not in value or value['notificato'] is None :
				value['notificato'] = False


	logger.debug(' ---------- FINE  SistemaResto -------------- ')

	return accountJson


def SistemaTitolo( accountJson ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT SistemaTitolo -------------- ')

	platforms = ['fb' , 'yt' ]

	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		for key,valueUp in account.iteritems():
			#print key, valueUp
			value = valueUp['SocialDetails']

			if 'videoTitle' not in value or value['videoTitle'] is None or len( value['videoTitle'] ) < 1 :
				# devo mettere il titolo dell Evento
				if not ( accountJson['title'] is None ) :
					value['videoTitle'] = accountJson['title'] 
				else:
					value['videoTitle'] = ''


			if 'videoDesc' not in value or value['videoDesc'] is None or len( value['videoDesc'] ) < 1 :
				# devo mettere il titolo dell Evento
				if not ( accountJson['subtitle'] is None ) :
					value['videoDesc'] = accountJson['subtitle'] 
				else:
					value['videoDesc'] = ''

	logger.debug(' ---------- FINE  SistemaTitolo -------------- ')

	return accountJson

def RemoveLiveId( socialAccounts, liveId ) :

	result = socialAccounts
	# devo cancellare il liveId richiesto 
	# dopo aver verificato che non ci siano VideoDetails o WowzaDetails
	# e se ci sono cancellarli  
	value = socialAccounts[ liveId ]
	if 'VideoDetails' in value:
		# CLAD TODO
		logger.debug( ' Dovrei spegnere il video di : ' + liveId  + ' e cancellarne la entry ')
		# e per spegnerlo magari far passare una lista indietro di entry da spegnere 
		# al Controlla_Flag_Social ?
		# o magari solo flaggare il socialAccount anziche cancellarlo e 
		# TBD
	if 'WowzaDetails' in value:
		# CLAD TODO
		logger.debug( ' Dovrei spegnere il wowza di : ' + liveId  + ' e cancellarne la entry ')
	
		
	return result

def MergeSocialFlags( socialNew, socialOld ) :

	logger.debug(' ---------- INIT  MergeSocialFlags -------------- ')
	logger.debug(' socialNew : ')
	logger.debug(socialNew)
	logger.debug(' socialOld : ')
	logger.debug(socialOld)

	# se non 'e ancora stata inizializzata cioe non arriva dal DB
	# non devo rinfrescarla ma settarla
	if len( socialOld ) < 1 :
		result = socialNew
		return result

	# in caso contrario devo passare al merge
	# il cui compito e' aggiornare i valori dele social  Flags
	# che arrivano da ECE senza cancellare gli eventuali valori
	# di video o wowza

	
	# aggiungo verifica che non sia stata cancellata una entry dalla scheda
	# quindi giro su quello vecchio e verifico che ci siano TUTTE le entry 
	# aka livesocialid
	for key,value in socialOld.iteritems(): 
		if 'livestreaming-account' in key:
			new_accounts = {}
			# siamo nel dict dei broadcast di fb o yt
			# quindi in fb-livestreaming-account oppure
			# yt-livestreaming-account
			for liveId, detailsvalues in value.iteritems():
				# qui in liveId ho gli id unici che devo cercare nel socialNew
				if not liveId in socialNew[ key ]:
					# non trovato in quello nuovo quindi lo hanno tolto
					# devo cancellarlo da quello vecchio 
					logger.debug( ' hanno cancellato -> ' + liveId )
					RemoveLiveId( value , liveId )
				else:
					new_accounts[ liveId ] = value[ liveId ]

			socialOld[ key ] = new_accounts
					
	#logger.debug( ' new Accounts : ' + str(socialOld[  key ] ))
					
	result = socialOld
	#print 'socialNew : ' + str(socialNew)
	#print 'socialOld : ' + str(socialOld)
	# DEBUG CLAD  da verificare

	
	for key,value in socialNew.iteritems(): 
		if 'livestreaming-account' in key:
			# siamo nel dict dei broadcast di fb o yt
			for broad,broadValue in value.iteritems():
				logger.debug(' chiave dell account : ' + broad)
				logger.debug(' value : ' + str( broadValue ) )
				# devo trovare il posto giusto di quella vecchia
				# se non ha ancora nulla gli copia dentro la chiave 
				# come se niente fosse 
				if not broad in result[key]:
					result[key][broad]  = broadValue
				else:
					
					result[ key ][broad]['SocialDetails'] = broadValue['SocialDetails'] 
			continue
		result[ key ] = value

	logger.debug(' ---------- END MergeSocialFlags -------------- ')
	logger.debug(result)
	logger.debug(' ---------- END MergeSocialFlags -------------- ')


	return result

def prendiExpiration( tree ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT prendiExpiration -------------- ')
	Tab_Json = {}
	try:
	
		# Prendo anche  lo stato dell asset per sapere se e' stato 
		# spubblicato
		Tab_Json['state'] = PrendiState( tree , 'dummy')

		# e adesso prendo lo start e end time
		[ Tab_Json['startTime'], Tab_Json['endTime'] ] = PrendiTime( tree )

		# e adesso prendo lo start e end time
		[ Tab_Json['socialStartTime'], Tab_Json['socialEndTime'] ] = PrendiSocialTime( tree )

		# e adesso prendo i check times = activation expire ...
		[ Tab_Json['activation'], Tab_Json['published'], Tab_Json['expiration']] = PrendiTimeCtrl( tree )

		logger.debug('ID : ' + str(Id_xx) + ' = ' +  str(Tab_Json))
		logger.debug(' ---------- FINE prendiExpiration -------------- ')
		result.close()
	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' ---------- FINE prendiExpiration -------------- ')
		return [ u'None' , u'None', u'None']
	return Tab_Json




def Prendi_Flag_Social_New( Id_xx ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Prendi_Flag_Social -------------- ')
	lista_value_social = ['title','subtitle']


	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
	#link = os.environ['ECE_SERVER']  + str(Id_xx)
	# cambiato per andare a prendere pub2 senza pwd
	print os.environ['ECE_SERVER']
	link = os.environ['ECE_SERVER']  + str(Id_xx)
	print link
	#print 'Clad'
	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)

	Tab_Json = {}
	try:
	
		result = urllib2.urlopen(request)
		#print result.read() 

		#tree = ET.parse(result)
		#tree.write('1011.xml')
		#ET.dump(tree)
		#exit(0)

		for val in lista_value_social:
			Tab_Json[val] =  PrendiField( tree, val)

		# Prendo anche  lo stato dell asset per sapere se e' stato 
		# spubblicato
		Tab_Json['state'] = PrendiState( tree , 'dummy')

		# e adesso prendo lo start e end time
		[ Tab_Json['startTime'], Tab_Json['endTime'] ] = PrendiTime( tree )

		# e adesso prendo lo start e end time
		[ Tab_Json['socialStartTime'], Tab_Json['socialEndTime'] ] = PrendiSocialTime( tree )

		if len(Tab_Json['socialStartTime']) > 1 :
			logger.debug( 'setto Start = SocialStart' )
			Tab_Json['startTime'] = Tab_Json['socialStartTime']

		if len(Tab_Json['socialEndTime']) > 1 :
			logger.debug( 'setto End = SocialEnd' )
			Tab_Json['endTime'] = Tab_Json['socialEndTime']

		# e adesso prendo i check times = activation expire ...
		[ Tab_Json['activation'], Tab_Json['published'], Tab_Json['expiration']] = PrendiTimeCtrl( tree )



		# e adesso prendo gli account fi facebook a cui spedirlo
		Tab_Json['fb-livestreaming-account'] = PrendiAccounts( tree , 'fblivesocencchan')


		# e adesso prendo gli account fi facebook a cui spedirlo
		# qui sistemo gli account di YT
		Tab_Json['yt-livestreaming-account'] = PrendiAccounts( tree , 'ioutubelivesocencchan')


		# sistemo i tempi per avere t_start e t_end
		[ Tab_Json['t_start'], Tab_Json['t_end'] ] = SistemaTime( Tab_Json )

		# sistemo gli encoder
		Tab_Json  = SistemaEncoders( Tab_Json )


		Tab_Json = SistemaTitolo( Tab_Json )
		Tab_Json = SistemaResto( Tab_Json )


		logger.debug('ID : ' + str(Id_xx) + ' = ' +  str(Tab_Json))
		logger.debug(' ---------- FINE Prendi_Flag_Social -------------- ')
		result.close()
	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' ---------- FINE Prendi_Flag_Social -------------- ')
		return [ u'None' , u'None', u'None']
	return Tab_Json


def Prendi_Section_Parameters( Id_xx ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Prendi_Section_Parameters -------------- ')
	lista_value_social = [ 'title','subtitle', 'fbVideoTitle', 'fbVideoDesc', 'ytVideoTitle','ytVideoDesc' ]


	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
	#link = os.environ['ECE_SERVER']  + str(Id_xx)
	# cambiato per andare a prendere pub2 senza pwd
	print os.environ['ECE_SECTION']

	link = os.environ['ECE_SECTION']  + str(Id_xx)
	logger.debug( link )
	#print 'Clad'
	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)

	num_posts = 0
	account = ''
	nomefile = ''
	path = ''

	try:
	
		result = urllib2.urlopen(request)
		#print result.read() 

		tree = ET.parse(result)
		#tree.write('22742.xml')
		#ET.dump(tree)
		print 'scritto'
		#return
		parametri = PrendiParam( tree )
		print parametri
		parametri = parametri.splitlines()
		for para in parametri:
			if 'mostSocial.account' in para:
				print 'trovato account'
				account = para.split('=')[-1]
			if 'mostSocial.num_posts' in para:
				print 'trovato num_posts'
				num_posts = para.split('=')[-1]
			if 'mostSocial.nomeFile' in para:
				print 'trovato num_posts'
				nomefile = para.split('=')[-1]
			if 'mostSocial.path' in para:
				print 'trovato num_posts'
				path = para.split('=')[-1]
		
		

		logger.debug(' ---------- FINE Prendi_Section_Parameters -------------- ')
		result.close()
	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' ---------- FINE Prendi_Section_Parameters -------------- ')

		return { u'num_posts' : u'None' , u'account' : u'None', u'nomefile': u'None', u'path': u'None'}
		
	
	return { u'num_posts' : num_posts , u'account' : account , u'nomefile' : nomefile, u'path' : path }


def PrendiParam( entry ):

	rel = 'sectionParameters'

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)
			#print fields

			for idx , fiel in enumerate(fields):
				#print idx, fiel
				#print fiel.attrib['name']
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None







def Prendi_Altro_Content( Id_xx ):

	logger.debug(' INIT Prendi_Altro_Content ')
	
	contentDiRitorno = {}
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#logger.debug(ET._namespace_map)
		#tree.write('rimpingua_content_con_img.xml')
		#ET.dump(tree)
		#logger.debug(key)

		# come fatto qui sotto posso prendere tutti i fields
		title =  PrendiField( tree, 'title' )
		subtitle = PrendiField( tree, 'subtitle' )
		leadtext = PrendiField( tree, 'leadtext' )
		logger.debug(title, subtitle , leadtext)
		# e title e subtitle li metto direttamente al posto giusto

		#body =   PrendiFieldTest( tree, 'body')
		#logger.debug(body)
		#logger.debug(' fine body ')


		#logger.debug(PrendiField( tree, 'leadtext' ))
		#logger.debug(PrendiLinkRelatedId( tree, 'related'))
		#logger.debug(PrendiLinkLeadId( tree, 'related'))
		
		tipo = PrendiTipo( tree )
		if 'short' in tipo:
			link_alla_url = 'http://www.rsi.ch/web/ultimora/?a=' + Id_xx
		else:
			#link_alla_url = Prendi_Url( Id_xx )
			link_alla_url = 'http://www.rsi.ch/g/' + Id_xx

		link_alla_url = link_alla_url.replace('publishing','www')

		rel_id = PrendiLinkLeadId( tree, 'related')
		if not( rel_id is None):

	
			'''

			# eventualmente per prendere la caption
			related_content = PrendiLinkRelated( tree, 'related')

			lista =  related_content.findall(namespaces['vdf'] + "payload")
			logger.debug(len(lista))
			for lis in lista:
				logger.debug(lis.attrib)
			logger.debug(related_content)
			exit(0)

			''' 
			caption = ''




			url = 'https://www.rsi.ch/rsi-api/resize/image/BASE_FREE/' + rel_id
			contentDiRitorno = { 'link' : link_alla_url,
					'picture':url,
					'name' : title, 
					'description':subtitle,
					'caption':caption}
		else:
			url = None
			contentDiRitorno = { 'link' : link_alla_url,
					'picture':url,
					'name' : title, 
					'description':subtitle,
					'caption':''}
		
		result.close()
	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None']
		
	
	logger.debug(contentDiRitorno)
	return contentDiRitorno 

def CambiaDurationProd( Id_xx , duration ):
	
		
	logger.debug(' --------------------------- init CambiaDurationProd ----------------------')
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)
	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)
	logger.debug(' link = ' + link )

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('2748631.xml')
		# esempio di modifica del file xml 
		#tmp_tree =  CambiaField( tree, "duration", '444.444' )
		#tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('2748631_cambiato.xml')
		#exit(0)
		
		#ET.dump(tree)

		# come fatto qui sotto posso prendere tutti i fields
		tmp_tree =  CambiaField( tree, 'duration', duration )
		tree = tmp_tree

		#logger.debug('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine CambiaDurationProd ----------------------')
		return tree

	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return None
		
	
	logger.debug(' --------------------------- fine CambiaDurationProd ----------------------')
	return None



def Cambia_Flag_Social( Id_xx , UpdateFlags ):
	
		
	logger.debug(' --------------------------- init Cambia_Flag_Social ----------------------')
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('8568391.xml')
		# esempio di modifica del file xml 
		#tmp_tree =  CambiaField( tree, "facebookText", 'CAMBIATOOOO' )
		#tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('8568391_cambiato.xml')
		#exit(0)
		
		#ET.dump(tree)
		for key, value in UpdateFlags.iteritems():

			# fix per non modificare i campi data
			# perche se non ha alcun valore io ci sbatterei dentro None 
			# al posto di niente
			if 'Time' in key:
				continue
			# invece cosi non li cambio per niente e mi ritrovo
			# magicamente i valori che erano settati prima.
			

			logger.debug(key, value)
			if value is None:
				value = 'None'
			else:
				if  isinstance( value, ( int ) ):
					logger.debug(value)
					value = str( value )
				
			if 'None' in value:
				continue
				

			# come fatto qui sotto posso prendere tutti i fields
			tmp_tree =  CambiaField( tree, key, value )
			tree = tmp_tree

		logger.debug('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine Cambia_Flag_Social ----------------------')
		return tree

	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	logger.debug(' --------------------------- fine Cambia_Flag_Social ----------------------')
	return []


def Cambia_MonitorProd( Id_xx , html_body ):
	
		
	logger.debug(' --------------------------- init Cambia_Monitor ----------------------')
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)
	prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
	link = prod_ece  + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('10378235.xml')
		# esempio di modifica del file xml 
		#cambio = '&lt;div&gt; Tabella Per i Valentinis ... e tutti i picchetti riuniti &lt;\/div&gt;'
		cambio = 'CAMBIATOOOO  DEFAULT CLAD'
		cambio = html_body

		tmp_tree =  CambiaField( tree, "code", cambio )
		tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('10378235_cambiato.xml')
		#exit(0)
		
		#logger.debug('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
		return tree

	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
	return []



def Cambia_Monitor( Id_xx , html_body ):
	
		
	logger.debug(' --------------------------- init Cambia_Monitor ----------------------')
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('10378235.xml')
		# esempio di modifica del file xml 
		#cambio = '&lt;div&gt; Tabella Per i Valentinis ... e tutti i picchetti riuniti &lt;\/div&gt;'
		cambio = 'CAMBIATOOOO  DEFAULT CLAD'
		cambio = html_body

		tmp_tree =  CambiaField( tree, "code", cambio )
		tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('10378235_cambiato.xml')
		#exit(0)
		
		#logger.debug('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
		return tree

	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
	return []


def Rimpingua_Content( lista_content ):

	logger.debug(' ----------------------- inizia Rimpingua_Content -----------------------')
	
	result_list = []

	count = 0
	for lis in lista_content:
		#logger.debug(' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- ')
		count = count + 1
		lis['lead'] = Prendi_Altro_Content(lis['id'][8:])
		lis['link'] = 'www.rsi.ch/g/'+   lis['id'][8:]
		logger.debug(lis['lead'])
		# prima la lista da_fare la aggiungevo qui .....
		# adesso per motivi di performance  ho spostato sotto il rimpingua content
		# che faccio solo su quelle che devo postare 
		# e la lista da_fare la aggiungo prima
		#lis['da_fare'] = []
		result_list.append( lis )
		
	return result_list

	logger.debug(' ----------------------- fine Rimpingua_Content -----------------------')

def Aggiungi_Items_al_Db( archive_name, archive_dict ):
	
	if len(archive_dict) == 0 :
		return
        old_dict = {}
	if os.path.isfile( archive_name ):
		adesso = datetime.utcnow()
		shutil.copy( archive_name, archive_name + "_" + adesso.strftime("%s") )
		try :
			logger.debug('AGGIUNGO a  ARCHIVE_NAME ' + archive_name)
			fin = codecs.open( archive_name, 'r', 'utf-8' )
			old_dict = json.load( fin )
			logger.debug(old_dict)
			fin.close()
		except:
			logger.debug('PROBLEMI CON IL DB')
			pass


	for key,value in archive_dict.iteritems():
		old_dict[ key ] = value


        fout = codecs.open( archive_name, 'w', 'utf-8')
        json.dump( old_dict, fout )
        fout.close()



def Scrivi_Items_Db( db_name, lista ):

	
	
	#if len(lista) == 0 :
		#return

	#adesso = datetime.utcnow()

	#shutil.copy( db_name, db_name + "_" + adesso.strftime("%s") )

        fout = codecs.open( db_name, 'w', 'utf-8')
        json.dump( lista, fout )
        fout.close()

def Prendi_Items_Db( db_name ):

        result_dict = {}
	try :
		#logger.debug('leggo DB_NAME ' + db_name)
		fin = codecs.open( db_name, 'r', 'utf-8' )
		result_dict = json.load( fin )
		#logger.debug(result_dict)
		fin.close()
	except:
		logger.debug('PROBLEMI CON IL DB')
		pass

        return result_dict

def Aggiungi_Items_da_Db( db_name, dict_content):

	# deve aggiungere tutti e solo gli item che ci non nel Db e che mancano
	# alla query del Solr cioe non sono stati modificati nell'ultimo minuto

	#logger.debug(dict_content)
	result = dict_content

	for key,value in dict_content.iteritems():
		dict_content[ key ]['FBStreamingDetails'] = {}
		dict_content[ key ]['YTStreamingDetails'] = {}
	
	# quindi prendo quelli del db
	dict_db = Prendi_Items_Db( db_name )
	logger.debug('  totale content preso dal DB        =  ' + str(len(dict_db)))
	# e verifico che non siano gia' presenti nella dict_content
	for key, value in dict_db.iteritems():
		#logger.debug(key)
		#logger.debug(value)
		#logger.debug()
		#logger.debug(value['SocialFlags'])
		#logger.debug(value['ECEDetails'])
		
		_appendo = True
		if key in dict_content:
			# questo non lo aggiungo
			_appendo = False
			# se esistono gia i video details allora li copia nelle nuove info che arrivano da ECE
			# magari qualcuno ha cambiato qualche valore di time e dobbiamo riverificare tutto
			# in base a quell id ma non perdere i videodetail e neppure i wowza details
			if 'FBStreamingDetails' in value:
				dict_content[ key ]['FBStreamingDetails'] = value['FBStreamingDetails']
			if 'YTStreamingDetails' in value:
				dict_content[ key ]['YTStreamingDetails'] = value['YTStreamingDetails']
					
		if _appendo:
			result[ key ] = value
	
	return result

def printList( dict_content ):


	for key, value in dict_content.iteritems():
		logger.debug(' Key -----> ' + key)
		if 'ECEDetails' in value :
			logger.debug('----  ECEDetails : '	)
			logger.debug(value['ECEDetails'])
		if 'SocialFlags' in value :
			logger.debug('----  SocialFlags : '	)
			logger.debug(value['SocialFlags'])
		if 'FBStreamingDetails' in value and len(value['FBStreamingDetails']) > 0:
			logger.debug('----  FBStreamingDetails : '	)
			for keystream,valuestrem in value['FBStreamingDetails'].iteritems():
				logger.debug('Details of : ' + keystream)
				logger.debug(valuestrem)
		if 'YTStreamingDetails' in value and len(value['YTStreamingDetails']) > 0:
			logger.debug('----  YTStreamingDetails : '	)
			for keystream,valuestrem in value['YTStreamingDetails'].iteritems():
				logger.debug('Details of : ' + keystream)
				logger.debug(valuestrem)
		logger.debug('------------------------- fine ' + key + ' -----------------------\n')
	


def printIDS( dict_content ):


	lista = []
	for key, value in dict_content.iteritems():
		lista.append( str(key) )
	logger.info( str(lista) )
	

def Controlla_Flag_Social( lista_content ):
	

	logger.info('----------------------- inizia Controlla_Flag_Social -----------------------')
	#logger.debug(' lunghezza lista iniziale : ' + str(len(lista_content)))
	logger.debug(lista_content)
	#logger.debug(' da fare : ')


	# questa deve preparare la lista di quelle da fare adesso e quelle da scrivere nel DB
	# e quelli da fare sono quelli da aprire o chiudere
	# quindi che hanno appena passato lo starttime
	# o che hanno appena passato endTime
	
	da_fare_list_NOTIFICA = {}
	da_fare_list_ON = {}
	da_fare_list_OFF = {}
	da_fare_list_PREPARA = {}

	
	# cosi considero anche eventuali cambiamenti di daysaving time 
	adesso = datetime.now()
	# per controllo su data emissione mi server il tempo attuale
	logger.debug('prendo tempo attuale in utc ' + str(adesso))

	# CLAD se sono in production setto una var per togliere il 9845290
	_PROD_ = False
	if '_APICoreXEnv_' in os.environ:
		if 'PRODUCTION' in os.environ['_APICoreXEnv_']:
			_PROD_ = True

	count = 0
        #spinner = pySpin.Spinner()
        #spinner.start()
        # ... some long-running operations
        # time.sleep(3)
	for key,value in lista_content.iteritems():
		# giro su tutti gli itemspresi sia da Solr che dal mio db
		logger.debug(' ----- ' + str(count) + ' ------ ' + key + ' ------- ')

		# CLAD se sono in production TOLGO il 9845290
		if _PROD_ and '9845290' in  key:
			logger.debug( ' SONO in PROD -> Rimuovo item 9845290' )
			continue

	
		count = count + 1

		# qui prendo le social flags
		# se arrivo dal db le riprendo cmq nel caso qualcuno avesse cambiato qualcosa nell'ultimo
		# minuto 
		# 12.04.2019 nuovo content type : non basta copiarle sopra le altre
		# perche non sono piu separati i contenuti.
		# devo andare dentro e fare una merge
		# value['SocialFlags'] =   Prendi_Flag_Social_New( key )
		logger.debug( ' value -> ' + str(value ) )

		# DEBUG CLAD  da verificare
		value['SocialFlags'] =   MergeSocialFlags( Prendi_Flag_Social_New( key ), value['SocialFlags'] )
		# DEBUG CLAD 
		

		#logger.debug('debug delle social flags')
		#logger.debug('SocialFlags')
		#for k,v in value['SocialFlags'].iteritems():
			#print k,v

		#exit(0)
		# qui metto il controllo che sia ancora published
		# altrimenti salto
		if not ( value['SocialFlags']['state'] == 'published' ) :
			logger.debug(' ESCO perche non PUBLISHED')
			continue
	

		# qui metto il controllo che ci sia almeno un account
		# a cui spedirlo altrimenti salto
		if len(value['SocialFlags']['fb-livestreaming-account'] ) < 1 and len(value['SocialFlags']['yt-livestreaming-account'] ) < 1:
			logger.debug(' ESCO perche non ci sono le FlagSocial settate')
			continue
	

		# i casi sono tre
		if adesso < datetime.strptime( value['SocialFlags'] ['t_start'],  '%Y-%m-%dT%H:%M:%SZ' ):
			# devo verificare che non ci sia settata la flag setNotifTime
			# in caso positivo devo verificare il valore della t_notifica
			# e se almeno uno dei canali ha la notifica minore di adesso 
			# metto il tutto in lista prepara
			if adesso > datetime.strptime( PrendiMinNotifica( value['SocialFlags']) ,  '%Y-%m-%dT%H:%M:%SZ' ):
				logger.debug( '-----------> : ' + key +' --> Ho data notifica minore di timeNow ')
				# devo preparare il video e in caso ci sia notifica mandarla
				da_fare_list_PREPARA[ key ] = value
				# la metto da_scrivere
				# ci sara' da fare qualcosa 
			else:
			
				logger.debug( '-----------> : ' + key +' -->  Ho data notifica MAGGIORE di timeNow ')
				da_fare_list_NOTIFICA[ key ] = value
				# devo mettere il video nel DB per poi prepararlo in seguito
				# la metto da_scrivere
		else:
			if adesso > datetime.strptime( value['SocialFlags'] ['t_end'],  '%Y-%m-%dT%H:%M:%SZ' ):
				# devo spegnere
				da_fare_list_OFF[ key ] = value
			else:
				if len(value['SocialFlags']['fb-livestreaming-account']) > 0:

					if 'VideoDetails' in value['SocialFlags']['fb-livestreaming-account'].values()[0]:
						if value['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] == 'LIVE_STOPPED':
							continue
				else:
					logger.debug('QUI DOVREI AGGIUNGERE LA PREPARAZIONE avendo passato la data di inizio ma non essendo stato preparato ? \r\n')

				if len(value['SocialFlags']['yt-livestreaming-account']) > 0:
					if 'VideoDetails' in value['SocialFlags']['yt-livestreaming-account'].values()[0]:
						if value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] == 'complete':
							continue
				else:
					logger.debug('QUI DOVREI AGGIUNGERE LA PREPARAZIONE avendo passato la data di inizio ma non essendo stato preparato ? \r\n')


				# devo accendere oppure e gia accesa
				da_fare_list_ON[ key ] = value
				# e cmq ci sara qualcosa da fare

		

        #spinner.stop()
	logger.info(' lunghezza lista PREPARA  : ' + str(len(da_fare_list_PREPARA)) + '\t\t\t\t--PREPARA')
	printIDS( da_fare_list_PREPARA )
	printList (da_fare_list_PREPARA )
	logger.info(' lunghezza lista ON  : ' + str(len(da_fare_list_ON)) + '\t\t\t\t\t--ON')
	printIDS( da_fare_list_ON )
	printList ( da_fare_list_ON )
	logger.info(' lunghezza lista OFF  : ' + str(len(da_fare_list_OFF)) + '\t\t\t\t\t--OFF')
	printIDS( da_fare_list_OFF )
	printList ( da_fare_list_OFF )
	logger.info(' lunghezza lista NOTIFICA  : ' + str(len(da_fare_list_NOTIFICA)) + '\t\t\t\t--NOTIFICA')
	printIDS( da_fare_list_NOTIFICA )
	printList ( da_fare_list_NOTIFICA )
	logger.info(' ----------------------- fine Controlla_Flag_Social -----------------------')
	return [ da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF, da_fare_list_NOTIFICA ]
			
def getId( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)

		tree = ET.parse(result)
		#tree.write('prendi_URL_ok.xml')
		#ET.dump(tree)

	except urllib2.HTTPError, e:
		logger.debug(' EXCEPTIOOOONNNNNN in getId - ritorno none !!!! ' + str(e))
		return None
		 
	
	return tree


def rimuoviTagDecade( entry, tagName ) :
	
	tagEntry = prendiEntryTags( entry, 'com.escenic.tags' )
	#print tagEntry
	listList = tagEntry.findall(namespaces['vdf'] + "list" ) 
	for lis in listList:
		payList = lis.findall(namespaces['vdf'] + "payload" )
		for pay in  payList:
			fields = pay.findall(namespaces['vdf'] + "field") 
			for idx , fiel in enumerate(fields):
				if 'tag' in fiel.attrib['name']: 
					for child in fiel:
						if 'origin' in child.tag: 
							#print child.attrib['href']
							if tagName in  child.attrib['href']:
								logger.debug('rimuovo tag -> ' + child.attrib['href'])
								lis.remove( pay ) 
	



	return entry



def contieneTagDecade( entry ) :
	
	tagEntry = prendiEntryTags( entry, 'com.escenic.tags' )
	#print tagEntry
	listList = tagEntry.findall(namespaces['vdf'] + "list" ) 
	for lis in listList:
		payList = lis.findall(namespaces['vdf'] + "payload" )
		for pay in  payList:
			fields = pay.findall(namespaces['vdf'] + "field") 
			for idx , fiel in enumerate(fields):
				if 'tag' in fiel.attrib['name']: 
					for child in fiel:
						if 'origin' in child.tag: 
							#print child.attrib['href']
							if 'decadi' in  child.attrib['href']:
								return True
	return False


def contieneTag( entry, tag ) :
	
	tagEntry = prendiEntryTags( entry, 'com.escenic.tags' )
	#print tagEntry
	listList = tagEntry.findall(namespaces['vdf'] + "list" ) 
	for lis in listList:
		payList = lis.findall(namespaces['vdf'] + "payload" )
		for pay in  payList:
			fields = pay.findall(namespaces['vdf'] + "field") 
			for idx , fiel in enumerate(fields):
				if 'tag' in fiel.attrib['name']: 
					for child in fiel:
						if 'origin' in child.tag: 
							print child.attrib['href']
							if tag in  child.attrib['href']:
								return True
	return False



def contieneTagAbc( entry ) :
	
	tagEntry = prendiEntryTags( entry, 'com.escenic.tags' )
	#print tagEntry
	listList = tagEntry.findall(namespaces['vdf'] + "list" ) 
	for lis in listList:
		payList = lis.findall(namespaces['vdf'] + "payload" )
		for pay in  payList:
			fields = pay.findall(namespaces['vdf'] + "field") 
			for idx , fiel in enumerate(fields):
				if 'tag' in fiel.attrib['name']: 
					for child in fiel:
						if 'origin' in child.tag: 
							#print child.attrib['href']
							if 'tag:abc.' in  child.attrib['href']:
								return True
	return False



def estraiDuration( idX ):

	logger.debug(' ----------------------- inizia estraiDuration -----------------------')
	result = []

	try:
		# devo prendere xml di idx
		tree = getId( idX )
		
		result = PrendiField( tree, 'duration' )
	except:
		return [ False, "-1" ]

	return [ True, result ]
	

def TrovaKeyframe( idX ):

	logger.debug(' ----------------------- inizia TrovaKeyframe -----------------------')
	result = []

	# devo prendere xml di idx
	tree = getId( idX )
	
	result = PrendiListaKeyframe( tree, 'related' )

	return result
	
def UpdateSpostaArchiviKeyPic_OLD( idX , section):

	logger.debug(' ----------------------- inizia UpdateSpostaArchiviKeyPic -----------------------')

	# CLADDDDDD #
	# devo prendere xml di idx
	tree = getId( idX )

	# devo verificare  che non abbia gia la homesection giusta
	if ControllaHomeSection( tree, 'section', 'Shared Content DME' ):
		logger.warning('- aveva gia homesection == Shared Content DME - skippato item --> ' + idX )
		return False

	# devo rimuovere la section Archivi se esiste
	tree = RimuoviArchiviSection( tree, 'section' , 'Archivi')

	# devo cambiargli la home section
	sectionEntry =  CambiaSection( tree, 'section', section, 'Shared Content DME'  )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "published")

	nomefile = os.environ['UPDATE_FILE']
	print nomefile

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	
	return Put_Content_In_ECE.putId( idX, nomefile )
		
def cambiaExpireDate( idX ):

	logger.debug(' ----------------------- inizia cambiaExpireDate -----------------------')

	# CLADDDDDD #
	# devo prendere xml di idx
	tree = getId( idX )

	# devo prendere la expiration date
	expires = PrendiExpires( tree, 'expiration' )
	print( expires )
	if expires is None :
		return False

	# se i valori di expire sono sbalati
	if '4000' in expires or '2999' in expires:
		print('data sballata !')
		#devo correggerli 
		tree = removeExpires(tree, "")
		
		expires = PrendiExpires( tree, 'expiration' )
		print( expires )
	else:
		# se expirration e a posto torno indietro
		return False

	#  quindi devo ributtarlo su
	nomefile = os.environ['UPDATE_FILE']
	print nomefile

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	
	Put_Content_In_ECE.putId( idX, nomefile )
	return True
	

def UpdateAggiungiSection( idX , section):

	logger.debug(' ----------------------- inizia UpdateAggiungiSection -----------------------')

	# CLADDDDDD #
	# devo prendere xml di idx
	tree = getId( idX )

	# devo cambiargli la home section
	sectionEntry =  aggiungiSection( tree, 'section', section, 'Ricette'  )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "published")

	nomefile = os.environ['UPDATE_FILE']
	print nomefile

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	
	return Put_Content_In_ECE.putId( idX, nomefile )
	

def cambiaChannel( idX , value):

	logger.debug(' ----------------------- inizia cambiaPathSubtitles -----------------------')

	# devo prendere xml di idx
	tree = getId( idX )

	# devo cambiargli i tags e togliede tutti i Decade
	tree =  CambiaChannel( tree, value )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "draft")

	nomefile = os.environ['UPDATE_FILE']
	#print nomefile

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	logger.debug(' ----------------------- end cambiaPathSubtitles -----------------------')
	
	return Put_Content_In_ECE.putId( idX, nomefile )
	
		
	
def cambiaPathSubtitles( idX ):

	logger.debug(' ----------------------- inizia cambiaPathSubtitles -----------------------')

	# devo prendere xml di idx
	tree = getId( idX )

	# devo cambiargli i tags e togliede tutti i Decade
	tree =  CambiaSubtitleUrl( tree )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "published")

	nomefile = os.environ['UPDATE_FILE']
	#print nomefile

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	logger.debug(' ----------------------- end cambiaPathSubtitles -----------------------')
	
	return Put_Content_In_ECE.putId( idX, nomefile )
	
	
def UpdateSpostaArchiviKeyPic( idX , section):

	logger.debug(' ----------------------- inizia UpdateSpostaArchiviKeyPic -----------------------')

	# CLADDDDDD #
	# devo prendere xml di idx
	tree = getId( idX )

	# devo cambiargli la home section
	sectionEntry =  CambiaSection( tree, 'section', section, 'New Articles'  )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "published")

	nomefile = os.environ['UPDATE_FILE']
	print nomefile

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	
	return Put_Content_In_ECE.putId( idX, nomefile )

			


def UpdateTagArchivi( idX ):

	logger.debug(' ----------------------- inizia UpdateTagArchivi -----------------------')

	# CLADDDDDD #
	# devo prendere xml di idx
	tree = getId( idX )

	# devo verificare se ha dei tag con dentro decade
	if not contieneTagDecade( tree ) :
		logger.warning('- non aveva tags DECADE da togliere - skippato item --> ' + idX )
		return False
	#DEBUG CLAD
	#else:
		#logger.debug('- item -> ' + idX + ' - contiene tags abc ')
		#return True


	#logger.warning('- PASSO DI QUI ???? ' )
	#return False

	# devo cambiargli i tags e togliede tutti i Decade
	tree =  rimuoviTagDecade( tree, 'decadi'  )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "published")

	nomefile = os.environ['UPDATE_FILE']
	#print nomefile

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	
	return Put_Content_In_ECE.putId( idX, nomefile )



def UpdateSpostaArchiviNew( idX , section):

	logger.debug(' ----------------------- inizia UpdateSpostaArchiviNew -----------------------')

	# CLADDDDDD #
	# devo prendere xml di idx
	tree = getId( idX )

	# devo verificare  che non abbia gia la homesection giusta
	if ControllaHomeSection( tree, 'section', 'Archivi' ):
		logger.warning('- aveva gia homesection == Archivi - skippato item --> ' + idX )
		return False

	# devo verificare se ha dei tag con dentro abc
	if not contieneTag( tree, 'tag:abc.topic.rsi.ch,2021' ) :
		logger.warning('- non aveva abc tags - skippato item --> ' + idX )
		return False

	#DEBUG CLAD
	#else:
		#logger.debug('- item -> ' + idX + ' - contiene tags abc ')
		#return True


	#logger.warning('- PASSO DI QUI ???? ' )
	#return False

	# devo cambiargli la home section
	tree =  CambiaSection( tree, 'section', section, 'Archivi'  )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "published")

	nomefile = os.environ['UPDATE_FILE']

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	#
	return Put_Content_In_ECE.putId( idX, nomefile )



def UpdateSpostaArchivi( idX , section):

	logger.debug(' ----------------------- inizia UpdateSpostaArchivi -----------------------')

	# CLADDDDDD #
	# devo prendere xml di idx
	tree = getId( idX )

	# devo verificare  che non abbia gia la homesection giusta
	if ControllaHomeSection( tree, 'section', 'Shared Content DME' ):
		logger.warning('- aveva gia homesection == Shared Content DME - skippato item --> ' + idX )
		return False

	# devo verificare se ha dei tag con dentro abc
	if not contieneTagAbc( tree ) :
		logger.warning('- non aveva abc tags - skippato item --> ' + idX )
		return False
	#DEBUG CLAD
	#else:
		#logger.debug('- item -> ' + idX + ' - contiene tags abc ')
		#return True


	#logger.warning('- PASSO DI QUI ???? ' )
	#return False

	# devo cambiargli la home section
	sectionEntry =  CambiaSection( tree, 'section', section, 'Shared Content DME'  )
	

	# devo ributtarlo su
	tree = CambiaState(tree, "published")

	nomefile = os.environ['UPDATE_FILE']

	try:
		os.remove(nomefile)
		logger.debug(' remove finto')
	except:
		pass

	tree.write(nomefile)
	#
	return Put_Content_In_ECE.putId( idX, nomefile )

		
def UpdateMonitorProd( id_monitor, html_body ):

	logger.debug(' ----------------------- inizia UpdateMonitor -----------------------')
	logger.debug(' su id =  ' + id_monitor )
	
	count = 0

	flagsdiritorno =  Cambia_MonitorProd( id_monitor, html_body )

	# CLADDDDDD #

	logger.debug(' tornato in UpdateMonitor da Cambia _Flag_Social ')
	logger.debug(PrendiState( flagsdiritorno, 'dummy' ))
	flagsdiritorno =  CambiaState(flagsdiritorno, "published")

	nome_file = os.environ['UPDATE_FILE']

	try:
		os.remove(nome_file)
		logger.debug(' remove finto')
	except:
		pass

	flagsdiritorno.write(nome_file)
	# brutto fix : cambiamo <html: e </html: direttamente nel file
	CambiaBodyFile( nome_file)

	Put_IdProd( id_monitor, nome_file)

	logger.debug(' ----------------------- fine UpdateMonitor CLAD -----------------------')
	return 
	
		
def UpdateMonitor( id_monitor, html_body ):

	logger.debug(' ----------------------- inizia UpdateMonitor -----------------------')
	logger.debug(' su id =  ' + id_monitor )
	
	count = 0

	flagsdiritorno =  Cambia_Monitor( id_monitor, html_body )

	# CLADDDDDD #

	logger.debug(' tornato in UpdateMonitor da Cambia _Flag_Social ')
	logger.debug(PrendiState( flagsdiritorno, 'dummy' ))
	flagsdiritorno =  CambiaState(flagsdiritorno, "published")

	nome_file = os.environ['UPDATE_FILE']
	try:
		os.remove(nome_file)
		logger.debug(' remove finto')
	except:
		pass

	flagsdiritorno.write(nome_file)
	# brutto fix : cambiamo <html: e </html: direttamente nel file
	CambiaBodyFile( nome_file)

	Put_Id( id_monitor, nome_file)

	logger.debug(' ----------------------- fine UpdateMonitor CLAD -----------------------')
	return 
		
	
		
def Update_Flag_Social( lista_content ):

	logger.debug(' ----------------------- inizia Update_Flag_Social -----------------------')
	logger.debug(' lunghezza lista iniziale : ' + str(len(lista_content)))
	
	result_list = []

	nome_file = os.environ['UPDATE_FILE']
	count = 0
	for lis in lista_content:
		logger.debug(' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- ')
		count = count + 1
		logger.debug(lis['social_flags'])

		# il valore di ritorno del forceResend DEVE SEMPRE essere a false
		lis['social_flags']['tw-forceResend'] = 'false'
		lis['social_flags']['fb-forceResend'] = 'false'

	
		flagsdiritorno =  Cambia_Flag_Social(lis['id'][8:], lis['social_flags'])

		# CLADDDDDD #

		logger.debug(' tornato in Update_Flag da Cambia _Flag_Social ')
		logger.debug(PrendiState( flagsdiritorno, 'dummy' ))
		flagsdiritorno =  CambiaState(flagsdiritorno, "published")

		try:
			os.remove(nome_file)
			logger.debug(' remove finto')
		except:
			pass

		flagsdiritorno.write(nome_file)
		# brutto fix : cambiamo <html: e </html: direttamente nel file
		CambiaBodyFile( nome_file)

		Put_Id( lis['id'][8:], nome_file)

	logger.debug(' ----------------------- fine Update CLAD Flag_Social -----------------------')
	return result_list
		
		

if __name__ == "__main__":

	os.environ['ECE_USER'] = 'TSMM'
	os.environ['ECE_PWD'] = '8AKjwWXiWAFTxb2UM3pZ'

	os.environ['ECE_SERVER'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/'
	os.environ['ECE_SERVER'] = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
	os.environ['ECE_SECTION'] = 'http://internal.publishing.production.rsi.ch/webservice/escenic/section/'

	Put_IdProd( 1011 , '/home/perucccl/Webservices/STAGING/TransAudioDuration/1011.xml')
	exit(0)
	print 'CambioDuration'
	tree = CambiaDurationProd( '1011', '666.666' )
	tree.write('1011.xml')
	ET.dump(tree)
	exit(0)

	lista =  Prendi_Flag_Social_New( '1011' )
	logger.debug(lista)
	for lis in lista.iteritems():
		logger.debug(lis)
	logger.debug(lista)
	exit(0)

	lista_parameters = Prendi_Section_Parameters( '22742' )
	print lista_parameters
	exit(0)
	
	lista =  Prendi_Flag_Social( '10501717' )
	logger.debug(lista)
	for lis in lista.iteritems():
		logger.debug(lis)
	logger.debug(lista)
	exit(0)

	#Get_Item_Content('8845033')
	#exit(0)

	Put_Id( 8732965, './_test_cambiamento_')
	exit(0)

	#logger.debug(Prendi_Social_TimeCtrl('8730301'))
	# logger.debug(Prendi_Social_TimeCtrl('8568391'))
	
	#logger.debug(Check_Data_Range( datetime.utcnow(), '8568391'))
	#logger.debug('Adesso CLAD')
	#logger.debug(Prendi_Altro_Content( '8730301'))
	#exit(0)

	UpdateMonitor( '10378235', 'dummy', 'dummy' )
	exit(0)

	lista =  Prendi_Flag_Social( '9845290' )
	logger.debug(lista)
	for lis in lista.iteritems():
		logger.debug(lis)
	logger.debug(lista)
	exit(0)
	lista_da_passare = { 'id':'1234567:8568391'}
	lista_da_passare = {'contenttype': 'story', 'lead': {'picture': None, 'caption': '', 'link': 'http://www.rsi.ch/temp/test-push-to-social-Civi-8568391.html', 'name': 'test push to social Civi', 'description': 'subhead'}, 'title': 'test story title', 'lastmodifieddate': '2017-01-10T13:23:23Z', 'state': 'published', 'da_fare': [], 'id': 'article:8568391'}

	Controlla_Flag_Social( [ lista_da_passare])
	#logger.debug('in main')
	#Prendi_Altro_Content( '8568391')
	exit(0)
	#programmeId  = GetSection_and_State( 117530 )
	#logger.debug(programmeId)
	#logger.debug()
	#logger.debug(State_Id_xx)
	#print
	#logger.debug(Edited_Id_xx)
