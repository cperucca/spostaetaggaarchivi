
# -*- coding: utf-8 -*-

import logging
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import urllib
import json
import os

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'vdf' : '{http://www.vizrt.com/types}',
		'ece' : '{http://www.escenic.com/2007/content-engine}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

logger = logging.getLogger('pyECE')

def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()


	return


def Dump_Link_Vmeo ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('online@vizrt.com', '3sc3niC')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()
	exit(0)
	return

#logger.debug(' Dump_Link_Vmeo ')
#link = 'http://10.102.7.21//rest/search/RSI?query=production'
#link = 'http://10.102.7.21//rest/search/RSI?query=36943'
#link = 'http://10.102.7.21//rest/digitalItem/search?provider=RSI&query=title:"escenic:production36943"'
#link = 'http://10.102.7.21//rest/search/RSI?query=production36943'
#link = 'http://10.102.7.21//rest/publishedItems/search/RSI?query=production36943'
#link = 'http://10.102.7.21//rest/digitalItem/status/9255'
#link = 'http://10.102.7.21//rest/digitalItem/status/9255'
#link = 'http://10.102.7.21//rest/digitalItem/status/36943'
#link = 'http://10.102.7.21//rest/publishedItems/search?provider=RSI&query="*production36943"'
#Dump_Link_Vmeo( link, 'test_search')

def Solr_Prendi_DateSbagliate( section_id ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug('-------------- INIT ---- Solr_Prendi_Archivi ----------- ' )

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	# prende items con 
	# state:published 
	# creationdate:[NOW-1DAY TO NOW]
	# contenttype:transcodableAudio
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AProgrammeAudio&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true

	# questa con la lastmodifieddate
	# dell'ultima ora
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"

	# per i video del 2000
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3A*Video&fq=state%3Apublished&fq=publishdate%3A%5B%222000-01-01T00%3A00%3A01Z%22+TO+%222020-12-31T23%3A59%3A59Z%22%5D&fl=publishdate%2C+&wt=json&indent=true"
	# per i video degli anni 70
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3A*Video&fq=state%3Apublished&fq=publishdate%3A%5B%221970-01-01T00%3A00%3A01Z%22+TO+%221979-12-31T23%3A59%3A59Z%22%5D&fl=publishdate%2C+classification%2Cclassification_parent_path%2Csrg+play+tags_facet%2Csrg+hbbtv+tags_facet&wt=json&indent=true"
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3A*Video&fq=state%3Apublished&fq=publishdate%3A%5B__TIME__%5D&fl=publishdate%2C+classification%2Cclassification_parent_path%2Csrg+play+tags_facet%2Csrg+hbbtv+tags_facet&wt=json&indent=true"

	# per prendere tutte le pictures e i keyframes
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3Apicture+%7C%7C+contenttype%3Akeyframe&fq=state%3Apublished&fl=publishdate%2C+classification%2Cclassification_parent_path%2Csrg+play+tags_facet%2Csrg+hbbtv+tags_facet&wt=json&indent=true"

	# per prendere tutto
	# http://internal.presentation.staging.rsi.ch:8180

	# per prendere tutti quelli con tag = tag:srg.topic.rsi.ch,2017:sharedcontentplattformdme
	# http://internal.publishing.rsi.ch:8180/solr/collection1/select?q=*%3A*&fq=classification%3A%22tag:srg.topic.rsi.ch,2017:sharedcontentplattformdme%22&sort=publishdate+desc&fl=objectid&wt=json&indent=true

	#link_template = os.environ['SOLR_SERVER'] + "?q=*%3A*&fq=section%3A__SECTION_ID__&fq=contenttype%3Apicture+%7C%7C+contenttype%3Akeyframe&fq=state%3Apublished&sort=creationdate+DESC&start=__START__&rows=100&fl=id&wt=json&indent=true"
	
	#link_template = os.environ['SOLR_SERVER'] + "?q=*%3A*&fq=section%3A__SECTION_ID__&fq=contenttype%3A*Video&fq=state%3Apublished&sort=creationdate+DESC&start=__START__&rows=100&fl=id&wt=json&indent=true"
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=state%3Apublished&fq=contenttype%3Apicture+%7C%7C+contenttype%3Akeyframe&fq=home_section%3A10630&wt=json&indent=true

	#http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableVideo+%7C%7C+contenttype%3AprogrammeVideo+%7C%7C+contenttype%3AmamProgrammeVideo+%7C%7C+contenttype%3AmamTranscodableVideo&fq=state%3Apublished&fq=creationdate%3A%5BNOW-4YEARS+TO+NOW%5D&wt=json&indent=true

	#link_template = os.environ['SOLR_SERVER'] + "?q=*%3A*&fq=state%3Apublished&fq=contenttype%3Apicture+%7C%7C+contenttype%3Akeyframe&sort=creationdate+DESC&fq=home_section%3A10630&start=__START__&rows=100&fl=id&wt=json&indent=true"
	link_template = os.environ['SOLR_SERVER'] + "?q=*%3A*&fq=state%3Apublished&fq=contenttype%3AtranscodableVideo+%7C%7C+contenttype%3AprogrammeVideo+%7C%7C+contenttype%3AmamProgrammeVideo+%7C%7C+contenttype%3AmamTranscodableVideo&fq=creationdate%3A%5BNOW-5YEARS+TO+NOW-2YEARS%5D&sort=creationdate+DESC&start=__START__&rows=100&fl=id&wt=json&indent=true"
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableAudio&fq=state%3Apublished&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=home_section_name%3A\"Notiziario\"&sort=creationdate+DESC&start=__START__&rows=100&fl=id&wt=json&indent=true"

	# CLAD cambiato per avere dei livestreaming
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-1MONTHS+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	
	link = link_template.replace('__START__', '0')
	link = link.replace('__SECTION_ID__', section_id)
	print link

	connection = urllib2.urlopen(link)
	response = eval(connection.read() )

	logger.info('Presi dal Solr : ' + str(response['response']['numFound']) + ' ' + "documents found")
	print('Presi dal Solr : ' + str(response['response']['numFound']) + ' ' + "documents found")

	#logger.debug(len(response['response']['docs']))

	lista_content_id_data = response['response']['docs']
	logger.debug(len(lista_content_id_data))

        totresult = int(response['response']['numFound'])
        items_per_page = int(100)

        if  totresult > items_per_page:
                #logger.debug(' giro sui next e prev ')
                # devi  girare sui next per prendere gli altri
                for x in range(int(float(totresult)/float(items_per_page))):
			__start__ = (x+1) * items_per_page
			logger.debug(str(x+1) + ' ' + str(__start__) )
			
			#logger.debug(' giro per prenderli tutti')
                        #e qui faccio la request sul campo next
                        link_next = link_template.replace('__SECTION_ID__', section_id).replace('__START__', str(__start__))
			#logger.debug(link_next)
                        #logger.debug(link_next)
                        connection = urllib2.urlopen(link_next)
			response = eval(connection.read() )

			#logger.debug('Ricevuti : ' + str(len(response['response']['docs'])))
			#logger.debug( response['response']['docs'])

			lista_content_id_data = lista_content_id_data +  response['response']['docs']
			
			#lista_content_id_data.append( response['response']['docs'] )
			#logger.debug(len(lista_content_id_data))


        #logger.debug(' totale content preso per section ' + section_id + '  = % d ' % len(lista_content_id_data))
	# e adesso lo trasformo in un dizionario
	result_dict = {}

	#print lista_content_id_data
	#print type( lista_content_id_data )
	
	#with open('../Resources/Jsons/transAudioNotiziari.json', 'w' ) as fout:
		#for lis in lista_content_id_data:
			#print >> fout,str(lis['id'][8:])

	#exit(0)
		
	
	
	resultList = []
	for lis in lista_content_id_data:
		resultList.append( lis['id'][8:] )
		result_dict[str(lis['id'][8:])] = lis

	#logger.debug(result_dict)
	print resultList
	print str(len(resultList))
		
	logger.debug('-------------- END ---- Solr_Prendi_Archivi ----------- ' )

        return resultList



def Solr_Prendi_Archivi_Time( section_id, time ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug('-------------- INIT ---- Solr_Prendi_Archivi ----------- ' )

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	# prende items con 
	# state:published 
	# creationdate:[NOW-1DAY TO NOW]
	# contenttype:transcodableAudio
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AProgrammeAudio&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true

	# questa con la lastmodifieddate
	# dell'ultima ora
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"

	# per i video del 2000
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3A*Video&fq=state%3Apublished&fq=publishdate%3A%5B%222000-01-01T00%3A00%3A01Z%22+TO+%222020-12-31T23%3A59%3A59Z%22%5D&fl=publishdate%2C+&wt=json&indent=true"
	# per i video degli anni 70
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3A*Video&fq=state%3Apublished&fq=publishdate%3A%5B%221970-01-01T00%3A00%3A01Z%22+TO+%221979-12-31T23%3A59%3A59Z%22%5D&fl=publishdate%2C+classification%2Cclassification_parent_path%2Csrg+play+tags_facet%2Csrg+hbbtv+tags_facet&wt=json&indent=true"
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3A*Video&fq=state%3Apublished&fq=publishdate%3A%5B__TIME__%5D&fl=publishdate%2C+classification%2Cclassification_parent_path%2Csrg+play+tags_facet%2Csrg+hbbtv+tags_facet&wt=json&indent=true"

	# per prendere tutte le pictures e i keyframes
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=section%3A20815&fq=contenttype%3Apicture+%7C%7C+contenttype%3Akeyframe&fq=state%3Apublished&fl=publishdate%2C+classification%2Cclassification_parent_path%2Csrg+play+tags_facet%2Csrg+hbbtv+tags_facet&wt=json&indent=true"

	# per prendere tutto
	# http://internal.presentation.staging.rsi.ch:8180

	link_template = os.environ['SOLR_SERVER'] + "?q=*%3A*&fq=section%3A__SECTION_ID__&fq=contenttype%3A*Video&fq=state%3Apublished&fq=publishdate%3A%5B__TIME__%5D&&sort=creationdate+DESC&start=__START__&rows=100&fl=id&wt=json&indent=true"
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableAudio&fq=state%3Apublished&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=home_section_name%3A\"Notiziario\"&sort=creationdate+DESC&start=__START__&rows=100&fl=id&wt=json&indent=true"

	# CLAD cambiato per avere dei livestreaming
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-1MONTHS+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	link = link_template.replace('__START__', '0')
	link = link.replace('__SECTION_ID__', section_id).replace('__TIME__', time)
	print link

	connection = urllib2.urlopen(link)
	response = eval(connection.read() )

	logger.info('Presi dal Solr : ' + str(response['response']['numFound']) + ' ' + "documents found")

	#logger.debug(len(response['response']['docs']))

	lista_content_id_data = response['response']['docs']
	logger.debug(len(lista_content_id_data))

        totresult = int(response['response']['numFound'])
        items_per_page = int(100)

        if  totresult > items_per_page:
                #logger.debug(' giro sui next e prev ')
                # devi  girare sui next per prendere gli altri
                for x in range(int(float(totresult)/float(items_per_page))):
			__start__ = (x+1) * items_per_page
			logger.debug(str(x+1) + ' ' + str(__start__) )
			
			#logger.debug(' giro per prenderli tutti')
                        #e qui faccio la request sul campo next
                        link_next = link_template.replace('__SECTION_ID__', section_id).replace('__START__', str(__start__)).replace('__TIME__', time)
			#logger.debug(link_next)
                        #logger.debug(link_next)
                        connection = urllib2.urlopen(link_next)
			response = eval(connection.read() )

			#logger.debug('Ricevuti : ' + str(len(response['response']['docs'])))
			#logger.debug( response['response']['docs'])

			lista_content_id_data = lista_content_id_data +  response['response']['docs']
			
			#lista_content_id_data.append( response['response']['docs'] )
			#logger.debug(len(lista_content_id_data))


        #logger.debug(' totale content preso per section ' + section_id + '  = % d ' % len(lista_content_id_data))
	# e adesso lo trasformo in un dizionario
	result_dict = {}

	#print lista_content_id_data
	#print type( lista_content_id_data )
	
	#with open('../Resources/Jsons/transAudioNotiziari.json', 'w' ) as fout:
		#for lis in lista_content_id_data:
			#print >> fout,str(lis['id'][8:])

	#exit(0)
		
	
	
	resultList = []
	for lis in lista_content_id_data:
		resultList.append( lis['id'][8:] )
		result_dict[str(lis['id'][8:])] = lis

	#logger.debug(result_dict)
		
	logger.debug('-------------- END ---- Solr_Prendi_Archivi ----------- ' )

        return resultList



def Solr_Prendi_Video_Time( section_id, timeStr ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug('-------------- INIT ---- Solr_Prendi_Dict_From_Rsi ----------- ' )

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	# prende items con 
	# state:published 
	# creationdate:[NOW-1DAY TO NOW]
	# contenttype:transcodableAudio
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AProgrammeAudio&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true

	# questa con la lastmodifieddate
	# dell'ultima ora
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-5MINUTES+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&start=__START__&rows=100&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableVideo+%7C%7C+contenttype%3AprogrammeVideo&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableVideo+%7C%7C+contenttype%3AprogrammeVideo+%7C%7C+contenttype%3AmamProgrammeVideo+%7C%7C+contenttype%3AmamTranscodableVideo&fq=state%3Apublished&start=__START__&rows=100&fq=publishdate%3A%5BNOW-__TIMESTR__DAY+TO+NOW%5D&sort=creationdate+DESC&fl=id&wt=json&indent=true"

	# CLAD cambiato per avere dei livestreaming
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-1MONTHS+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	link = link_template.replace('__TIMESTR__',timeStr).replace('__START__', '0')
	print link

	logger.debug('link : ' + link )

	connection = urllib2.urlopen(link)
	response = eval(connection.read() )

	logger.info('Presi dal Solr : ' + str(response['response']['numFound']) + ' ' + "documents found")

	#logger.debug(len(response['response']['docs']))

	lista_content_id_data = response['response']['docs']
	logger.debug(len(lista_content_id_data))

        totresult = int(response['response']['numFound'])
        items_per_page = int(100)

        if  totresult > items_per_page:
                #logger.debug(' giro sui next e prev ')
                # devi  girare sui next per prendere gli altri
                for x in range(int(float(totresult)/float(items_per_page))):
			__start__ = (x+1) * items_per_page
			logger.debug(str(x+1) + ' ' + str(__start__) )
			
			#logger.debug(' giro per prenderli tutti')
                        #e qui faccio la request sul campo next
                        link_next = link_template.replace('__TIMESTR__',timeStr).replace('__SECTION_ID__', section_id).replace('__START__', str(__start__))
			#logger.debug(link_next)
                        #logger.debug(link_next)
                        connection = urllib2.urlopen(link_next)
			response = eval(connection.read() )

			#logger.debug('Ricevuti : ' + str(len(response['response']['docs'])))
			#logger.debug( response['response']['docs'])

			lista_content_id_data = lista_content_id_data +  response['response']['docs']
			
			#lista_content_id_data.append( response['response']['docs'] )
			#logger.debug(len(lista_content_id_data))


        #logger.debug(' totale content preso per section ' + section_id + '  = % d ' % len(lista_content_id_data))
	# e adesso lo trasformo in un dizionario
	result_dict = {}

	'''
	print lista_content_id_data
	print type( lista_content_id_data )
	
	with open('../Resources/Jsons/transAudioDataSorted.json', 'w' ) as fout:
		for lis in lista_content_id_data:
			print >> fout,str(lis['id'][8:])

	exit(0)
	'''
		
	
	
	for lis in lista_content_id_data:
		result_dict[str(lis['id'][8:])] = lis

	#logger.debug(result_dict)
		
	logger.debug('-------------- END ---- Solr_Prendi_Dict_From_Rsi ----------- ' )

        return result_dict


def Solr_Prendi_Audio( section_id ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug('-------------- INIT ---- Solr_Prendi_Dict_From_Rsi ----------- ' )

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	# prende items con 
	# state:published 
	# creationdate:[NOW-1DAY TO NOW]
	# contenttype:transcodableAudio
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AProgrammeAudio&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true

	# questa con la lastmodifieddate
	# dell'ultima ora
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-5MINUTES+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&start=__START__&rows=100&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableAudio&fq=state%3Apublished&start=__START__&rows=100&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&sort=creationdate+DESC&fl=id&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableAudio&fq=state%3Apublished&start=__START__&rows=100&fq=creationdate%3A%5BNOW-33DAY+TO+NOW%5D&sort=creationdate+DESC&fl=id&wt=json&indent=true"

	# CLAD cambiato per avere dei livestreaming
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-1MONTHS+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	link = link_template.replace('__START__', '0')
	print link

	logger.debug('link : ' + link )

	connection = urllib2.urlopen(link)
	response = eval(connection.read() )

	logger.info('Presi dal Solr : ' + str(response['response']['numFound']) + ' ' + "documents found")

	#logger.debug(len(response['response']['docs']))

	lista_content_id_data = response['response']['docs']
	logger.debug(len(lista_content_id_data))

        totresult = int(response['response']['numFound'])
        items_per_page = int(100)

        if  totresult > items_per_page:
                #logger.debug(' giro sui next e prev ')
                # devi  girare sui next per prendere gli altri
                for x in range(int(float(totresult)/float(items_per_page))):
			__start__ = (x+1) * items_per_page
			logger.debug(str(x+1) + ' ' + str(__start__) )
			
			#logger.debug(' giro per prenderli tutti')
                        #e qui faccio la request sul campo next
                        link_next = link_template.replace('__SECTION_ID__', section_id).replace('__START__', str(__start__))
			#logger.debug(link_next)
                        #logger.debug(link_next)
                        connection = urllib2.urlopen(link_next)
			response = eval(connection.read() )

			#logger.debug('Ricevuti : ' + str(len(response['response']['docs'])))
			#logger.debug( response['response']['docs'])

			lista_content_id_data = lista_content_id_data +  response['response']['docs']
			
			#lista_content_id_data.append( response['response']['docs'] )
			#logger.debug(len(lista_content_id_data))


        #logger.debug(' totale content preso per section ' + section_id + '  = % d ' % len(lista_content_id_data))
	# e adesso lo trasformo in un dizionario
	result_dict = {}

	'''
	print lista_content_id_data
	print type( lista_content_id_data )
	
	with open('../Resources/Jsons/transAudioDataSorted.json', 'w' ) as fout:
		for lis in lista_content_id_data:
			print >> fout,str(lis['id'][8:])

	exit(0)
	'''
		
	
	
	for lis in lista_content_id_data:
		result_dict[str(lis['id'][8:])] = lis

	#logger.debug(result_dict)
		
	logger.debug('-------------- END ---- Solr_Prendi_Dict_From_Rsi ----------- ' )

        return result_dict


def Solr_Prendi_Audio_New( section_id, mese, tipo ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug('-------------- INIT ---- Solr_Prendi_Dict_From_Rsi ----------- ' )

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	# prende items con 
	# state:published 
	# creationdate:[NOW-1DAY TO NOW]
	# contenttype:transcodableAudio
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AProgrammeAudio&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true

	# questa con la lastmodifieddate
	# dell'ultima ora
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-5MINUTES+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AprogrammeAudio&fq=state%3Apublished&start=__START__&rows=100&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fl=id%2Ctitle&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableAudio&fq=state%3Apublished&start=__START__&rows=100&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&sort=creationdate+DESC&fl=id&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableAudio&fq=state%3Apublished&start=__START__&rows=100&fq=creationdate%3A%5BNOW-4MONTHS+TO+NOW%5D&sort=creationdate+DESC&fl=id&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3AtranscodableAudio&start=__START__&rows=100&fq=creationdate%3A%5B%222019-02-01T00%3A00%3A01Z%22+TO+%222019-02-30T23%3A59%3A59Z%22%5D&sort=creationdate+DESC&fl=id&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Atranscodable" + tipo + "&start=__START__&rows=100&fq=creationdate%3A%5B%222020-"+mese+"-01T00%3A00%3A01Z%22+TO+%222020-"+mese+"-30T23%3A59%3A59Z%22%5D&sort=creationdate+DESC&fl=id&wt=json&indent=true"

	# CLAD cambiato per avere dei livestreaming
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-1MONTHS+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	link = link_template.replace('__START__', '0')
	print link

	logger.debug('link : ' + link )

	connection = urllib2.urlopen(link)
	response = eval(connection.read() )

	logger.info('Presi dal Solr : ' + str(response['response']['numFound']) + ' ' + "documents found")

	#logger.debug(len(response['response']['docs']))

	lista_content_id_data = response['response']['docs']
	logger.debug(len(lista_content_id_data))

        totresult = int(response['response']['numFound'])
        items_per_page = int(100)

        if  totresult > items_per_page:
                #logger.debug(' giro sui next e prev ')
                # devi  girare sui next per prendere gli altri
                for x in range(int(float(totresult)/float(items_per_page))):
			__start__ = (x+1) * items_per_page
			logger.debug(str(x+1) + ' ' + str(__start__) )
			
			#logger.debug(' giro per prenderli tutti')
                        #e qui faccio la request sul campo next
                        link_next = link_template.replace('__SECTION_ID__', section_id).replace('__START__', str(__start__))
			#logger.debug(link_next)
                        #logger.debug(link_next)
                        connection = urllib2.urlopen(link_next)
			response = eval(connection.read() )

			#logger.debug('Ricevuti : ' + str(len(response['response']['docs'])))
			#logger.debug( response['response']['docs'])

			lista_content_id_data = lista_content_id_data +  response['response']['docs']
			
			#lista_content_id_data.append( response['response']['docs'] )
			#logger.debug(len(lista_content_id_data))


        #logger.debug(' totale content preso per section ' + section_id + '  = % d ' % len(lista_content_id_data))
	# e adesso lo trasformo in un dizionario
	result_dict = {}

	'''
	print lista_content_id_data
	print type( lista_content_id_data )
	
	with open('../Resources/Jsons/transAudioDataSorted.json', 'w' ) as fout:
		for lis in lista_content_id_data:
			print >> fout,str(lis['id'][8:])

	exit(0)
	'''
		
	
	
	for lis in lista_content_id_data:
		result_dict[str(lis['id'][8:])] = lis

	#logger.debug(result_dict)
		
	logger.debug('-------------- END ---- Solr_Prendi_Dict_From_Rsi ----------- ' )

        return result_dict

def PrendiListaVideoPulita( timeStr ):

	# CLAD -> OCIOO CHE ESCE DA SOPRAAAA !!!
	listaVideo = Solr_Prendi_Video_Time( '4', timeStr )
	listaDaFare = []

	logger.info( 'len Video Tot = ' + str(len(listaVideo )))

	for lis,value in listaVideo.iteritems():
		listaDaFare.append( lis )

	logger.info( 'len Video Da Fare = ' + str(len(listaDaFare )))
	return listaDaFare
		

def PrendiListaAudioPulita( timeStr ):

	# CLAD -> OCIOO CHE ESCE DA SOPRAAAA !!!
	listaNotiziari = Solr_Prendi_Notiziario_Time( '24204', timeStr )
	listaAudio = Solr_Prendi_Audio_Time( '4', timeStr )
	listaDaFare = []

	logger.info( 'len Notiziari = ' + str(len(listaNotiziari )))
	logger.info( 'len Audio Tot = ' + str(len(listaAudio )))

	for lis,value in listaAudio.iteritems():
		if lis in listaNotiziari:
			continue
		listaDaFare.append( lis )

	logger.info( 'len Audio Da Fare = ' + str(len(listaDaFare )))
	return listaDaFare
		
def ScriviDurataAudioVideo():
	
	result = {}
	
	for tipo in [ 'Audio', 'Video' ]:
		print tipo
		result[ tipo ] = {}
		for mese in range( 1,2 ):
			print "%02d" % mese
			meseStr =  "%02d" % mese
			listaAudio = Solr_Prendi_Audio_New( '4' , meseStr, tipo)
			result[tipo][meseStr] = []
			print str(len(listaAudio))
			for lis in listaAudio:
				print lis
				result[tipo][meseStr].append( lis )
	return result
	exit(0)


if __name__ == "__main__":

	os.environ['SOLR_SERVER'] = 'http://internal.publishing.rsi.ch:8180/solr/collection1/select'
	os.environ['SOLR_SERVER'] = 'http://10.102.7.38:8180/solr/collection1/select'
	# CLAD -> OCIOO CHE ESCE DA SOPRAAAA !!!
	listaArchivi = Solr_Prendi_DateSbagliate( '20815' )
	exit(0)

	listaAudio = Solr_Prendi_Audio( '4' )
	listaDaFare = []

	print len(listaNotiziari )
	print len(listaAudio )

	for lis,value in listaAudio.iteritems():
		if lis in listaNotiziari:
			continue
		listaDaFare.append( lis )

	print len( listaDaFare )
		
	
	exit(0)


	
	print lista_content
	print len(lista_content)
	with open('./transAudioNotiziari.json', 'w') as f:
	    json.dump(lista_content, f)
	exit(0)

	def getKey( item ):
		return item[1]
	logger.debug(' ----------------- in Crea_List_Audio_Video_RSI ---------------')


	lista_sections = [ '4588' ]

	lista_content = []
	for lis in lista_sections:
		lista_content =  lista_content + Solr_Prendi_Content_From_Rsi( lis )

	logger.debug(len(lista_content))
	logger.debug(lista_content)
