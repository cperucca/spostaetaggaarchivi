#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import urllib2
import urllib
import requests
import json
import ast
import logging
import time
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
http_client.HTTPConnection.debuglevel = 0
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

logger = logging.getLogger('pyGetRestJson')

# fare una GET su http://www.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json
# mi permette di prendere il json 
# fare una POST su http://publishing.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json
# mi permette di ributtare su il nuovo json

def PostJson( jsonToPost, url ):

        logger.debug(" ---------------------- INIT PostJson ------------------ " )
        result = {}
        try:

                meta =  jsonToPost

                param = { }


                headers =  { 'Content-type': 'application/json'}

                requ = requests.request('POST', url ,headers=headers,params=param,data=json.dumps(meta))


                #upload your video
                logger.debug(requ.status_code)
                logger.debug(requ.content)

                # get youtube id
                result = json.loads(requ.content)
                logger.debug(result)

	except Exception as e:
		logger.debug( 'PROBLEMI in PostJson : ' + str(e) )
                pass

        logger.debug(" ---------------------- FINE PostJson ------------------ " )
 
def GetAudioDurationFromPlay( idX ):

        logger.debug(" ---------------------- INIZIO GetAudioDurationFromPlay ------------------ ")
	result = False
        playJson = {}
	[ result, playJson ] = GetJsonFromPlay("https://www.rsi.ch/rsi-api/intlay/srgplay/play/", idX)
	#print [ result, playJson ]
	if not result:
		[ result, playJson ] = GetJsonFromPlay("https://pindex.rsi.ch/api/", idX)
	print  'GetAudioDuration playJson = ' + str(playJson )
	if not result :
		return[ False, playJson, '0' ]	
	
	if not 'Audios' in playJson and not 'Videos' in playJson:
		return[ False, playJson, '1' ]	
	logger.debug( '.')
	if 'Audios' in playJson:
		primo = 'Audios'
		secondo = 'Audio'
	else:
		primo = 'Videos'
		secondo = 'Video'

	if not secondo in playJson[primo] or len(playJson[primo][secondo]) < 1:
		return[ False, playJson, '2' ]	

	assetJson = playJson[primo][secondo][0]
	if not 'assetMetadatas' in assetJson:
		return[ False, playJson, '2.5' ]	
	else:
		logger.debug( '..')

		if not 'duration' in assetJson['assetMetadatas']:
			return[False, playJson, '3' ]

		else:
			return[ True, playJson, assetJson['assetMetadatas']['duration'] ]

	logger.debug(" ---------------------- FINE GetAudioDurationFromPlay   ------------------ ")
        return [False, 'Non ho trovato nulla', '4']


def GetVideoDurationFromPlay( idX ):

        logger.debug(" ---------------------- INIZIO GetVideoDurationFromPlay ------------------ ")
	result = False
        playJson = {}
	[ result, playJson ] = GetJsonFromPlay("https://www.rsi.ch/rsi-api/intlay/srgplay/play/", idX)
	#print [ result, playJson ]
	if not result:
		[ result, playJson ] = GetJsonFromPlay("https://pindex.rsi.ch/api/", idX)
	#logger.debug( 'GetVideoDuration playJson = ' + str(playJson ))
	if not result :
		return[ False, playJson, '0' ]	
	
	if not 'Videos' in playJson :
		return[ False, playJson, '1' ]	
	logger.debug( '.')
	if not 'Video' in playJson['Videos'] or len(playJson['Videos']['Video']) < 1:
		return[ False, playJson, '2' ]	

	audioJson = playJson['Videos']['Video'][0]
	if not 'assetMetadatas' in audioJson:
		return[ False, playJson, '2.5' ]	
	else:
		logger.debug( '..')

		if not 'duration' in audioJson['assetMetadatas']:
			return[False, playJson, '3' ]

		else:
			return[ True, playJson, audioJson['assetMetadatas']['duration'] ]
							 
			

	logger.debug(" ---------------------- FINE GetVideoDurationFromPlay   ------------------ ")
        return [False, 'Non ho trovato nulla', '4']


def GetAudioFileFromPlay( idX ):

        logger.debug(" ---------------------- INIZIO GetAudioFileFromPlay ------------------ ")
	result = False
        playJson = {}
	[ result, playJson ] = GetJsonFromPlay("https://www.rsi.ch/rsi-api/intlay/srgplay/play/", idX)
	if not result:
		[ result, playJson ] = GetJsonFromPlay("https://pindex.rsi.ch/api/", idX)
	#logger.debug( 'GetAudioFile playJson = ' + str(playJson ))
	if not result :
		return[ False, playJson ]	
	
	if not 'Audios' in playJson :
		return[ False, playJson ]	
	logger.debug( '.')
	if not 'Audio' in playJson['Audios'] :
		return[ False, playJson ]	
	else:
		logger.debug( '..')
		audioJson = playJson['Audios']['Audio'][0]

		if not 'AssetSet' in audioJson:
			return[False, playJson ]

		if not 'location' in audioJson['AssetSet']:
			return[False, playJson ]

		logger.debug( '...')
		if not 'podcast' in audioJson['AssetSet']['location']:
			logger.debug( '....')
			return[False, playJson ]
		else:
			logger.debug( '.....')
			filePaths = audioJson['AssetSet']['location']['podcast']
			for audioFile in filePaths:
				if 'mimeType' in audioFile and 'mpeg' in audioFile['mimeType']:
					if 'uri' in audioFile:
						logger.info('Trovato file path = ' + audioFile['uri'] )
						if len(  audioFile['uri'] )  > 0 :
							return [ True, audioFile['uri'] ]
						else:
							return[False, playJson ]
							 
			

	logger.debug(" ---------------------- FINE GetAudioFileFromPlay   ------------------ ")
        return [False, 'Non ho trovato nulla']



def GetVideoFile( idX ):
	# questo parsa il json che arriva dal nuovo servizio mediaurlservice
	# che parte dalla AppUtils di Erich

        logger.debug(" ---------------------- INIZIO GetVideoFile  ------------------ ")
	result = False
	duration = ''
        playJson = {}
	[ result, playJson ] = GetJson(idX)
	#logger.debug( 'GetAudioFile playJson = ' + str(playJson ))
	#print( 'GetAudioFile playJson = ' + str(playJson ))
	
	if not result :
		return[ False, playJson, '' ]	
	
	if not 'article' in playJson :
		return[ False, playJson , '']	
	logger.debug( '.')
	if 'duration' in playJson['article'] :
		logger.debug('prendo duration dal Json = ' + str(playJson['article']['duration']))
		duration = str(playJson['article']['duration'])
		return[True, playJson , duration]
	
	if not 'mediaProfiles' in playJson['article'] :
		return[ False, playJson, duration ]	
	else:
		logger.debug( '..')
		listaMediaProfiles = playJson['article']['mediaProfiles']
		audioJson = {}

		for lis in listaMediaProfiles:
			if 'mime-type' in lis and 'mpeg' in lis['mime-type']:
				if 'uri' in lis:
					logger.info('GetAudioFile Trovato file path = ' + lis['uri'] )
					if len(  lis['uri'] )  > 0 :
						return [ True, lis['uri'], duration ]
					else:
						return[False, playJson , duration]


	logger.debug(" ---------------------- FINE GetVideoFile  ------------------ ")
        return [False, 'Non ho trovato nulla', duration ]




def GetAudioFile( idX ):
	# questo parsa il json che arriva dal nuovo servizio mediaurlservice
	# che parte dalla AppUtils di Erich

        logger.debug(" ---------------------- INIZIO GetAudioFile  ------------------ ")
	result = False
	duration = ''
        playJson = {}
	[ result, playJson ] = GetJson(idX)
	#logger.debug( 'GetAudioFile playJson = ' + str(playJson ))
	#print( 'GetAudioFile playJson = ' + str(playJson ))
	
	if not result :
		return[ False, playJson, '' ]	
	
	if not 'article' in playJson :
		return[ False, playJson , '']	
	logger.debug( '.')
	if 'duration' in playJson['article'] :
		logger.debug('prendo duration dal Json = ' + str(playJson['article']['duration']))
		duration = str(playJson['article']['duration'])
		return[True, playJson , duration]
	
	if not 'mediaProfiles' in playJson['article'] :
		return[ False, playJson, duration ]	
	else:
		logger.debug( '..')
		listaMediaProfiles = playJson['article']['mediaProfiles']
		audioJson = {}

		for lis in listaMediaProfiles:
			if 'mime-type' in lis and 'mpeg' in lis['mime-type']:
				if 'uri' in lis:
					logger.info('GetAudioFile Trovato file path = ' + lis['uri'] )
					if len(  lis['uri'] )  > 0 :
						return [ True, lis['uri'], duration ]
					else:
						return[False, playJson , duration]


	logger.debug(" ---------------------- FINE GetAudioFile  ------------------ ")
        return [False, 'Non ho trovato nulla', duration ]




def GetJsonFromPlay( url, idX ):

        logger.debug(" ---------------------- INIZIO GetJSON  ------------------ ")
        result = {}

        try:

                url_per_info = os.environ['PLAY_INFO'] + str(idX)
		# CLADDDDDD DEBUGGGG DA TOGLIEREEEE
                url_per_info = "http://www.rsi.ch/rsi-api/intlay/srgplay/play/" + str(idX)
                url_per_info = url + str(idX)

                meta = {}
                headers = {}
                param = { }

                headers =  { 'Accept': 'application/json'}

                requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

                #print requ
                #print requ.text
                # bind your stream
                logger.debug('\r\n')
                result = json.loads( requ.text )
		if 'statusCode' in result:
			logger.error('Errore: ' + str(idX) + ' - statusCode: ' + result['statusCode'] )	
			logger.error(json.dumps(result, indent=4))
		if 'error' in result:
			raise Exception('Errore: ' + result['error'] )	
                logger.debug(json.dumps(result, indent=4))
                logger.debug(" ---------------------- FINE GetJSON  ------------------ ")

        except Exception as e:
		logger.error(' .. ')
                logger.error('ERROR: EXCEPT in GetJSON  = ' + str(e))
		return [ False, str(e) ]
                pass
        return [True, result]





def GetJson( idX ):

        logger.debug(" ---------------------- INIZIO GetJSON  ------------------ ")
        result = {}

        try:

                url_per_info = os.environ['PLAY_INFO'] + str(idX)
		# CLADDDDDD DEBUGGGG DA TOGLIEREEEE
                url_per_info = "http://presentation5.rsi.ch/rsi-api/mediaurlservice/article/" + str(idX)

                meta = {}
                headers = {}
                param = { }

                headers =  { 'Accept': 'application/json'}

                requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

                #print requ
                #print requ.text
                # bind your stream
                logger.debug('\r\n')
                result = json.loads( requ.text )
		if 'statusCode' in result:
			logger.error('Errore: ' + str(idX) + ' - statusCode: ' + result['statusCode'] )	
			logger.error(json.dumps(result, indent=4))
		if 'error' in result:
			raise Exception('Errore: ' + result['error'] )	
                logger.debug(json.dumps(result, indent=4))
                logger.debug(" ---------------------- FINE GetJSON  ------------------ ")

        except Exception as e:
		logger.error(' .. ')
                logger.error('ERROR: EXCEPT in GetJSON  = ' + str(e))
		return [ False, str(e) ]
                pass
        return [True, result]




if __name__ == "__main__":

	keyframeJson = GetJson('https://www.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json')
	print keyframeJson
	exit(0)

	jsontoload = {u'GP765997': [2055479 ,2055473, 2055475], u'GP765666': [205566 ,665473, 205665] }
	print PostJson( jsontoload, 'http://publishing.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json' )



