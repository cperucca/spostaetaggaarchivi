import os 

# -*- coding: utf-8 -*-

import logging
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import Helpers
import shutil
import glob
import json
from datetime import datetime, timedelta
import pySpin
import ast
import requests

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

logger = logging.getLogger('pyECE')

for entry in namespaces:
	#logger.debug(entry, namespaces[entry])
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry


def Dump_ET_Link ( id, filename ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(id)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	tree = ET.parse(result)
	tree.write(filename)

	return



def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	return

def Put_Link( link, filename ):
	
	logger.debug('------------------------ inizia Put_Link -------------- ')

        base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
        file = open(filename)
        dati = file.read()

        request = urllib2.Request(link, data=dati)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'PUT'
        result = urllib2.urlopen(request)

        #logger.debug(result.read())
        return
        xml = minidom.parse(result)
        fout = codecs.open(filename, 'w', 'utf-8')
        fout.write( xml.toxml() )
        fout.close()


        return

def Put_IdProd( idx, filename ):

	logger.debug('------------------------ inizia Put_IdProd -------------- ')
	try:
		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		print dati
		link = os.environ['ECE_SERVER']  + str(idx)

		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		#prod_ece = 'http://presentation2.rsi.ch/webservice/escenic/content/'
		link = prod_ece  + str(idx)
		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.debug('Put_Id' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print 'PROBLEMI in Put_Id : ' + str(e)
		return False

	logger.debug('------------------------ finisce Put_IdProd -------------- ')
        return True


def putId( idx, filename ):

	logger.debug('------------------------ inizia putId -------------- ')
	try:
		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.debug('putId' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print 'PROBLEMI in putId : ' + str(e)
		return False

	logger.debug('------------------------ finisce putId -------------- ')
        return True





def Put_Id( idx, filename ):

	logger.debug('------------------------ inizia Put_Id -------------- ')
	try:
		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		request = urllib2.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib2.urlopen(request)

		logger.debug('Put_Id' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print 'PROBLEMI in Put_Id : ' + str(e)
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
        return True


def PutJsonToRest( dati, path, nomefile ):

	logger.debug( '------------------------ inizia PutJsonToRest -------------- ' )
	#print 'dati : ' + str(dati) 
	try:
		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		stag_ece = 'https://sslstagingapp.rsi.ch/rsi-api/intlay/test/writefblands'
		prod_ece = 'http://www.rsi.ch/rsi-api/fblikesandshares/writefblands'
		

		
		#link = os.environ['ECE_SERVER']  + str(idx)
		link = os.environ['ECE_REST']


		logger.debug( 'path : ' + path )
		logger.debug( 'nomefile : ' + nomefile )
		headers = {
		'rsiauthentication': 'fb14nd5ru135',
		'pathfromheader': path,
		'nomefilefromheader': nomefile
		}    


		r = requests.post(link, headers=headers, json=dati)
		logger.debug( 'risultato post su :' + prod_ece + ' con autentication ' ) 
		logger.debug( r.status_code  )
		resultJson = r.json() 
		logger.debug( resultJson )
	
	except Exception as e:
		logger.debug( 'PROBLEMI in PutJsonToRest : ' + str(e) )
		return False

	logger.debug('------------------------ finisce PutJsonToRest -------------- ')
        return True


if __name__ == "__main__":


	os.environ['ECE_USER'] = 'TSMM'
	os.environ['ECE_PWD'] = '8AKjwWXiWAFTxb2UM3pZ'

	Dump_Link( 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/161451', './161451.xml')
	exit(0)
	#Put_Link( 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/2748631', './2748631.xml')
	#exit(0)

	os.environ['ECE_SERVER'] = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'

	Put_IdProd( 161451, '/home/perucccl/Webservices/STAGING/TransAudioDuration/_cambiamento_')
	exit(0)

	dati  = { "chiave":"valore"}
	PutJsonToRest( dati  )

