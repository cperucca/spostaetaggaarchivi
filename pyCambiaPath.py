
# -*- coding: utf-8 -*-


import logging
import logging.config

import ast
import os
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import json
from datetime import datetime, timedelta
import time
import copy

import glob
import codecs

import ECE.Crea_List_Archivi as Crea_List_Archivi
import ECE.Prendi_Content_Da_ECE as Prendi_Content_Da_ECE
import ECE.Put_Content_In_ECE as Put_Content_In_ECE
import ECE.GetRestJson as GetRestJson
import Resources.pySettaVars as pySettaVars
import Helpers.pyCallFFProbe as pyCallFFProbe
import listaArchivi as listaArchivi
import listaDaFare as listaDaFare
import listaDaSpostare as listaDaSpostare
import listaKeyPic as listaKeyPic
import listaProgDaCambiare as listaProgDaCambiare
import listaKeyDaSpostare as listaKeyDaSpostare



def Croppa_Titolo( titolo, link ):

	logger.debug(len(titolo))
	
	
	delta = len( link) + 5
	# il delta e +5 se con i puntini mentre e 
	# +1 se senza puntini

	
	if len(titolo) + delta > 138:
		logger.debug(' > 138')
 
		puntini = u'...'
	else:
		logger.debug(' < 138')
		puntini = u''

	while len( titolo ) + delta > 138:
		#logger.debug(titolo.split(' '))
		#logger.debug(titolo.split(' ')[-1])
		lunghezza = -(len(titolo.split(' ')[-1]) + 1)
		#logger.debug(lunghezza)
		titolo = titolo[0:lunghezza]
		#logger.debug(len(titolo))
		#logger.debug(titolo)

	#logger.debug(titolo + '...')
	titolo = titolo + puntini

	#logger.debug(titolo + u' ' + link)

	return titolo + u' ' + link

def Croppa_Coda_Testo( testo ):

	_debugga_ = True

	if _debugga_ :
		logger.debug(len(testo))

	while len( testo ) > 138:
		#logger.debug(testo.split(' '))
		#logger.debug(testo.split(' ')[-1])
		lunghezza = -(len(testo.split(' ')[-1]) + 1)

		#logger.debug(lunghezza)
		testo = testo[0:lunghezza]
		if _debugga_ :
			logger.debug(len(testo))
			logger.debug(testo)

	if _debugga_ :
		logger.debug(testo + '...')


	return testo + '...'




def Componi_Testo_Limite( name, link, desc ):
	# questa limita a 140 caratteri quello che vogliamo buttare su
	totale = name + u' ' + link + u' ' + desc 	
	logger.debug(totale)

	if len(totale) > 138:
		if len(name + u' ' + link) < 138:
			#logger.debug('titolo troppo lungo ma croppo solo in fondo')
			totale = Croppa_Coda_Testo( totale )
		else:
			# vuol dire che devo togliere dal titolo il numerodi caratteri del link 
			# piu tre puntini che aggiungero in fondo
			#logger.debug('devo cavare dal titolo link e tre puntini')
			totale = Croppa_Titolo( name, link )
			
	logger.debug('result = ' + totale)
	return totale


#Componi_Testo_Limite(u'TESTTitle: test 4.0 post su social',u'http://www.rsi.ch/g/8845033' ,  u'TESTSubTitle: n insetticidi e distribuite a oltre 578 milioni di')
#Componi_Testo_Limite(u'TESTTitle: test 4.0 post su social',u'http://www.rsi.ch/g/8845033' ,  u'TESTSubTitle: test 4.0 post su social ata su più fronti. Tra il 2008 e il 2010 cè stato un progressivo aumento del numero di zanzariere trattate con insetticidi e distribuite a oltre 578 milioni di')
#Componi_Testo_Limite(u'TESTSubTitle: test 4.0 post su social ata su più fronti. Tra il 2008 e il 2010 cè stato un progressivo aumento del numero di zanzariere trattate con insetticidi e distribuite a oltre 578 milioni di', u'http://www.rsi.ch/g/8845033' , u'TESTTitle: test 4.0 post su social')
#exit(0)


def ChooseAccount( da_fare_under, lis ):

	logger.debug(' ------------- INIT ChooseAccount --------------------------------------- ')
	abs_account = None
	msg = u''
	fb_title = ''
	fb_alt_desc = ''

	if 'fb' in da_fare:
		try :
			logger.debug('in FB')
			abs_account = pyFacebookClass.FacebookConnection( da_fare_under )

			# logger.debug(abs_account)
			# e devo anche mettere il testo giusto
			# per facebook ci potrebbero anche stare 
			#'link':'http://www.rsi.ch', oppure all articolo
			#'picture':'http://pngimg.com/upload/water_PNG3290.png',
			#'name':'NAME WATER',
			#'description':'DESCRIPTION WATER',
			#'caption':'CAPTION WATER'
			# da vedere

			if lis['social_flags']['facebookAltTitle'] is None  or 'None' in lis['social_flags']['facebookAltTitle'] :
				logger.debug(' 1 ' )
			else:
				logger.debug(' 2 ')

			if not (lis['social_flags']['facebookAltTitle'] is None  or 'None' in lis['social_flags']['facebookAltTitle'] ):
				logger.debug('nome = title')
				lis['lead']['name'] = lis['social_flags']['facebookAltTitle'].strip()
			else:
				logger.debug('facebookAltTitle is None')

			logger.debug(' in mezzo')

			if not (lis['social_flags']['facebookAltDesc'] is None or 'None' in lis['social_flags']['facebookAltDesc'] ):
				logger.debug('desc = desc')
				lis['lead']['description'] = lis['social_flags']['facebookAltDesc'].strip() 
			else:
				logger.debug('facebookAltDesc is None')


			logger.debug(lis['lead'])

			if not (lis['social_flags']['facebookText'] is None):
				msg = lis['social_flags']['facebookText'].strip()
			else:
				logger.debug('facebookText is None')
			logger.debug('try msg del Facebook = ' + msg)
		except:
			logger.debug('except  msg del Facebook = ' + msg)
			return [ None, '', '','']
	else:
		try:
			logger.debug('in TW')
			abs_account = pyTwitterClass.TwitterConnection( da_fare_under )
			logger.debug(' preso account ' + str(abs_account))

			logger.debug(lis['lead'])

			# e devo anche mettere il testo giusto
			logger.debug(' sta per strippare ')
			if lis['social_flags']['twitterText'] is None:
				logger.debug('lis e NONE')
			if not (lis['social_flags']['twitterText'] is None or not (lis['social_flags']['twitterText'])):
				msg = Croppa_Titolo( lis['social_flags']['twitterText'].strip() , lis['link'])
				logger.debug(' 1 - setto msg = ' + msg)

			else:
				# devo verificare su richiesta di Diego se ho l upload dell immagine
				# nel qual caso devo metterci il titolo  + url breve + sottotitolo
				logger.debug(' nella verifica della social flag')
				if  not (lis['social_flags']['tw-uploadImage'] is None ) and 'true' in lis['social_flags']['tw-uploadImage'] :
					logger.debug(' tw-uploadImage true ')
					#logger.debug(lis['link'])
					#logger.debug(lis['lead']['description'])
					#logger.debug(lis['title'])

					if lis['lead']['description'] is None:
						logger.debug('description is NONE')
						desc = ''
					else:
						desc = lis['lead']['description']
					#logger.debug(' --- ' )
					msg = Componi_Testo_Limite( lis['lead']['name'] , lis['lead']['link'] , desc )
					logger.debug(' 2 - setto msg = ' + msg)
					#logger.debug(' --- ' )
				
				else:
					# avendo cambiato il testo del msg non posso mettere SEMPRE la url in fondo
					# quindi e qui che devo aggiungerla o meno in fondo al msg
					logger.debug(' tw-uploadImage false ')
					# se saimo una short story dopbbiamo mettere il titolo nel testo
					if 'short' in lis['contenttype']:
						msg = Croppa_Titolo( lis['title'] , lis['lead']['link'])
						logger.debug(' 3 - setto msg = ' + msg)
					else:
						msg = Croppa_Titolo( '' ,  lis['lead']['link'] )
						logger.debug(' 4 - setto msg = ' + msg)
	
			logger.debug('try msg del TWitt = ')
			logger.debug(msg )
			
		except:
			logger.debug('except msg del TWitt = ' )
			logger.debug(msg)
			return [ None, []]

	logger.debug(lis['lead'])
	logger.debug(' ------------- FINE  ChooseAccount --------------------------------------- ')
	return [ abs_account, [ msg, '', '', ast.literal_eval(str(lis['lead'])) ] ]

def Aumenta_Error( val ):
	
	if val is None or ( not ( 'Errore' in val )) :
		return 'Errore 1'
	else:
		numero = val.split(':')[0]
		numero = numero.split('Errore')[-1]
		logger.debug(numero.strip())
	
	return 'Errore ' +  str( int(numero) + 1)


def SettaValue( result, lis, da_fare, _id ):

	if result:
		lis['social_flags'][da_fare.replace('account', 'sent')] =  _id
		logger.debug('aggiunto a lista' + str(da_fare.replace('account', 'sent')) +' '+  str(lis['social_flags'][da_fare.replace('account', 'sent')]))
	else:
		if '403' in str(_id):
			lis['social_flags'][da_fare.replace('account', 'sent')] =  Aumenta_Error( lis['social_flags'][da_fare.replace('account', 'sent')]) + ': tweet duplicato'
			logger.debug('da ritornare il motivo per cui non riusciamo a postare\n\n')
		else:
			lis['social_flags'][da_fare.replace('account', 'sent')] =  Aumenta_Error( lis['social_flags'][da_fare.replace('account', 'sent')]) +':' +  str(_id).split(',')[-1] 
			logger.debug('da ritornare il motivo per cui non riusciamo a postare\n\n')

def SettaEnvironment( debug ):

	if (debug):
		print 'PRIMA DI SETTARE ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ

	if (debug):
		print '---------------------------------'

	Environment_Variables = {}
	
	print 'verifico il setting della variabile _AAA_ '
	if '_AAA_' in os.environ:
		if 'PRODUCTION' in os.environ['_AAA_']:
			print 'variabile _AAA_ ha valore \'PRODUCTION\''
			print 'setto ENV di PROD'
			Environment_Variables = pySettaVars.PROD_Environment_Variables
			pySettaVars.LOGS_CONFIG_FILE = pySettaVars.PROD_CONFIG_FILE
			pySettaVars.WOW_SERVER = pySettaVars.PROD_WOW_SERVER
			print ' settato server = ' + pySettaVars.WOW_SERVER
		else:
			print 'variabile _AAA_ ha valore : ' + os.environ['_AAA_']
			print 'diverso da \'PRODUCTION\''
			print 'setto ENV di TEST'
			Environment_Variables = pySettaVars.TEST_Environment_Variables
			pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE
			pySettaVars.WOW_SERVER = pySettaVars.TEST_WOW_SERVER
			print ' settato server = ' + pySettaVars.WOW_SERVER
			
	else:
		print 'variabile _AAA_ non trovata: setto ENV di TEST'
		Environment_Variables = pySettaVars.TEST_Environment_Variables
		pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE
		pySettaVars.WOW_SERVER = pySettaVars.TEST_WOW_SERVER
		print ' settato server = ' + pySettaVars.WOW_SERVER


	for param in Environment_Variables.keys():
		#print "%20s %s" % (param,dict_env[param])
		os.environ[param] = Environment_Variables[ param ]


	if (debug):
		print 'DOPO AVER SETTATO ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ

def Smonta_Details( streaming_details ):

	# qui devo verificare che per tutti gli account ci siano sia i 
	# VideoDetails che i WowzaDetails
	# altrimenti cancellare quello dei due che manca e mettere quei i
	# valori di quegli account a nullo
	result_details = {}

	for chiave,valore in streaming_details.iteritems():
		logger.debug(chiave + ' : '  + str(valore))
		result_details[ chiave ] = valore
		if not 'VideoDetails' in chiave:
			# verifico se esiste il corrispettivo Wowza 
			# per quell account ( parte che viene dopo VideoDetails )
			logger.debug( ' passato da qui: No VideoDetails' )
			
	return result_details

def QualcunoMancaStreamingDetailsFB( item ):

	logger.debug(" ---------------------- INIZIO QualcunoMancaStreamingDetailsFB  ------------------ " )
	logger.debug(item)

	adesso = datetime.now()

	result = False

	account = item['SocialFlags'][ 'fb-livestreaming-account' ]
	logger.debug( account )
	for key,valueUp in account.iteritems():
		socialDetails = valueUp['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(valueUp ))

		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'beccato  account con notifica nel futuro :-----------> ' + key )
			return True

		if 'notificato' in socialDetails :
			continue

		# qui devo verificare che non ce ne siano dei pezzi gia' fatti
		# se ci sono gia li metto nell account_details
		if 'VideoDetails' in socialDetails and  'WowzaDetails' in socialDetails:
			continue

		result = True
			
	logger.debug(" ---------------------- FINE QualcunoMancaStreamingDetailsFB  ------------------ ")
	return result


def QualcunoMancaStreamingDetailsYT( item ):

	logger.debug(" ---------------------- INIZIO QualcunoMancaStreamingDetailsYT  ------------------ " )
	logger.debug(item)

	adesso = datetime.now()

	result = False

	account = item['SocialFlags'][ 'yt-livestreaming-account' ]
	logger.debug( account )
	for key,valueUp in account.iteritems():
		socialDetails = valueUp['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(valueUp ))

		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'beccato  account con notifica nel futuro :-----------> ' + key )
			return True

		if 'notificato' in socialDetails :
			continue

		# qui devo verificare che non ce ne siano dei pezzi gia' fatti
		# se ci sono gia li metto nell account_details
		if 'VideoDetails' in socialDetails and  'WowzaDetails' in socialDetails:
			continue

		result = True
			
	logger.debug(" ---------------------- FINE QualcunoMancaStreamingDetailsYT  ------------------ ")
	return result

def PreparaFB( item ):

	logger.debug(" ---------------------- INIZIO PreparaFB  ------------------ " )
	logger.debug(item)
	# per Preparare lo stream devo :
	# girando sugli account sotto le SocialFlags
	#
	# 1 - FB.Create_Live_Video( title, description ) che mi torna	
	# {
    	# "secure_stream_url": "rtmps://rtmp-api.facebook.com:443/rtmp/1471030776261970?ds=1&s_l=1&a=ATh1bGN96LhhKx8S",
    	# "id": "1471019276263120"
	# ..... 
	# oltre a tutto il resto
	# }

	#result_create = pyFacebookClass.Create_Live_Video( item['SocialFlags']['fbVideoTitle' ] , item['SocialFlags']['fbVideoDesc' ]  )
	# cosi considero anche eventuali cambiamenti di daysaving time 
	# prendo la data per verificare che la notifica sia passata
	adesso = datetime.now()

	t_start = item['SocialFlags']['t_start']
	
        # CLAD -> Risoluzioe problema del minuto indietro.
        # logger.debug(datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s"))
        # aggiungo un minuto per passarlo al time di Facebook
        dt_start = datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ')
        dt_start = dt_start + timedelta(minutes=1)

	# dict che conterra i Video e Wow Details
	_fb_livestreaming_details = {}
	_account_details = {}

	_fb_accounts = item['SocialFlags']['fb-livestreaming-account']
	# tra queste ce ne sono alcune che devono essere notificate

	for key,FBEntry in _fb_accounts.iteritems():
		socialDetails = FBEntry['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(FBEntry ))
		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'salto account con notifica nel futuro :-----------> ' + key )
			continue
		try:

			# qui devo verificare che non ce ne siano dei pezzi gia' fatti
			# se ci sono gia li metto nell account_details
			if 'VideoDetails' in FBEntry:
				# quindi se esiste la entry per quell account 
				# DEVE esistere il post cioe i VideoDetails devono essere inizializzati
				# allora li copio in _account_details e li uso per produrre i WowzaDetails
				# se questi non esistono
				logger.debug('setto _account_details sul valore che mi arriva da DB ' )
				_account_details['VideoDetails'] = FBEntry['VideoDetails']
			else:
					
				logger.debug('creo un nuovo _account_details :' + key )

				logger.debug(' per FB : socialDetails[videoTitle] : ' + socialDetails['videoTitle' ] )
				logger.debug(' per FB : socialDetails[videoDesc] : ' + socialDetails['videoDesc' ] )
				logger.debug(' per FB : socialDetails[isGeoBlocked] : ' + socialDetails['isGeoBlocked' ] )
				logger.debug(' per FB : socialDetails[continuousLive] : ' + str(socialDetails['continuousLive' ] ))
				logger.debug(' per FB : socialDetails[livestreamingaccount] : ' + str(socialDetails['livestreamingaccount' ] ))

				result_create = pyFacebookClass.Create_Scheduled_Video_Geo( socialDetails['videoTitle' ],socialDetails['videoDesc' ],dt_start.strftime("%s"),socialDetails['livestreamingaccount'],socialDetails['isGeoBlocked'],socialDetails['continuousLive'])

				# DEBUG CLAD
				#result_create = {u'status': u'SCHEDULED_UNPUBLISHED', u'is_manual_mode': False, u'description': u'post fb 1', u'title': u'titolo fb 1', u'secure_stream_url': u'rtmps://live-api-s.facebook.com:443/rtmp/2334040573294315?s_sw=0&s_vt=api-s&a=AbzxZkfGOfkDbYiq', u'live_views': 0, u'creation_time': u'2019-04-11T09:29:58+0000', u'seconds_left': 0, u'dash_preview_url': u'https://video-frt3-1.xx.fbcdn.net/hvideo-prn1/v/r_zg4F3-7oM_LytD7RHBF/live-dash/dash-md/2334040573294315.mpd?lvp=1&_nc_rl=AfAGK6GvSsj8rvFn&oh=40a28ac881c9dac4f57c4f20c1c73a49&oe=5CB1AE45', u'broadcast_start_time': u'2019-04-11T09:29:58+0000', u'planned_start_time': u'2019-04-17T16:00:59+0000', u'embed_html': u'<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1237091532989230%2Fvideos%2F2679365098760193%2F&width=400" width="400" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>', u'permalink_url': u'https://www.facebook.com/1237091532989230/videos/2679365098760193/', u'id': u'2334040573294315'}
				# DEBUG CLAD

				#print result_create
				_account_details['VideoDetails'] = result_create
				# 2 - passare la secure_stream_url alla create_mapentry del WOW
				# cambio lo stream name per essere quella giusta da mandare alla lib Wow
				# per adesso assumo che il Wirecast sia sempre attivo
				# poi mi preoccupero di accenderlo
				_account_details['VideoDetails']['wow_secure_stream'] =  _account_details['VideoDetails']['secure_stream_url'].split('443/rtmp/')[-1]
				#_account_details['VideoDetails']['wow_name'] =  item['ECEDetails']['id'] + '_post:' + _account_details['VideoDetails']['id'] + '_account:' + socialDetails['livestreamingaccount']
				_account_details['VideoDetails']['wow_name'] =  'article:' + key + '_post:' + _account_details['VideoDetails']['id'] + '_account:' + socialDetails['livestreamingaccount']

			# qui inizializzo la VIDEODETAILS
			logger.debug('---------------------------------- init VideoDetails ---------------------------')
			logger.debug( _account_details['VideoDetails'])
			logger.debug('----------------------------------- end VideoDetails ---------------------------')

			# devo verificare se esistono gia anche i WowzaDetails
			if 'WowzaDetails' in FBEntry:
				_account_details['WowzaDetails'] = FBEntry['WowzaDetails'] 
			else:
				# se non esiste la entry per i WowzaDetails la creo
				logger.debug(' parametri passati a pyWowzaClass.Create_Mapentry_FB')
				logger.debug( _account_details['VideoDetails']['wow_name'] + ' ' +  _account_details['VideoDetails']['wow_secure_stream']  + ' ' + socialDetails['incoming_stream'] + '_720p' + ' ' + pySettaVars.WOW_SERVER )

				# CLAD -> per FB mando su il 720p aggiungendo alla fine del item['SocialFlags']['incoming_stream'] un _720p


				wow = pyWowzaClass.Create_Mapentry_FB( _account_details['VideoDetails']['wow_name'] , _account_details['VideoDetails']['wow_secure_stream'] , socialDetails['incoming_stream'] + '_720p', pySettaVars.WOW_SERVER )
				
				# DEBUG CLAD
				# wow = {u'message': u'Entry (article:9845290_post:2334040573294315_account:testare_e_bello) saved successfully', u'data': None, u'success': True}
				# DEBUG CLAD



				logger.debug(wow)
				if 'success' in wow and wow['success'] :
					logger.debug( 'passo da wow = success' )
					# 3 - se ritornato success : true
					# 4 - chiederne i details con Get_Wowza_Mapentry_Details da salvare in lis 

					# qui inizializzo la WOWZADETAILS




					_account_details['WowzaDetails'] = pyWowzaClass.Get_Wowza_Mapentry_Details( _account_details['VideoDetails']['wow_name' ] , pySettaVars.WOW_SERVER )
					# per successive modifiche con Action_Mapentry( nome, 'enable/disable')
					# o per la semplice Delete_Mapentry( nome )

					
					result_enable = pyWowzaClass.Action_Mapentry( _account_details['VideoDetails']['wow_name' ] , 'enable', pySettaVars.WOW_SERVER )

					# DEBUG CLAD
					# result_enable = {u'message': u'Entry (article:9845290_post:2334040573294315_account:testare_e_bello) enabled successfully', u'data': None, u'success': True}
					# DEBUG CLAD

					logger.debug( result_enable)
					if result_enable['success']:
						_account_details['WowzaDetails']['enabled'] = True
				else:
					logger.debug( 'passo da wow = NOT success' ) 


			# qui inizializzo la WOWZADETAILS
			logger.debug('----------------------------------- init WowzaDetails ---------------------------')
			logger.debug( _account_details['WowzaDetails'])
			logger.debug('----------------------------------- end WowzaDetails ---------------------------')

			#logger.debug( _account_details )

			FBEntry['VideoDetails'] = copy.deepcopy( _account_details['VideoDetails'] )
			FBEntry['WowzaDetails'] = copy.deepcopy( _account_details['WowzaDetails'] )
			FBEntry['SocialDetails']['notificato'] = True

			#logger.debug(" ---------------------- DEBUG PreparaFB FBEntry ------------------ ")
			#logger.debug( 'FBEntry : ' + str( FBEntry ) )
			_account_details = {}
			
		except Exception as e:
			logger.error('ERROR: EXCEPT in PreparaFB  = ' + str(e))
			logger.error('ERROR: ' + str(_account_details))
			
			_account_details = Smonta_Details( _account_details )
			

			# Da decidere strategia gestione errori
			_account_details = {}
			# se ho fatto un errore per questo account, NON devo cancellare 
			# i VideoDetails in modo da tornare tra un minuto a cercare di 
			# attaccarci i WowDetails

			pass
		
	# qui aggiunge la parte relativa a FB
	# item['FBStreamingDetails'] = _account_details


	logger.debug(" ---------------------- DEBUG PreparaFB _fb_livestreaming_details:  ------------------ ")
	logger.debug( item['SocialFlags'] )
	logger.debug(" ---------------------- DEBUG PreparaFB  giro sugli items di _fb_livestreaming_details ------------------ ")

	# clean per il test chiama le delete delle entry create
	#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
	#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] , pySettaVars.WOW_SERVER )
	#logger.debug(wow_delete)
	# DA TOGLIERE

	logger.debug(" ---------------------- FINE PreparaFB  ------------------ ")
	
	return item


def Prendi_Id_StreamName( VideoDetailsDictionary ):
	result = None
	id = None

	if 'LivestreamsDetails' in VideoDetailsDictionary and 'cdn' in VideoDetailsDictionary['LivestreamsDetails'] and  \
		'ingestionInfo' in VideoDetailsDictionary['LivestreamsDetails']['cdn'] and 'streamName' in VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']:
		logger.debug('streamname = ' + VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName'])
		result = VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName']

	if 'BindDetails' in VideoDetailsDictionary and 'id' in VideoDetailsDictionary['BindDetails']:
		logger.debug('bind Id = ' + VideoDetailsDictionary['BindDetails']['id'])
		id = VideoDetailsDictionary['BindDetails']['id']

	return [ result , id ]



def PreparaYT( item ):

	logger.debug(" ---------------------- INIZIO PreparaYT  ------------------ " )
	logger.debug(item)



	# DEBUG CLAD
	# DEBUG CLAD

	# per Preparare lo stream devo :
	# 1 - YT.Create_Live_Video( title, description ) che mi torna	
	# {
    	# "secure_stream_url": "rtmps://rtmp-api.facebook.com:443/rtmp/1471030776261970?ds=1&s_l=1&a=ATh1bGN96LhhKx8S",
    	# "id": "1471019276263120"
	# ..... 
	# oltre a tutto il resto
	# }

	#result_create = pyFacebookClass.Create_Live_Video( item['SocialFlags']['fbVideoTitle' ] , item['SocialFlags']['fbVideoDesc' ]  )
	# cosi considero anche eventuali cambiamenti di daysaving time 
	# prendo la data per verificare che la notifica sia passata
	adesso = datetime.now()

	t_start = item['SocialFlags']['t_start']
		
        # CLAD -> Risoluzioe problema del minuto indietro.
        # logger.debug(datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s"))
        # aggiungo un minuto per passarlo al time di Facebook
        dt_start = datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ')
        dt_start = dt_start + timedelta(minutes=1)

	logger.debug(datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s"))

	_yt_livestreaming_details = {}
	_account_details = {}

	_yt_accounts = item['SocialFlags']['yt-livestreaming-account']
	# tra queste ce ne sono alcune che devono essere notificate

	for key,YTEntry in _yt_accounts.iteritems():
		socialDetails = YTEntry['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(YTEntry ))

		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'salto account con notifica nel futuro :-----------> ' + key )
			continue
	
		try:

			# qui devo verificare che non ce ne siano dei pezzi gia' fatti
			# se ci sono gia li metto nell account_details
			if 'VideoDetails' in YTEntry:
				# quindi se esiste la entry per quell account 
				# DEVE esistere il post cioe i VideoDetails devono essere inizializzati
				# allora li copio in _account_details e li uso per produrre i WowzaDetails
				# se questi non esistono
				logger.debug('setto _account_details sul valore che mi arriva da DB ' )
				_account_details['VideoDetails'] = YTEntry['VideoDetails']
			else:
				logger.debug('creo un nuovo _account_details :' + key )

				logger.debug(' per YT : socialDetails[videoTitle] : ' + socialDetails['videoTitle' ] )
				logger.debug(' per YT : socialDetails[videoDesc] : ' + socialDetails['videoDesc' ] )
				logger.debug(' per YT : socialDetails[isGeoBlocked] : ' + socialDetails['isGeoBlocked' ] )
				logger.debug(' per YT : socialDetails[continuousLive] : ' + str(socialDetails['continuousLive' ] ))
				logger.debug(' per YT : socialDetails[livestreamingaccount] : ' + str(socialDetails['livestreamingaccount' ] ))

				yt_account = socialDetails['livestreamingaccount'].replace('-','_')
				result_create = pyYoutubeClass.Crea_And_Bind_Livestream_Event( socialDetails['videoTitle' ] , socialDetails['videoDesc' ], datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m-%dT%H:%M:%S.000Z') , 'public', socialDetails['continuousLive' ] , yt_account )

				#result_create = {u'LivestreamsDetails': {u'status': {u'streamStatus': u'ready', u'healthStatus': {u'status': u'noData'}}, u'kind': u'youtube#liveStream', u'cdn': {u'resolution': u'1080p', u'ingestionType': u'rtmp', u'ingestionInfo': {u'ingestionAddress': u'rtmp://a.rtmp.youtube.com/live2', u'streamName': u't2fp-cwzg-z646-2zk0', u'backupIngestionAddress': u'rtmp://b.rtmp.youtube.com/live2?backup=1'}, u'frameRate': u'30fps', u'format': u'1080p'}, u'snippet': {u'isDefaultStream': False, u'channelId': u'UCv7yEZoifGFg_CioakqBMFA', u'description': u'Test_Description', u'publishedAt': u'2017-06-01T13:50:02.000Z', u'title': u'Stream_per_Test_CLAD.2.0'}, u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/2aA49S91KnshlTkN2tMSEmcESAE"', u'id': u'v7yEZoifGFg_CioakqBMFA1496325002526255'}, u'BindDetails': {u'contentDetails': {u'closedCaptionsType': u'closedCaptionsDisabled', u'projection': u'rectangular', u'startWithSlate': False, u'boundStreamId': u'v7yEZoifGFg_CioakqBMFA1496325002526255', u'enableEmbed': False, u'enableClosedCaptions': False, u'enableLowLatency': False, u'boundStreamLastUpdateTimeMs': u'2017-06-01T13:50:02.537Z', u'enableContentEncryption': False, u'recordFromStart': True, u'enableDvr': True, u'monitorStream': {u'broadcastStreamDelayMs': 0, u'embedHtml': u'<iframe width="425" height="344" src="https://www.youtube.com/embed/zEa_vQr6v7A?autoplay=1&livemonitor=1" frameborder="0" allowfullscreen></iframe>', u'enableMonitorStream': True}}, u'kind': u'youtube#liveBroadcast', u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/rPXg9ooOujATuCxHtve5Tt0Pqmg"', u'id': u'zEa_vQr6v7A'}, u'LivebroadcastDetails': {u'snippet': {u'thumbnails': {u'default': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/default_live.jpg?sqp=CIi3wMkF&rs=AOn4CLD-OH957yBGBrNOZB1s-_Wqe8gq0Q', u'width': 120, u'height': 90}, u'high': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/hqdefault_live.jpg?sqp=CIi3wMkF&rs=AOn4CLBi97H8jZLxX3S1EHXKTiuZijOMtg', u'width': 480, u'height': 360}, u'medium': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/mqdefault_live.jpg?sqp=CIi3wMkF&rs=AOn4CLB_LuQaBEamvFwnHdAGGy3Zun6Fdg', u'width': 320, u'height': 180}}, u'title': u'Broad_per_Test_CLAD.2.0', u'channelId': u'UCv7yEZoifGFg_CioakqBMFA', u'publishedAt': u'2017-06-01T13:50:03.000Z', u'liveChatId': u'Cg0KC3pFYV92UXI2djdB', u'scheduledStartTime': u'2017-06-01T22:00:00.000Z', u'isDefaultBroadcast': False, u'description': u'Create_Broadcast_Description'}, u'status': {u'recordingStatus': u'notRecording', u'privacyStatus': u'private', u'lifeCycleStatus': u'created'}, u'kind': u'youtube#liveBroadcast', u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/ikqqsSV_d94FW2DIvGSVREyQa4E"', u'id': u'zEa_vQr6v7A'}}


				# qui inizializzo la VIDEODETAILS con un dizionario che mi arriva dalla create e bind di youtube
				# formata come segue:  { 'LivestreamsDetails':livestreams_res,'LivebroadcastDetails':livebroadcast_res,'BindDetails':bind_livebroadcast}

				_account_details['VideoDetails'] = result_create
				logger.debug('----------------------------------- first VideoDetails ---------------------------')
				logger.debug( _account_details['VideoDetails' ])
				logger.debug('----------------------------------- first VideoDetails ---------------------------')

				# 2 - passare la secure_stream_url alla create_mapentry del WOW
				# cambio lo stream name per essere quella giusta da mandare alla lib Wow
				# per adesso assumo che il Wirecast sia sempre attivo
				# poi mi preoccupero di accenderlo
				[ streamName , details_id ] = Prendi_Id_StreamName( _account_details['VideoDetails'] )
				_account_details['VideoDetails']['wow_secure_stream'] = streamName 
				#_account_details['VideoDetails']['wow_name'] =  item['ECEDetails']['id'] + '_post:' + details_id + '_account:' + socialDetails['livestreamingaccount']
				_account_details['VideoDetails']['wow_name'] =  'article:' + key + '_post:' + details_id + '_account:' + socialDetails['livestreamingaccount']


			logger.debug('---------------------------------- init VideoDetails ---------------------------')
			logger.debug( _account_details['VideoDetails']['wow_name'] + ' ' +  _account_details['VideoDetails']['wow_secure_stream']  + ' ' + socialDetails['incoming_stream'] + '_1080p' + ' ' + pySettaVars.WOW_SERVER )
			logger.debug('----------------------------------- end VideoDetails ---------------------------')


			# devo verificare se esistono gia anche i WowzaDetails
			if 'WowzaDetails' in YTEntry:
				_account_details['WowzaDetails'] = YTEntry['WowzaDetails'] 
			else:

				# CLAD -> per YT mando su il 1080p aggiungendo alla fine del item['SocialFlags']['incoming_stream'] un _1080p
				wow = pyWowzaClass.Create_Mapentry_YT( _account_details['VideoDetails']['wow_name'] , _account_details['VideoDetails']['wow_secure_stream'], socialDetails['incoming_stream'] + '_1080p', pySettaVars.WOW_SERVER )
				logger.debug(wow)
				if wow['success'] :
					# 3 - se ritornato success : true
					# 4 - chiederne i details con Get_Wowza_Mapentry_Details da salvare in lis 

					_account_details['WowzaDetails'] = pyWowzaClass.Get_Wowza_Mapentry_Details( _account_details['VideoDetails']['wow_name' ] , pySettaVars.WOW_SERVER )
					# per successive modifiche con Action_Mapentry( nome, 'enable/disable')
					# o per la semplice Delete_Mapentry( nome )

					
					result_enable = pyWowzaClass.Action_Mapentry( _account_details['VideoDetails']['wow_name' ] , 'enable', pySettaVars.WOW_SERVER )

					# DEBUG CLAD
					# result_enable = {u'message': u'Entry (article:9845290_post:2334040573294315_account:testare_e_bello) enabled successfully', u'data': None, u'success': True}
					# DEBUG CLAD

					logger.debug( result_enable)
					if result_enable['success']:
						_account_details['WowzaDetails']['enabled'] = True
				else:
					logger.debug( 'passo da wow = NOT success' ) 



			# qui inizializzo la WOWZADETAILS
			logger.debug('----------------------------------- init WowzaDetails ---------------------------')
			logger.debug( _account_details['WowzaDetails'])
			logger.debug('----------------------------------- end WowzaDetails ---------------------------')
		
			#logger.debug( _account_details )
			# qui devo verificare che non ce ne siano dei pezzi gia' fatti
			# se ci sono gia li metto nell account_details
			if 'GeoBlockDetails' in YTEntry and 'OwnerDetails' in YTEntry['GeoBlockDetails'] and  len(YTEntry['GeoBlockDetails']['OwnerDetails']) > 0 and 'AssetDetails' in YTEntry['GeoBlockDetails'] and  len(YTEntry['GeoBlockDetails']['AssetDetails']) > 0 and 'ClaimDetails'  in YTEntry['GeoBlockDetails'] and  len(YTEntry['GeoBlockDetails']['ClaimDetails']) >  0:
				# quindi se esiste la entry per quell account 
				# DEVONO  esistere i valori di GeoBlockDetails
				# allora verifica che ci siano tutti e 
				# allora li copio in _account_details e li uso per produrre i WowzaDetails
				# se questi non esistono
				logger.debug('setto _account_details sul valore che mi arriva da DB ' )

				_account_details['GeoBlockDetails'] = YTEntry['GeoBlockDetails']
			else:
				logger.debug( ' creo nuovi Asset Ownership Claim se Geo')  
				if socialDetails['isGeoBlocked'] == True or socialDetails['isGeoBlocked'] == 'true' or 'True' in socialDetails['isGeoBlocked']:
					# sono geobloccato devo far partire la pantomima della ownership
					# chiamando la procedura di pyYoutubeClass
					logger.debug( ' creo nuovi Asset Ownership Claim perche GEO')
					_account_details['GeoBlockDetails'] = pyYoutubeClass.Crea_Asset_Ownership_Claim( _account_details['VideoDetails']['LivebroadcastDetails']['id'], socialDetails['videoTitle'], 'yt_account_test_elvezia', YTEntry ) 

					if 'error' in _account_details['GeoBlockDetails']:
						logger.error('ERROR: in Crea_Asset_Ownership_Claim  = ' + str(_account_details['GeoBlockDetails']['error'] ))
						_account_details = Smonta_Details( _account_details )
						_account_details = {}
						continue
				else:
					logger.debug( ' metto GeoDetails a {}  perche NON GEO')
					_account_details['GeoBlockDetails'] = {}


			# qui inizializzo la GeoBlockDetails
			logger.debug('----------------------------------- init GeoBlockDetails ---------------------------')
			logger.debug( _account_details['GeoBlockDetails'])
			logger.debug('----------------------------------- end GeoBlockDetails ---------------------------')
				
			if 'WowzaDetails' in _account_details and 'VideoDetails' in _account_details:
				videoDetails = _account_details['VideoDetails']
				logger.debug( 'CLAD -> id = ' + videoDetails['LivestreamsDetails']['id'] )

				# qui per accendere devo verificare ( con timeout ? ) che il livestreaming sia active
				# e quindi settare poi il broadcast 

				if not 'active' in videoDetails['LivestreamsDetails']['status']['streamStatus'] :
					args = ()
					kwargs = { 'status_param' : 'active', 'item_id' : videoDetails['LivestreamsDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
					if not wait_until(  pyYoutubeClass.Check_LiveStream_Status, 15, 2.00,*args, **kwargs ):
						# qui lo smonta details deve essere probabilmente piu furbo
						# per non trovarci in situazioni inconsistenti
						# o forse non devo proprio smontare nulla .... basta mettere 
						# videoDetails['LivestreamsDetails']['status']['streamStatus'] = 'ready'
						# per reiniziare a fare i giri giusti ?
						_account_details = Smonta_Details( _account_details )
						_account_details = {}
						continue

				logger.debug( 'passo primo check')
				# e setto il valore di status 
				videoDetails['LivestreamsDetails']['status']['streamStatus'] = 'active'
				

				# verificare che sia veramente a testing
				# per non rifarlo dopo che e' gia stato fatto
				if not 'testing' in videoDetails['LivebroadcastDetails']['status']['lifeCycleStatus'] and not pyYoutubeClass.Check_LiveBroadcast_Status( 'testing', videoDetails['LivebroadcastDetails']['id'],socialDetails['livestreamingaccount']):
				
					result_update = pyYoutubeClass.Update_LiveBroadcast(  videoDetails['LivebroadcastDetails']['id'], "testing" , socialDetails['livestreamingaccount'], socialDetails['continuousLive' ] )

					# verificare che sia veramente a testing
					args = ()
					kwargs = { 'status_param' : 'testing', 'item_id' : videoDetails['LivebroadcastDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
					if not wait_until( pyYoutubeClass.Check_LiveBroadcast_Status, 25, 1.30,*args, **kwargs ):
						_account_details = Smonta_Details( _account_details )
						_account_details = {}
						continue
			
					# e setto il valore di status 
					videoDetails['LivebroadcastDetails']['status']['lifeCycleStatus'] = 'testing'


			YTEntry['GeoBlockDetails'] = copy.deepcopy( _account_details['GeoBlockDetails'] )
			YTEntry['VideoDetails'] = copy.deepcopy( _account_details['VideoDetails'] )
			YTEntry['WowzaDetails'] = copy.deepcopy( _account_details['WowzaDetails'] )
			YTEntry['SocialDetails']['notificato'] = True

			_account_details = {}

		except Exception as e:

			logger.error('ERROR: EXCEPT in PreparaYT  = ' + str(e))
			_account_details = Smonta_Details( _account_details )
			_account_details = {}
			# se ho fatto un errore per questo account, devo cancellare 
			# i VideoDetails di quell'account SOLAMENTE e la entry su YT ed eventualmente 
			# le entry sul Wowza e i WowzaDetails
			pass


	logger.debug(" ---------------------- DEBUG PreparaYT  ------------------ ")
	logger.debug(item )
	logger.debug(" ---------------------- DEBUG PreparaYT  ------------------ ")

	logger.debug(" ---------------------- FINE PreparaYT  ------------------ ")
	return item

def VerificaON( item ):


	FB = True
	YT = True

	logger.debug('  ')

	if len(item['SocialFlags']['fb-livestreaming-account']) > 0:


		_fb_accounts = item['SocialFlags']['fb-livestreaming-account']
		for key,FBEntry in _fb_accounts.iteritems():

			if not 'VideoDetails' in FBEntry:
				# poi magari questa e condizione di cancellazione
				logger.debug('\n')
				FB = FB and False
				continue

			if not 'status' in FBEntry['VideoDetails']:
				logger.debug('\n')
				FB = FB and False
				continue


			logger.debug('- Status item FB - ' +  key + ' : ' + FBEntry['VideoDetails']['wow_name'] + ' = ' + FBEntry['VideoDetails']['status'])

			if 'LIVE' in FBEntry['VideoDetails']['status']:

				logger.debug('- Status item FB - ' +  key + ' : LIVE con FB !! ')
				FB = FB and True
				continue
			else:
				FB = FB and False
				continue
	
	# per guardare se e' on cerco nel Video Details se lo stato e
	# LIVE altrimenti
	if len(item['SocialFlags']['yt-livestreaming-account']) > 0:
		_yt_accounts = item['SocialFlags']['yt-livestreaming-account']
		for key,YTEntry in _yt_accounts.iteritems():

			if not 'VideoDetails' in YTEntry:
				# poi magari questa e condizione di cancellazione
				logger.debug('\n')
				YT = YT and False
				continue

			if not 'LivebroadcastDetails' in YTEntry['VideoDetails']:
				logger.debug('\n')
				YT = YT and False
				continue
				
			if not 'status' in YTEntry['VideoDetails']['LivebroadcastDetails']:
				logger.debug('\n')
				YT = YT and False
				continue
				
			if not 'lifeCycleStatus' in YTEntry['VideoDetails']['LivebroadcastDetails']['status']:
				logger.debug('\n')
				YT = YT and False
				continue

			if 'wow_name' in YTEntry['VideoDetails']:
				logger.debug('- Status item YT - ' +  key + ' : '  +  YTEntry['VideoDetails']['wow_name'] + ' = ' + YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'])
			else:
				logger.debug('- Status item YT - ' +  key + ' : '  + YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'])
				logger.debug(' OCIO che manca WOW_NAME !!! ' )
				
			if 'live' == YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus']:
					logger.debug( '- Status item YT - ' +  key + ' : LIVE con YT !! ')
					YT = YT and True
					continue

			if 'liveStarting' in YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus']:
				# vado a verificare se sono passato a live prendendo lo status del liveBroadcast
				args = ()
				kwargs = { 'status_param' : 'live', 'item_id' : YTEntry['VideoDetails']['LivebroadcastDetails']['id'], 'account' : YTEntry['SocialDetails']['livestreamingaccount'] }
				if not wait_until( pyYoutubeClass.Check_LiveBroadcast_Status, 25, 1.30,*args, **kwargs ):
					logger.debug( '- Status item YT - ' +  key + ' : LIVESTARTING con YT !! ')
					YT = YT and True
					continue
				else:
					YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] = 'live'
					logger.debug( '- Status item YT - ' +  key + ' : LIVE con YT !! ')
					YT = YT and True
					continue
			else:
				YT = YT and False
				continue
		
	logger.debug('  ')
	logger.debug( str(FB) + ' ' +  str(YT ))
	return [ FB, YT ]

def AccendiFB( item ):

	logger.debug(" ---------------------- INIZIO AccendiFB  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# 
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna

	if len(item['SocialFlags']['fb-livestreaming-account']) > 0:
		if 'LIVE' == item['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] :
			logger.debug(' in AccendiFB siamo LIVE !! ')
			return item


		# se sono qui devo accendere il Wow


		_fb_accounts = item['SocialFlags']['fb-livestreaming-account']
		# tra queste ce ne sono alcune che devono essere notificate
		for key,FBEntry in _fb_accounts.iteritems():
			socialDetails = FBEntry['SocialDetails']
			videoDetails = FBEntry['VideoDetails']
			wowzaDetails = FBEntry['WowzaDetails']

			logger.debug(' giro in AccendiFB con account = ' + key)

			'''
			result_enable = pyWowzaClass.Action_Mapentry( item['FBStreamingDetails'][_fb_accounts]['VideoDetails']['wow_name' ] , 'enable', pySettaVars.WOW_SERVER )

			if result_enable['success']:
				item['FBStreamingDetails'][_fb_accounts]['WowzaDetails']['enabled'] = True

			'''
			if wowzaDetails['enabled'] : 
				#result_update = pyFacebookClass.Update_Live_Video_Details(  videoDetails['id'], "status", "LIVE_NOW" , socialDetails['livestreamingaccount'] )
				result_update = pyFacebookClass.Update_Live_Video_Details(  videoDetails['id'], "status", "SCHEDULED_LIVE" , socialDetails['livestreamingaccount'] )

			if 'error' in result_update:
				videoDetails['status'] = 'LIVE_STOPPED'
				return item


			videoDetails['status'] = result_update['status']
			logger.debug(" ---------------------- DEBUG AccendiFB  ------------------ ")
			logger.debug("FBEntry['VideoDetails'] ")
			logger.debug(videoDetails)


	logger.debug(" ---------------------- FINE AccendiFB  ------------------ ")
	return item


def wait_until(somepredicate, timeout, period=0.25, *args, **kwargs):

	mustend = time.time() + timeout
	while time.time() < mustend:
		if somepredicate(*args, **kwargs): 
			return True
		time.sleep(period)
	return False

def AccendiYT( item ):

	logger.debug(" ---------------------- INIZIO AccendiYT  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# 
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna

	if len(item['SocialFlags']['yt-livestreaming-account']) > 0:
		if 'live' in item['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] :
			logger.debug(' in AccendiYT siamo LIVE !! ')
			return item


		# se sono qui devo accendere il Wow

		_yt_accounts = item['SocialFlags']['yt-livestreaming-account']

		for key,YTEntry in _yt_accounts.iteritems():
			socialDetails = YTEntry['SocialDetails']
			videoDetails = YTEntry['VideoDetails']
			wowzaDetails = YTEntry['WowzaDetails']

			logger.debug(' giro in AccendiYT con account = ' + key)

			'''
			result_enable = pyWowzaClass.Action_Mapentry( videoDetails['wow_name'] , 'enable', pySettaVars.WOW_SERVER )

			if result_enable['success']:
				wowzaDetails['enabled'] = True


			# qui per accendere devo verificare ( con timeout ? ) che il livestreaming sia active
			# e quindi settare poi il broadcast 

			args = ()
			kwargs = { 'status_param' : 'active', 'item_id' : videoDetails['LivestreamsDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
			if not wait_until( pyYoutubeClass.Check_LiveStream_Status, 10, 0.25,*args, **kwargs ):
				continue

			
			# prima a testing
			result_update = pyYoutubeClass.Update_LiveBroadcast(  videoDetails['LivebroadcastDetails']['id'], "testing" , socialDetails['livestreamingaccount'], socialDetails['continuousLive' ] )

			# verificare che sia veramente a testing
			args = ()
			kwargs = { 'status_param' : 'testing', 'item_id' : videoDetails['LivebroadcastDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
			if not wait_until( pyYoutubeClass.Check_LiveBroadcast_Status, 15, 0.25,*args, **kwargs ):
				continue
		
			'''
			# e quindi a live
			result_update = pyYoutubeClass.Update_LiveBroadcast(  videoDetails['LivebroadcastDetails']['id'], "live" , socialDetails['livestreamingaccount'] , socialDetails['continuousLive' ])

			logger.debug(result_update)

			if 'error' in result_update:
				# CLAD DEBUG 
				# da verificare se devo mettere il valore di complete sotto qui oppure
				# sotto i LiveBroadcastDetails
				videoDetails['status'] = 'complete'
				return item


			# CLAD DEBUG
			# da verificare se devo aggiungere anche ai videoDetails il valore di status
			# vedere se lo usiamo per qualche verifica da qualche parte
			videoDetails['LivebroadcastDetails'] = result_update
			logger.debug(" ---------------------- DEBUG AccendiYT  ------------------ ")
			logger.debug(' Account = ' + key)
			logger.debug(YTEntry)


			#logger.debug(" ---------------------- DEBUG AccendiYT  ------------------ ")
			#logger.debug(item['SocialFlags'])
			#logger.debug(" ---------------------- DEBUG AccendiYT  ------------------ ")


			# clean per il test chiama le delete delle entry create
			#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
			#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] , pySettaVars.WOW_SERVER )
			#logger.debug(wow_delete)
			# DA TOGLIERE

	logger.debug(" ---------------------- FINE AccendiYT  ------------------ ")
	return item

def SpegniFB( item ):

	logger.debug(" ---------------------- INIZIO SpegniFB  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'LIVE_STOPPED' == item['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] :
		logger.debug(' in SpegniFB siamo STOPPATI !! ')
		return item

	# se sono qui devo spegnere il Wow

	_fb_accounts = item['SocialFlags']['fb-livestreaming-account']

	for key,FBEntry in _fb_accounts.iteritems():
		socialDetails = FBEntry['SocialDetails']
		videoDetails = FBEntry['VideoDetails']
		wowzaDetails = FBEntry['WowzaDetails']

		result_update = pyFacebookClass.Update_Live_Video_Details( videoDetails['id'] , "end_live_video", "true",  socialDetails['livestreamingaccount']  )

		result_disable = pyWowzaClass.Action_Mapentry( videoDetails['wow_name' ] , "disable", pySettaVars.WOW_SERVER )
		if result_disable['success']:
			wowzaDetails['enabled'] = False

		if 'error' in result_update:
			videoDetails['status'] = 'LIVE_STOPPED'
			return item
		videoDetails['status'] = result_update['status']

		wowzaDetails['WowDeleted'] = pyWowzaClass.Delete_Mapentry( videoDetails['wow_name' ] , pySettaVars.WOW_SERVER )
			


	logger.debug(" ---------------------- FINE SpegniFB  ------------------ ")
	return item


def SpegniYT( item ):

	logger.debug(" ---------------------- INIZIO SpegniYT  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'complete' == item['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] :
		logger.debug(' in SpegniYT siamo STOPPATI !! ')
		return item

	# se sono qui devo accendere il Wow
	_yt_accounts = item['SocialFlags']['yt-livestreaming-account']

	for key,YTEntry in _yt_accounts.iteritems():
		socialDetails = YTEntry['SocialDetails']
		videoDetails = YTEntry['VideoDetails']
		wowzaDetails = YTEntry['WowzaDetails']

		result_disable = pyWowzaClass.Action_Mapentry( videoDetails['wow_name' ] , "disable", pySettaVars.WOW_SERVER )
		if result_disable['success']:
			wowzaDetails['enabled'] = False

		result_update = pyYoutubeClass.Update_LiveBroadcast( videoDetails['LivebroadcastDetails']['id'] , "complete", socialDetails['livestreamingaccount'], 'dummyContinuousLive')


		if 'error' in result_update:
			videoDetails['LivebroadcastDetails']['status']['lifeCycleStatus'] = 'complete'
			wowzaDetails['WowDeleted'] = pyWowzaClass.Delete_Mapentry( videoDetails['wow_name' ] , pySettaVars.WOW_SERVER )
			return item
		videoDetails['LivebroadcastDetails'] = result_update

		wowzaDetails['WowDeleted'] = pyWowzaClass.Delete_Mapentry( videoDetails['wow_name' ] , pySettaVars.WOW_SERVER )
		

		#logger.debug(" ---------------------- DEBUG SpegniYT  ------------------ ")
		#logger.debug(item['SocialFlags'])
		#logger.debug(" ---------------------- DEBUG SpegniYT  ------------------ ")


		# clean per il test chiama le delete delle entry create
		#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
		#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] , pySettaVars.WOW_SERVER )
		#logger.debug(wow_delete)
		# DA TOGLIERE

	logger.debug(" ---------------------- FINE SpegniYT  ------------------ ")
	return item

def GestisciPREPARA( dict_items ):

	logger.debug(" ---------------------- Inizio GestisciPREPARA  ------------------ ")
	#logger.debug(" dict_items =  " + str( dict_items ))
	result_dict = {}
	
	# devo verificare che sia gia stato preparato
	# altrimenti connetterlo
	# qui giro sulla lista preaparata dalla Controlla_Social_Flags
	for key, value in dict_items.iteritems():
		#logger.debug(value)
		if len(value['SocialFlags']['fb-livestreaming-account']) > 0:

			'''
			if not 'FBStreamingDetails' in value or not value['SocialFlags']['fb-livestreaming-account'][0] in value['FBStreamingDetails']:
				logger.debug(' gestisciPrepara dice che devo farlo FB')
				#CLAD
				value['FBStreamingDetails'] =  PreparaFB( value )
			else:
				logger.debug(' gestisciPrepara dice che esiste gia FB')
				# verifico se manca la parte di Wowza ed eventualmente la metto
				
			CLAD Questo diventa : 
			'''

			logger.debug(" passo a preparare ")
			value =  PreparaFB( value )
			result_dict[key] =  value 

	# devo verificare che sia gia stato preparato
	# altrimenti connetterlo
	for key, value in dict_items.iteritems():
		if len(value['SocialFlags']['yt-livestreaming-account']) > 0:
			value = PreparaYT( value )
			result_dict[key] =  value 

	logger.debug(" ---------------------- Fine GestisciPREPARA  ------------------ ")
	return result_dict


def GestisciON( dict_items ):

	logger.debug(" ---------------------- Inizio GestisciON  ------------------ ")
	logger.debug( dict_items )
	logger.debug(" ---------------------- Inizio GestisciON  ------------------ ")
	result_dict = {}

	# devo verificare che sia gia stato acceso
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		logger.debug( 'giro per FB: ' + key)
		if len(value['SocialFlags']['fb-livestreaming-account']) > 0:
			# cioe se ci sono degli account FB da accendere
			if QualcunoMancaStreamingDetailsFB( value ) :
				logger.debug('NOT videDetails')
				value = PreparaFB( value )
			else:
				# il VerificaON ritorna [ FB , YT ] 
				# e quindi con [0] prendo FB
				if not VerificaON( value )[0]:
					logger.debug('not VerificaON')
					value = AccendiFB( value )

			logger.debug('setto il FB result = ' + str(value))
			result_dict[key] = value 	
		else:
			logger.error( 'la len di value[SocialFlags][fb-livestreaming-account] == 0')

	# devo verificare che sia gia stato acceso
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		logger.debug( 'giro per YT: ' + key)
		if len(value['SocialFlags']['yt-livestreaming-account']) > 0:
			if QualcunoMancaStreamingDetailsYT( value ) :
				logger.debug('NOT videDetails')
				value = PreparaYT( value )
			else:
				# il VerificaON ritorna [ FB , YT ] 
				# e quindi con [-1] prendo YT
				if not VerificaON( value )[-1]:
					logger.debug('not VerificaON')
					value = AccendiYT( value )

			logger.debug('setto il YT result = ' + str(value))
			result_dict[key] = value 	
		else:
			logger.error( 'la len di value[SocialFlags][yt-livestreaming-account] == 0')

	logger.debug(" ---------------------- Fine GestisciON  ------------------ ")
	return result_dict


def GestisciOFF( dict_items ):

	logger.debug(" ---------------------- Inizio GestisciOFF  ------------------ ")
	result_dict = {}
	da_archiviare_dict = {}

	# devo verificare che sia gia stato spento
	# altrimenti spegnerlo
	for key, value in dict_items.iteritems():
		if len(value['SocialFlags']['fb-livestreaming-account']) > 0:

			if not 'VideoDetails'  in value['SocialFlags']['fb-livestreaming-account'].values()[0]:
				# poi magari questa e condizione di cancellazione
				logger.debug('NOT videDetails')
				continue
			if value['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] == 'LIVE_STOPPED' :
				# per adesso lo metto ancora 
				# poi magari questa e condizione di cancellazione
				logger.debug('Video LIVE_STOPPED')
				da_archiviare_dict[key] = value 	
				continue
			else:
				value = SpegniFB( value )

			result_dict[key] = value 	

		else:
			# poi magari questa e condizione di cancellazione
			logger.error(' lunghezza di value[FBStreamingDetails] == 0' )



	# devo verificare che sia gia stato spento
	# altrimenti spegnerlo
	for key, value in dict_items.iteritems():
		if len(value['SocialFlags']['yt-livestreaming-account']) > 0:

			if not 'VideoDetails' in value['SocialFlags']['yt-livestreaming-account'].values()[0]:
				# questa verifica se ci sono i video e o i wowza details
				# poi magari questa e condizione di cancellazione
				continue

			if not 'LivebroadcastDetails' in value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']:
				continue
			if not 'status' in value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']:
				continue
			if not 'lifeCycleStatus' in value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']:
				continue

			logger.debug('liveBroadcast status : ')

			logger.debug( value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'])

			if value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] == 'complete' :
				# per adesso lo metto ancora 
				# poi magari questa e condizione di cancellazione
				da_archiviare_dict[key] = value 	
				continue
			else:
				value = SpegniYT( value )

			result_dict[key] = value 	
		else:
			logger.error(' lunghezza di value[YTStreamingDetails] == 0' )

	logger.debug(" ---------------------- Fine GestisciOFF  ------------------ ")
	return [ result_dict, da_archiviare_dict ]


def Merge_Liste( da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF, da_fare_list_NOTIFICA):
	
	da_fare_list_PREPARA.update(da_fare_list_ON)
	da_fare_list_PREPARA.update(da_fare_list_OFF)
	da_fare_list_PREPARA.update(da_fare_list_NOTIFICA)

	return da_fare_list_PREPARA

def Merge_Dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def UpdateVideo( idX ):
	try:  
		logger.debug(" ---------------------- INIZIO UpdateVideo ------------------ ")
		videoFile = ''
		durata = '000.000'
		[ result, videoUri, duration ] = GetRestJson.GetVideoDurationFromPlay( idX )  
		# qui e dove da errore perche gli arriva un json al posto di una url
		#print [ result, videoUri, duration ] 
		if not result:
			return False
		else:
			durata = duration

		logger.debug(' che dura : ' + durata)
		Prendi_Content_Da_ECE.UpdateSpostaETaggaArchivi( idX, durata )
	except Exception as e:
		logger.error( 'ERRORE: ' + str(idX) + ' a causa di : '  + str(e))
		return False
		

def Update( idX ):
	try:  
		audioFile = ''
		durata = '000.000'
		[ result, audioUri, duration ] = GetRestJson.GetAudioDurationFromPlay( idX )
		print [ result, audioUri, duration ]
		
		if not result :
			logger.debug( 'ERRORE in get play : ' + audioUri)
			return False
		else:
			durata = duration

		logger.debug(' che dura : ' + durata)

		Prendi_Content_Da_ECE.UpdateSpostaETaggaArchivi( idX, durata )
	except Exception as e:
		logger.error( 'ERRORE: ' + str(idX) + ' a causa di : '  + str(e))
		return False


if __name__ == "__main__":

	print '-\n'
	print '------------ INIT ---------- pySpostaETaggaArchivi.py ---------------------'
	#print os.environ

	# questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
	SettaEnvironment( False )

	print '\n\n log configuration file : ' +  pySettaVars.LOGS_CONFIG_FILE + '\n'
	logging.config.fileConfig(pySettaVars.LOGS_CONFIG_FILE)
	logger = logging.getLogger('pySpostaETaggaArchivi')
	print ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n'

	###### DEBUGGGG ##############
	#lista =  Put_Content_In_ECE.Put_IdProd( 161451, './_cambiamento_') 
	#logger.debug(lista)

	#listaArchivi =  Crea_List_Archivi.Solr_Prendi_Archivi( '10630')
	#listaArchivi = listaArchivi.listaArchivi
	#with open('listaArchivi_pic_key_Sonaps_2.0.txt', 'w') as outfile:
		#json.dump(listaArchivi, outfile)
	#with open('listaArchivi_pic_key_Sonaps_2.0.txt', 'r') as infile:
		#listaArchivi = json.load( infile)
	
	#print 'lista ID della 20815 : ' + str(len(listaArchivi )) + ' \n\n'
	#print listaArchivi
	#listaDaSkippare =  Crea_List_Archivi.Solr_Prendi_Archivi( '27799')
	listaDaSkippare =  []
	#print len(listaDaSkippare )
	#print listaDaSkippare
	#listaDaFare = lista_ripasso

	listaDaFare = listaDaFare.listaDaFare
	listaDaFare = listaDaSpostare.listaDaSpostare
	listaDaFare = listaKeyPic.listaKeyPic
	listaDaFare = listaKeyDaSpostare.listaKeyDaSpostare
	listaDaFare = listaArchivi
	listaDaFare = listaProgDaCambiare.listaProgDaCambiare
	# sono integers
	listaDaSkippare = [14759301]
	
	print ' len listaDaFare = ' + str(len(listaDaFare))	

	sectionToGoTo = '5'

	countFiles = 1
	for item in listaDaFare:
		logger.debug( '- ' + str(countFiles) + '/' + str(len(listaDaFare)) + ' --> ' +  str(item))
		countFiles +=1
		if item in listaDaSkippare:
			logger.warning( '- skippato item --> ' +  str(item))
			continue
		if ( Prendi_Content_Da_ECE.cambiaPathSubtitles( str(item ))):
			logger.debug( 'spostato keypic --> ' + str(item ))
		time.sleep( 0.5 )
		

	print '------------ END ---------- pySpostaETaggaArchivi.py ---------------------'
