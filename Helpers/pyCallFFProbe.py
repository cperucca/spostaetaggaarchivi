#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import subprocess
import logging
import codecs
import json

logger = logging.getLogger('pyGetFFprobeDuration')

def GetFFprobeDuration( fileName ):

	result = ''
	args=("ffprobe","-loglevel","panic","-show_entries","format=duration","-i", fileName)
	popen = subprocess.Popen(args, stdout = subprocess.PIPE)
	popen.wait()
	output = popen.stdout.read()
	
	result = output.split("duration=")[-1].split("[")[0]
	result = result[:-4]
	logger.debug('ffprobe - duration = ' +  result)
	return result

def spezzaJson( nomeFile, numPerFile , nomeOut):

	fin = codecs.open( nomeFile, 'r', 'utf-8' )
	dict_in = json.load( fin )
	fin.close()

	dict_out = {}
	
	count = 0 
	numFiles = 0
	
	for audio,value in dict_in.iteritems():
		dict_out[ audio ] = value	
		count += 1
		if count == numPerFile:
			# ne ho messi 5000 in quello nuovo
			# lo scrivo con il count giusto zfillato a 3
			fout = codecs.open(  nomeOut + '_%03d' % numFiles + '.json', 'w', 'utf-8' ) 
			numFiles += 1
			json.dump( dict_out, fout )
			fout.close()
			count = 0
			dict_out = {}

	exit(0)


def spezzaList( nomeFile, numPerFile , nomeOut):
	dict_out = {}
	
	count = 0 
	numFiles = 0

	fout = open( nomeOut + '_%03d' % numFiles + '.json', 'w' )
	
	with open( nomeFile, 'r' ) as fin:
		for line in fin:
			print line[:-1]
			print >> fout,line[:-1]
			count += 1
			if count == numPerFile:
				# ne ho messi 5000 in quello nuovo
				# lo scrivo con il count giusto zfillato a 3
				fout.close()
				numFiles += 1
				fout = open(  nomeOut + '_%03d' % numFiles + '.json', 'w' ) 
				count = 0
	exit(0)





if __name__ == "__main__":

	spezzaList(  '/home/perucccl/Webservices/STAGING/TransAudioDuration/Resources/Jsons/transAudioDataSorted.json', 2000, '/home/perucccl/Webservices/STAGING/TransAudioDuration/Resources/Jsons/tADataS')
	exit(0)
	GetFFprobeDuration( '/mnt/rsi_transcoded/vmeo/httpd/html/rsi/unrestricted/2014/01/29/132319.mp3' )
	
