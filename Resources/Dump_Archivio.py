import json
import codecs
import sys

def printList( dict_content ):


	for key, value in dict_content.iteritems():
		print ' Key -----> ' + key
		if 'ECEDetails' in value :
			print '----  ECEDetails : '	
			print value['ECEDetails']
		if 'SocialFlags' in value :
			print '----  SocialFlags : '	
			print value['SocialFlags']
		if 'FBStreamingDetails' in value and len(value['FBStreamingDetails']) > 0:
			print '----  FBStreamingDetails : '	
			for keystream,valuestrem in value['FBStreamingDetails'].iteritems():
				print 'Details of : ' + keystream
				print valuestrem
		if 'YTStreamingDetails' in value and len(value['YTStreamingDetails']) > 0:
			print '----  YTStreamingDetails : '	
			for keystream,valuestrem in value['YTStreamingDetails'].iteritems():
				print 'Details of : ' + keystream
				print valuestrem
		print '------------------------- fine ' + key + '-----------------------'
		print '\r\n'
	

print ' apro archivio ' + sys.argv[1]

fin = codecs.open( sys.argv[1], 'r', 'utf-8' )

arch = json.load(fin)

fin.close()

printList( arch )
