
# -*- coding: utf-8 -*-


import logging
import logging.config

import ast
import os
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import json
from datetime import datetime, timedelta
import time
import copy

import glob
import codecs

import ECE.Crea_List_TransAudio as Crea_List_TransAudio
import ECE.Prendi_Content_Da_ECE as Prendi_Content_Da_ECE
import ECE.Put_Content_In_ECE as Put_Content_In_ECE
import ECE.GetRestJson as GetRestJson
import Resources.pySettaVars as pySettaVars
import Helpers.pyCallFFProbe as pyCallFFProbe



def Croppa_Titolo( titolo, link ):

	logger.debug(len(titolo))
	
	
	delta = len( link) + 5
	# il delta e +5 se con i puntini mentre e 
	# +1 se senza puntini

	
	if len(titolo) + delta > 138:
		logger.debug(' > 138')
 
		puntini = u'...'
	else:
		logger.debug(' < 138')
		puntini = u''

	while len( titolo ) + delta > 138:
		#logger.debug(titolo.split(' '))
		#logger.debug(titolo.split(' ')[-1])
		lunghezza = -(len(titolo.split(' ')[-1]) + 1)
		#logger.debug(lunghezza)
		titolo = titolo[0:lunghezza]
		#logger.debug(len(titolo))
		#logger.debug(titolo)

	#logger.debug(titolo + '...')
	titolo = titolo + puntini

	#logger.debug(titolo + u' ' + link)

	return titolo + u' ' + link

def Croppa_Coda_Testo( testo ):

	_debugga_ = True

	if _debugga_ :
		logger.debug(len(testo))

	while len( testo ) > 138:
		#logger.debug(testo.split(' '))
		#logger.debug(testo.split(' ')[-1])
		lunghezza = -(len(testo.split(' ')[-1]) + 1)

		#logger.debug(lunghezza)
		testo = testo[0:lunghezza]
		if _debugga_ :
			logger.debug(len(testo))
			logger.debug(testo)

	if _debugga_ :
		logger.debug(testo + '...')


	return testo + '...'




def Componi_Testo_Limite( name, link, desc ):
	# questa limita a 140 caratteri quello che vogliamo buttare su
	totale = name + u' ' + link + u' ' + desc 	
	logger.debug(totale)

	if len(totale) > 138:
		if len(name + u' ' + link) < 138:
			#logger.debug('titolo troppo lungo ma croppo solo in fondo')
			totale = Croppa_Coda_Testo( totale )
		else:
			# vuol dire che devo togliere dal titolo il numerodi caratteri del link 
			# piu tre puntini che aggiungero in fondo
			#logger.debug('devo cavare dal titolo link e tre puntini')
			totale = Croppa_Titolo( name, link )
			
	logger.debug('result = ' + totale)
	return totale


#Componi_Testo_Limite(u'TESTTitle: test 4.0 post su social',u'http://www.rsi.ch/g/8845033' ,  u'TESTSubTitle: n insetticidi e distribuite a oltre 578 milioni di')
#Componi_Testo_Limite(u'TESTTitle: test 4.0 post su social',u'http://www.rsi.ch/g/8845033' ,  u'TESTSubTitle: test 4.0 post su social ata su più fronti. Tra il 2008 e il 2010 cè stato un progressivo aumento del numero di zanzariere trattate con insetticidi e distribuite a oltre 578 milioni di')
#Componi_Testo_Limite(u'TESTSubTitle: test 4.0 post su social ata su più fronti. Tra il 2008 e il 2010 cè stato un progressivo aumento del numero di zanzariere trattate con insetticidi e distribuite a oltre 578 milioni di', u'http://www.rsi.ch/g/8845033' , u'TESTTitle: test 4.0 post su social')
#exit(0)


def ChooseAccount( da_fare_under, lis ):

	logger.debug(' ------------- INIT ChooseAccount --------------------------------------- ')
	abs_account = None
	msg = u''
	fb_title = ''
	fb_alt_desc = ''

	if 'fb' in da_fare:
		try :
			logger.debug('in FB')
			abs_account = pyFacebookClass.FacebookConnection( da_fare_under )

			# logger.debug(abs_account)
			# e devo anche mettere il testo giusto
			# per facebook ci potrebbero anche stare 
			#'link':'http://www.rsi.ch', oppure all articolo
			#'picture':'http://pngimg.com/upload/water_PNG3290.png',
			#'name':'NAME WATER',
			#'description':'DESCRIPTION WATER',
			#'caption':'CAPTION WATER'
			# da vedere

			if lis['social_flags']['facebookAltTitle'] is None  or 'None' in lis['social_flags']['facebookAltTitle'] :
				logger.debug(' 1 ' )
			else:
				logger.debug(' 2 ')

			if not (lis['social_flags']['facebookAltTitle'] is None  or 'None' in lis['social_flags']['facebookAltTitle'] ):
				logger.debug('nome = title')
				lis['lead']['name'] = lis['social_flags']['facebookAltTitle'].strip()
			else:
				logger.debug('facebookAltTitle is None')

			logger.debug(' in mezzo')

			if not (lis['social_flags']['facebookAltDesc'] is None or 'None' in lis['social_flags']['facebookAltDesc'] ):
				logger.debug('desc = desc')
				lis['lead']['description'] = lis['social_flags']['facebookAltDesc'].strip() 
			else:
				logger.debug('facebookAltDesc is None')


			logger.debug(lis['lead'])

			if not (lis['social_flags']['facebookText'] is None):
				msg = lis['social_flags']['facebookText'].strip()
			else:
				logger.debug('facebookText is None')
			logger.debug('try msg del Facebook = ' + msg)
		except:
			logger.debug('except  msg del Facebook = ' + msg)
			return [ None, '', '','']
	else:
		try:
			logger.debug('in TW')
			abs_account = pyTwitterClass.TwitterConnection( da_fare_under )
			logger.debug(' preso account ' + str(abs_account))

			logger.debug(lis['lead'])

			# e devo anche mettere il testo giusto
			logger.debug(' sta per strippare ')
			if lis['social_flags']['twitterText'] is None:
				logger.debug('lis e NONE')
			if not (lis['social_flags']['twitterText'] is None or not (lis['social_flags']['twitterText'])):
				msg = Croppa_Titolo( lis['social_flags']['twitterText'].strip() , lis['link'])
				logger.debug(' 1 - setto msg = ' + msg)

			else:
				# devo verificare su richiesta di Diego se ho l upload dell immagine
				# nel qual caso devo metterci il titolo  + url breve + sottotitolo
				logger.debug(' nella verifica della social flag')
				if  not (lis['social_flags']['tw-uploadImage'] is None ) and 'true' in lis['social_flags']['tw-uploadImage'] :
					logger.debug(' tw-uploadImage true ')
					#logger.debug(lis['link'])
					#logger.debug(lis['lead']['description'])
					#logger.debug(lis['title'])

					if lis['lead']['description'] is None:
						logger.debug('description is NONE')
						desc = ''
					else:
						desc = lis['lead']['description']
					#logger.debug(' --- ' )
					msg = Componi_Testo_Limite( lis['lead']['name'] , lis['lead']['link'] , desc )
					logger.debug(' 2 - setto msg = ' + msg)
					#logger.debug(' --- ' )
				
				else:
					# avendo cambiato il testo del msg non posso mettere SEMPRE la url in fondo
					# quindi e qui che devo aggiungerla o meno in fondo al msg
					logger.debug(' tw-uploadImage false ')
					# se saimo una short story dopbbiamo mettere il titolo nel testo
					if 'short' in lis['contenttype']:
						msg = Croppa_Titolo( lis['title'] , lis['lead']['link'])
						logger.debug(' 3 - setto msg = ' + msg)
					else:
						msg = Croppa_Titolo( '' ,  lis['lead']['link'] )
						logger.debug(' 4 - setto msg = ' + msg)
	
			logger.debug('try msg del TWitt = ')
			logger.debug(msg )
			
		except:
			logger.debug('except msg del TWitt = ' )
			logger.debug(msg)
			return [ None, []]

	logger.debug(lis['lead'])
	logger.debug(' ------------- FINE  ChooseAccount --------------------------------------- ')
	return [ abs_account, [ msg, '', '', ast.literal_eval(str(lis['lead'])) ] ]

def Aumenta_Error( val ):
	
	if val is None or ( not ( 'Errore' in val )) :
		return 'Errore 1'
	else:
		numero = val.split(':')[0]
		numero = numero.split('Errore')[-1]
		logger.debug(numero.strip())
	
	return 'Errore ' +  str( int(numero) + 1)


def SettaValue( result, lis, da_fare, _id ):

	if result:
		lis['social_flags'][da_fare.replace('account', 'sent')] =  _id
		logger.debug('aggiunto a lista' + str(da_fare.replace('account', 'sent')) +' '+  str(lis['social_flags'][da_fare.replace('account', 'sent')]))
	else:
		if '403' in str(_id):
			lis['social_flags'][da_fare.replace('account', 'sent')] =  Aumenta_Error( lis['social_flags'][da_fare.replace('account', 'sent')]) + ': tweet duplicato'
			logger.debug('da ritornare il motivo per cui non riusciamo a postare\n\n')
		else:
			lis['social_flags'][da_fare.replace('account', 'sent')] =  Aumenta_Error( lis['social_flags'][da_fare.replace('account', 'sent')]) +':' +  str(_id).split(',')[-1] 
			logger.debug('da ritornare il motivo per cui non riusciamo a postare\n\n')

def SettaEnvironment( debug ):

	if (debug):
		print 'PRIMA DI SETTARE ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ

	if (debug):
		print '---------------------------------'

	Environment_Variables = {}
	
	print 'verifico il setting della variabile _TransAudioDurationEnv_ '
	if '_TransAudioDurationEnv_' in os.environ:
		if 'PRODUCTION' in os.environ['_TransAudioDurationEnv_']:
			print 'variabile _TransAudioDurationEnv_ ha valore \'PRODUCTION\''
			print 'setto ENV di PROD'
			Environment_Variables = pySettaVars.PROD_Environment_Variables
			pySettaVars.LOGS_CONFIG_FILE = pySettaVars.PROD_CONFIG_FILE
			pySettaVars.WOW_SERVER = pySettaVars.PROD_WOW_SERVER
			print ' settato server = ' + pySettaVars.WOW_SERVER
		else:
			print 'variabile _TransAudioDurationEnv_ ha valore : ' + os.environ['_TransAudioDurationEnv_']
			print 'diverso da \'PRODUCTION\''
			print 'setto ENV di TEST'
			Environment_Variables = pySettaVars.TEST_Environment_Variables
			pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE
			pySettaVars.WOW_SERVER = pySettaVars.TEST_WOW_SERVER
			print ' settato server = ' + pySettaVars.WOW_SERVER
			
	else:
		print 'variabile _TransAudioDurationEnv_ non trovata: setto ENV di TEST'
		Environment_Variables = pySettaVars.TEST_Environment_Variables
		pySettaVars.LOGS_CONFIG_FILE = pySettaVars.TEST_CONFIG_FILE
		pySettaVars.WOW_SERVER = pySettaVars.TEST_WOW_SERVER
		print ' settato server = ' + pySettaVars.WOW_SERVER


	for param in Environment_Variables.keys():
		#print "%20s %s" % (param,dict_env[param])
		os.environ[param] = Environment_Variables[ param ]


	if (debug):
		print 'DOPO AVER SETTATO ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ

def Smonta_Details( streaming_details ):

	# qui devo verificare che per tutti gli account ci siano sia i 
	# VideoDetails che i WowzaDetails
	# altrimenti cancellare quello dei due che manca e mettere quei i
	# valori di quegli account a nullo
	result_details = {}

	for chiave,valore in streaming_details.iteritems():
		logger.debug(chiave + ' : '  + str(valore))
		result_details[ chiave ] = valore
		if not 'VideoDetails' in chiave:
			# verifico se esiste il corrispettivo Wowza 
			# per quell account ( parte che viene dopo VideoDetails )
			logger.debug( ' passato da qui: No VideoDetails' )
			
	return result_details

def QualcunoMancaStreamingDetailsFB( item ):

	logger.debug(" ---------------------- INIZIO QualcunoMancaStreamingDetailsFB  ------------------ " )
	logger.debug(item)

	adesso = datetime.now()

	result = False

	account = item['SocialFlags'][ 'fb-livestreaming-account' ]
	logger.debug( account )
	for key,valueUp in account.iteritems():
		socialDetails = valueUp['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(valueUp ))

		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'beccato  account con notifica nel futuro :-----------> ' + key )
			return True

		if 'notificato' in socialDetails :
			continue

		# qui devo verificare che non ce ne siano dei pezzi gia' fatti
		# se ci sono gia li metto nell account_details
		if 'VideoDetails' in socialDetails and  'WowzaDetails' in socialDetails:
			continue

		result = True
			
	logger.debug(" ---------------------- FINE QualcunoMancaStreamingDetailsFB  ------------------ ")
	return result


def QualcunoMancaStreamingDetailsYT( item ):

	logger.debug(" ---------------------- INIZIO QualcunoMancaStreamingDetailsYT  ------------------ " )
	logger.debug(item)

	adesso = datetime.now()

	result = False

	account = item['SocialFlags'][ 'yt-livestreaming-account' ]
	logger.debug( account )
	for key,valueUp in account.iteritems():
		socialDetails = valueUp['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(valueUp ))

		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'beccato  account con notifica nel futuro :-----------> ' + key )
			return True

		if 'notificato' in socialDetails :
			continue

		# qui devo verificare che non ce ne siano dei pezzi gia' fatti
		# se ci sono gia li metto nell account_details
		if 'VideoDetails' in socialDetails and  'WowzaDetails' in socialDetails:
			continue

		result = True
			
	logger.debug(" ---------------------- FINE QualcunoMancaStreamingDetailsYT  ------------------ ")
	return result

def PreparaFB( item ):

	logger.debug(" ---------------------- INIZIO PreparaFB  ------------------ " )
	logger.debug(item)
	# per Preparare lo stream devo :
	# girando sugli account sotto le SocialFlags
	#
	# 1 - FB.Create_Live_Video( title, description ) che mi torna	
	# {
    	# "secure_stream_url": "rtmps://rtmp-api.facebook.com:443/rtmp/1471030776261970?ds=1&s_l=1&a=ATh1bGN96LhhKx8S",
    	# "id": "1471019276263120"
	# ..... 
	# oltre a tutto il resto
	# }

	#result_create = pyFacebookClass.Create_Live_Video( item['SocialFlags']['fbVideoTitle' ] , item['SocialFlags']['fbVideoDesc' ]  )
	# cosi considero anche eventuali cambiamenti di daysaving time 
	# prendo la data per verificare che la notifica sia passata
	adesso = datetime.now()

	t_start = item['SocialFlags']['t_start']
	
        # CLAD -> Risoluzioe problema del minuto indietro.
        # logger.debug(datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s"))
        # aggiungo un minuto per passarlo al time di Facebook
        dt_start = datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ')
        dt_start = dt_start + timedelta(minutes=1)

	# dict che conterra i Video e Wow Details
	_fb_livestreaming_details = {}
	_account_details = {}

	_fb_accounts = item['SocialFlags']['fb-livestreaming-account']
	# tra queste ce ne sono alcune che devono essere notificate

	for key,FBEntry in _fb_accounts.iteritems():
		socialDetails = FBEntry['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(FBEntry ))
		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'salto account con notifica nel futuro :-----------> ' + key )
			continue
		try:

			# qui devo verificare che non ce ne siano dei pezzi gia' fatti
			# se ci sono gia li metto nell account_details
			if 'VideoDetails' in FBEntry:
				# quindi se esiste la entry per quell account 
				# DEVE esistere il post cioe i VideoDetails devono essere inizializzati
				# allora li copio in _account_details e li uso per produrre i WowzaDetails
				# se questi non esistono
				logger.debug('setto _account_details sul valore che mi arriva da DB ' )
				_account_details['VideoDetails'] = FBEntry['VideoDetails']
			else:
					
				logger.debug('creo un nuovo _account_details :' + key )

				logger.debug(' per FB : socialDetails[videoTitle] : ' + socialDetails['videoTitle' ] )
				logger.debug(' per FB : socialDetails[videoDesc] : ' + socialDetails['videoDesc' ] )
				logger.debug(' per FB : socialDetails[isGeoBlocked] : ' + socialDetails['isGeoBlocked' ] )
				logger.debug(' per FB : socialDetails[continuousLive] : ' + str(socialDetails['continuousLive' ] ))
				logger.debug(' per FB : socialDetails[livestreamingaccount] : ' + str(socialDetails['livestreamingaccount' ] ))

				result_create = pyFacebookClass.Create_Scheduled_Video_Geo( socialDetails['videoTitle' ],socialDetails['videoDesc' ],dt_start.strftime("%s"),socialDetails['livestreamingaccount'],socialDetails['isGeoBlocked'],socialDetails['continuousLive'])

				# DEBUG CLAD
				#result_create = {u'status': u'SCHEDULED_UNPUBLISHED', u'is_manual_mode': False, u'description': u'post fb 1', u'title': u'titolo fb 1', u'secure_stream_url': u'rtmps://live-api-s.facebook.com:443/rtmp/2334040573294315?s_sw=0&s_vt=api-s&a=AbzxZkfGOfkDbYiq', u'live_views': 0, u'creation_time': u'2019-04-11T09:29:58+0000', u'seconds_left': 0, u'dash_preview_url': u'https://video-frt3-1.xx.fbcdn.net/hvideo-prn1/v/r_zg4F3-7oM_LytD7RHBF/live-dash/dash-md/2334040573294315.mpd?lvp=1&_nc_rl=AfAGK6GvSsj8rvFn&oh=40a28ac881c9dac4f57c4f20c1c73a49&oe=5CB1AE45', u'broadcast_start_time': u'2019-04-11T09:29:58+0000', u'planned_start_time': u'2019-04-17T16:00:59+0000', u'embed_html': u'<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1237091532989230%2Fvideos%2F2679365098760193%2F&width=400" width="400" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>', u'permalink_url': u'https://www.facebook.com/1237091532989230/videos/2679365098760193/', u'id': u'2334040573294315'}
				# DEBUG CLAD

				#print result_create
				_account_details['VideoDetails'] = result_create
				# 2 - passare la secure_stream_url alla create_mapentry del WOW
				# cambio lo stream name per essere quella giusta da mandare alla lib Wow
				# per adesso assumo che il Wirecast sia sempre attivo
				# poi mi preoccupero di accenderlo
				_account_details['VideoDetails']['wow_secure_stream'] =  _account_details['VideoDetails']['secure_stream_url'].split('443/rtmp/')[-1]
				#_account_details['VideoDetails']['wow_name'] =  item['ECEDetails']['id'] + '_post:' + _account_details['VideoDetails']['id'] + '_account:' + socialDetails['livestreamingaccount']
				_account_details['VideoDetails']['wow_name'] =  'article:' + key + '_post:' + _account_details['VideoDetails']['id'] + '_account:' + socialDetails['livestreamingaccount']

			# qui inizializzo la VIDEODETAILS
			logger.debug('---------------------------------- init VideoDetails ---------------------------')
			logger.debug( _account_details['VideoDetails'])
			logger.debug('----------------------------------- end VideoDetails ---------------------------')

			# devo verificare se esistono gia anche i WowzaDetails
			if 'WowzaDetails' in FBEntry:
				_account_details['WowzaDetails'] = FBEntry['WowzaDetails'] 
			else:
				# se non esiste la entry per i WowzaDetails la creo
				logger.debug(' parametri passati a pyWowzaClass.Create_Mapentry_FB')
				logger.debug( _account_details['VideoDetails']['wow_name'] + ' ' +  _account_details['VideoDetails']['wow_secure_stream']  + ' ' + socialDetails['incoming_stream'] + '_720p' + ' ' + pySettaVars.WOW_SERVER )

				# CLAD -> per FB mando su il 720p aggiungendo alla fine del item['SocialFlags']['incoming_stream'] un _720p


				wow = pyWowzaClass.Create_Mapentry_FB( _account_details['VideoDetails']['wow_name'] , _account_details['VideoDetails']['wow_secure_stream'] , socialDetails['incoming_stream'] + '_720p', pySettaVars.WOW_SERVER )
				
				# DEBUG CLAD
				# wow = {u'message': u'Entry (article:9845290_post:2334040573294315_account:testare_e_bello) saved successfully', u'data': None, u'success': True}
				# DEBUG CLAD



				logger.debug(wow)
				if 'success' in wow and wow['success'] :
					logger.debug( 'passo da wow = success' )
					# 3 - se ritornato success : true
					# 4 - chiederne i details con Get_Wowza_Mapentry_Details da salvare in lis 

					# qui inizializzo la WOWZADETAILS




					_account_details['WowzaDetails'] = pyWowzaClass.Get_Wowza_Mapentry_Details( _account_details['VideoDetails']['wow_name' ] , pySettaVars.WOW_SERVER )
					# per successive modifiche con Action_Mapentry( nome, 'enable/disable')
					# o per la semplice Delete_Mapentry( nome )

					
					result_enable = pyWowzaClass.Action_Mapentry( _account_details['VideoDetails']['wow_name' ] , 'enable', pySettaVars.WOW_SERVER )

					# DEBUG CLAD
					# result_enable = {u'message': u'Entry (article:9845290_post:2334040573294315_account:testare_e_bello) enabled successfully', u'data': None, u'success': True}
					# DEBUG CLAD

					logger.debug( result_enable)
					if result_enable['success']:
						_account_details['WowzaDetails']['enabled'] = True
				else:
					logger.debug( 'passo da wow = NOT success' ) 


			# qui inizializzo la WOWZADETAILS
			logger.debug('----------------------------------- init WowzaDetails ---------------------------')
			logger.debug( _account_details['WowzaDetails'])
			logger.debug('----------------------------------- end WowzaDetails ---------------------------')

			#logger.debug( _account_details )

			FBEntry['VideoDetails'] = copy.deepcopy( _account_details['VideoDetails'] )
			FBEntry['WowzaDetails'] = copy.deepcopy( _account_details['WowzaDetails'] )
			FBEntry['SocialDetails']['notificato'] = True

			#logger.debug(" ---------------------- DEBUG PreparaFB FBEntry ------------------ ")
			#logger.debug( 'FBEntry : ' + str( FBEntry ) )
			_account_details = {}
			
		except Exception as e:
			logger.error('ERROR: EXCEPT in PreparaFB  = ' + str(e))
			logger.error('ERROR: ' + str(_account_details))
			
			_account_details = Smonta_Details( _account_details )
			

			# Da decidere strategia gestione errori
			_account_details = {}
			# se ho fatto un errore per questo account, NON devo cancellare 
			# i VideoDetails in modo da tornare tra un minuto a cercare di 
			# attaccarci i WowDetails

			pass
		
	# qui aggiunge la parte relativa a FB
	# item['FBStreamingDetails'] = _account_details


	logger.debug(" ---------------------- DEBUG PreparaFB _fb_livestreaming_details:  ------------------ ")
	logger.debug( item['SocialFlags'] )
	logger.debug(" ---------------------- DEBUG PreparaFB  giro sugli items di _fb_livestreaming_details ------------------ ")

	# clean per il test chiama le delete delle entry create
	#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
	#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] , pySettaVars.WOW_SERVER )
	#logger.debug(wow_delete)
	# DA TOGLIERE

	logger.debug(" ---------------------- FINE PreparaFB  ------------------ ")
	
	return item


def Prendi_Id_StreamName( VideoDetailsDictionary ):
	result = None
	id = None

	if 'LivestreamsDetails' in VideoDetailsDictionary and 'cdn' in VideoDetailsDictionary['LivestreamsDetails'] and  \
		'ingestionInfo' in VideoDetailsDictionary['LivestreamsDetails']['cdn'] and 'streamName' in VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']:
		logger.debug('streamname = ' + VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName'])
		result = VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName']

	if 'BindDetails' in VideoDetailsDictionary and 'id' in VideoDetailsDictionary['BindDetails']:
		logger.debug('bind Id = ' + VideoDetailsDictionary['BindDetails']['id'])
		id = VideoDetailsDictionary['BindDetails']['id']

	return [ result , id ]



def PreparaYT( item ):

	logger.debug(" ---------------------- INIZIO PreparaYT  ------------------ " )
	logger.debug(item)



	# DEBUG CLAD
	# DEBUG CLAD

	# per Preparare lo stream devo :
	# 1 - YT.Create_Live_Video( title, description ) che mi torna	
	# {
    	# "secure_stream_url": "rtmps://rtmp-api.facebook.com:443/rtmp/1471030776261970?ds=1&s_l=1&a=ATh1bGN96LhhKx8S",
    	# "id": "1471019276263120"
	# ..... 
	# oltre a tutto il resto
	# }

	#result_create = pyFacebookClass.Create_Live_Video( item['SocialFlags']['fbVideoTitle' ] , item['SocialFlags']['fbVideoDesc' ]  )
	# cosi considero anche eventuali cambiamenti di daysaving time 
	# prendo la data per verificare che la notifica sia passata
	adesso = datetime.now()

	t_start = item['SocialFlags']['t_start']
		
        # CLAD -> Risoluzioe problema del minuto indietro.
        # logger.debug(datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s"))
        # aggiungo un minuto per passarlo al time di Facebook
        dt_start = datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ')
        dt_start = dt_start + timedelta(minutes=1)

	logger.debug(datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s"))

	_yt_livestreaming_details = {}
	_account_details = {}

	_yt_accounts = item['SocialFlags']['yt-livestreaming-account']
	# tra queste ce ne sono alcune che devono essere notificate

	for key,YTEntry in _yt_accounts.iteritems():
		socialDetails = YTEntry['SocialDetails']
		# se questa socialDetails non ha passato la data di notifica continuo alla prossima
		logger.debug( " entry : " + str(YTEntry ))

		if adesso < datetime.strptime( socialDetails['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
			logger.debug( 'salto account con notifica nel futuro :-----------> ' + key )
			continue
	
		try:

			# qui devo verificare che non ce ne siano dei pezzi gia' fatti
			# se ci sono gia li metto nell account_details
			if 'VideoDetails' in YTEntry:
				# quindi se esiste la entry per quell account 
				# DEVE esistere il post cioe i VideoDetails devono essere inizializzati
				# allora li copio in _account_details e li uso per produrre i WowzaDetails
				# se questi non esistono
				logger.debug('setto _account_details sul valore che mi arriva da DB ' )
				_account_details['VideoDetails'] = YTEntry['VideoDetails']
			else:
				logger.debug('creo un nuovo _account_details :' + key )

				logger.debug(' per YT : socialDetails[videoTitle] : ' + socialDetails['videoTitle' ] )
				logger.debug(' per YT : socialDetails[videoDesc] : ' + socialDetails['videoDesc' ] )
				logger.debug(' per YT : socialDetails[isGeoBlocked] : ' + socialDetails['isGeoBlocked' ] )
				logger.debug(' per YT : socialDetails[continuousLive] : ' + str(socialDetails['continuousLive' ] ))
				logger.debug(' per YT : socialDetails[livestreamingaccount] : ' + str(socialDetails['livestreamingaccount' ] ))

				yt_account = socialDetails['livestreamingaccount'].replace('-','_')
				result_create = pyYoutubeClass.Crea_And_Bind_Livestream_Event( socialDetails['videoTitle' ] , socialDetails['videoDesc' ], datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m-%dT%H:%M:%S.000Z') , 'public', socialDetails['continuousLive' ] , yt_account )

				#result_create = {u'LivestreamsDetails': {u'status': {u'streamStatus': u'ready', u'healthStatus': {u'status': u'noData'}}, u'kind': u'youtube#liveStream', u'cdn': {u'resolution': u'1080p', u'ingestionType': u'rtmp', u'ingestionInfo': {u'ingestionAddress': u'rtmp://a.rtmp.youtube.com/live2', u'streamName': u't2fp-cwzg-z646-2zk0', u'backupIngestionAddress': u'rtmp://b.rtmp.youtube.com/live2?backup=1'}, u'frameRate': u'30fps', u'format': u'1080p'}, u'snippet': {u'isDefaultStream': False, u'channelId': u'UCv7yEZoifGFg_CioakqBMFA', u'description': u'Test_Description', u'publishedAt': u'2017-06-01T13:50:02.000Z', u'title': u'Stream_per_Test_CLAD.2.0'}, u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/2aA49S91KnshlTkN2tMSEmcESAE"', u'id': u'v7yEZoifGFg_CioakqBMFA1496325002526255'}, u'BindDetails': {u'contentDetails': {u'closedCaptionsType': u'closedCaptionsDisabled', u'projection': u'rectangular', u'startWithSlate': False, u'boundStreamId': u'v7yEZoifGFg_CioakqBMFA1496325002526255', u'enableEmbed': False, u'enableClosedCaptions': False, u'enableLowLatency': False, u'boundStreamLastUpdateTimeMs': u'2017-06-01T13:50:02.537Z', u'enableContentEncryption': False, u'recordFromStart': True, u'enableDvr': True, u'monitorStream': {u'broadcastStreamDelayMs': 0, u'embedHtml': u'<iframe width="425" height="344" src="https://www.youtube.com/embed/zEa_vQr6v7A?autoplay=1&livemonitor=1" frameborder="0" allowfullscreen></iframe>', u'enableMonitorStream': True}}, u'kind': u'youtube#liveBroadcast', u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/rPXg9ooOujATuCxHtve5Tt0Pqmg"', u'id': u'zEa_vQr6v7A'}, u'LivebroadcastDetails': {u'snippet': {u'thumbnails': {u'default': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/default_live.jpg?sqp=CIi3wMkF&rs=AOn4CLD-OH957yBGBrNOZB1s-_Wqe8gq0Q', u'width': 120, u'height': 90}, u'high': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/hqdefault_live.jpg?sqp=CIi3wMkF&rs=AOn4CLBi97H8jZLxX3S1EHXKTiuZijOMtg', u'width': 480, u'height': 360}, u'medium': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/mqdefault_live.jpg?sqp=CIi3wMkF&rs=AOn4CLB_LuQaBEamvFwnHdAGGy3Zun6Fdg', u'width': 320, u'height': 180}}, u'title': u'Broad_per_Test_CLAD.2.0', u'channelId': u'UCv7yEZoifGFg_CioakqBMFA', u'publishedAt': u'2017-06-01T13:50:03.000Z', u'liveChatId': u'Cg0KC3pFYV92UXI2djdB', u'scheduledStartTime': u'2017-06-01T22:00:00.000Z', u'isDefaultBroadcast': False, u'description': u'Create_Broadcast_Description'}, u'status': {u'recordingStatus': u'notRecording', u'privacyStatus': u'private', u'lifeCycleStatus': u'created'}, u'kind': u'youtube#liveBroadcast', u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/ikqqsSV_d94FW2DIvGSVREyQa4E"', u'id': u'zEa_vQr6v7A'}}


				# qui inizializzo la VIDEODETAILS con un dizionario che mi arriva dalla create e bind di youtube
				# formata come segue:  { 'LivestreamsDetails':livestreams_res,'LivebroadcastDetails':livebroadcast_res,'BindDetails':bind_livebroadcast}

				_account_details['VideoDetails'] = result_create
				logger.debug('----------------------------------- first VideoDetails ---------------------------')
				logger.debug( _account_details['VideoDetails' ])
				logger.debug('----------------------------------- first VideoDetails ---------------------------')

				# 2 - passare la secure_stream_url alla create_mapentry del WOW
				# cambio lo stream name per essere quella giusta da mandare alla lib Wow
				# per adesso assumo che il Wirecast sia sempre attivo
				# poi mi preoccupero di accenderlo
				[ streamName , details_id ] = Prendi_Id_StreamName( _account_details['VideoDetails'] )
				_account_details['VideoDetails']['wow_secure_stream'] = streamName 
				#_account_details['VideoDetails']['wow_name'] =  item['ECEDetails']['id'] + '_post:' + details_id + '_account:' + socialDetails['livestreamingaccount']
				_account_details['VideoDetails']['wow_name'] =  'article:' + key + '_post:' + details_id + '_account:' + socialDetails['livestreamingaccount']


			logger.debug('---------------------------------- init VideoDetails ---------------------------')
			logger.debug( _account_details['VideoDetails']['wow_name'] + ' ' +  _account_details['VideoDetails']['wow_secure_stream']  + ' ' + socialDetails['incoming_stream'] + '_1080p' + ' ' + pySettaVars.WOW_SERVER )
			logger.debug('----------------------------------- end VideoDetails ---------------------------')


			# devo verificare se esistono gia anche i WowzaDetails
			if 'WowzaDetails' in YTEntry:
				_account_details['WowzaDetails'] = YTEntry['WowzaDetails'] 
			else:

				# CLAD -> per YT mando su il 1080p aggiungendo alla fine del item['SocialFlags']['incoming_stream'] un _1080p
				wow = pyWowzaClass.Create_Mapentry_YT( _account_details['VideoDetails']['wow_name'] , _account_details['VideoDetails']['wow_secure_stream'], socialDetails['incoming_stream'] + '_1080p', pySettaVars.WOW_SERVER )
				logger.debug(wow)
				if wow['success'] :
					# 3 - se ritornato success : true
					# 4 - chiederne i details con Get_Wowza_Mapentry_Details da salvare in lis 

					_account_details['WowzaDetails'] = pyWowzaClass.Get_Wowza_Mapentry_Details( _account_details['VideoDetails']['wow_name' ] , pySettaVars.WOW_SERVER )
					# per successive modifiche con Action_Mapentry( nome, 'enable/disable')
					# o per la semplice Delete_Mapentry( nome )

					
					result_enable = pyWowzaClass.Action_Mapentry( _account_details['VideoDetails']['wow_name' ] , 'enable', pySettaVars.WOW_SERVER )

					# DEBUG CLAD
					# result_enable = {u'message': u'Entry (article:9845290_post:2334040573294315_account:testare_e_bello) enabled successfully', u'data': None, u'success': True}
					# DEBUG CLAD

					logger.debug( result_enable)
					if result_enable['success']:
						_account_details['WowzaDetails']['enabled'] = True
				else:
					logger.debug( 'passo da wow = NOT success' ) 



			# qui inizializzo la WOWZADETAILS
			logger.debug('----------------------------------- init WowzaDetails ---------------------------')
			logger.debug( _account_details['WowzaDetails'])
			logger.debug('----------------------------------- end WowzaDetails ---------------------------')
		
			#logger.debug( _account_details )
			# qui devo verificare che non ce ne siano dei pezzi gia' fatti
			# se ci sono gia li metto nell account_details
			if 'GeoBlockDetails' in YTEntry and 'OwnerDetails' in YTEntry['GeoBlockDetails'] and  len(YTEntry['GeoBlockDetails']['OwnerDetails']) > 0 and 'AssetDetails' in YTEntry['GeoBlockDetails'] and  len(YTEntry['GeoBlockDetails']['AssetDetails']) > 0 and 'ClaimDetails'  in YTEntry['GeoBlockDetails'] and  len(YTEntry['GeoBlockDetails']['ClaimDetails']) >  0:
				# quindi se esiste la entry per quell account 
				# DEVONO  esistere i valori di GeoBlockDetails
				# allora verifica che ci siano tutti e 
				# allora li copio in _account_details e li uso per produrre i WowzaDetails
				# se questi non esistono
				logger.debug('setto _account_details sul valore che mi arriva da DB ' )

				_account_details['GeoBlockDetails'] = YTEntry['GeoBlockDetails']
			else:
				logger.debug( ' creo nuovi Asset Ownership Claim se Geo')  
				if socialDetails['isGeoBlocked'] == True or socialDetails['isGeoBlocked'] == 'true' or 'True' in socialDetails['isGeoBlocked']:
					# sono geobloccato devo far partire la pantomima della ownership
					# chiamando la procedura di pyYoutubeClass
					logger.debug( ' creo nuovi Asset Ownership Claim perche GEO')
					_account_details['GeoBlockDetails'] = pyYoutubeClass.Crea_Asset_Ownership_Claim( _account_details['VideoDetails']['LivebroadcastDetails']['id'], socialDetails['videoTitle'], 'yt_account_test_elvezia', YTEntry ) 

					if 'error' in _account_details['GeoBlockDetails']:
						logger.error('ERROR: in Crea_Asset_Ownership_Claim  = ' + str(_account_details['GeoBlockDetails']['error'] ))
						_account_details = Smonta_Details( _account_details )
						_account_details = {}
						continue
				else:
					logger.debug( ' metto GeoDetails a {}  perche NON GEO')
					_account_details['GeoBlockDetails'] = {}


			# qui inizializzo la GeoBlockDetails
			logger.debug('----------------------------------- init GeoBlockDetails ---------------------------')
			logger.debug( _account_details['GeoBlockDetails'])
			logger.debug('----------------------------------- end GeoBlockDetails ---------------------------')
				
			if 'WowzaDetails' in _account_details and 'VideoDetails' in _account_details:
				videoDetails = _account_details['VideoDetails']
				logger.debug( 'CLAD -> id = ' + videoDetails['LivestreamsDetails']['id'] )

				# qui per accendere devo verificare ( con timeout ? ) che il livestreaming sia active
				# e quindi settare poi il broadcast 

				if not 'active' in videoDetails['LivestreamsDetails']['status']['streamStatus'] :
					args = ()
					kwargs = { 'status_param' : 'active', 'item_id' : videoDetails['LivestreamsDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
					if not wait_until(  pyYoutubeClass.Check_LiveStream_Status, 15, 2.00,*args, **kwargs ):
						# qui lo smonta details deve essere probabilmente piu furbo
						# per non trovarci in situazioni inconsistenti
						# o forse non devo proprio smontare nulla .... basta mettere 
						# videoDetails['LivestreamsDetails']['status']['streamStatus'] = 'ready'
						# per reiniziare a fare i giri giusti ?
						_account_details = Smonta_Details( _account_details )
						_account_details = {}
						continue

				logger.debug( 'passo primo check')
				# e setto il valore di status 
				videoDetails['LivestreamsDetails']['status']['streamStatus'] = 'active'
				

				# verificare che sia veramente a testing
				# per non rifarlo dopo che e' gia stato fatto
				if not 'testing' in videoDetails['LivebroadcastDetails']['status']['lifeCycleStatus'] and not pyYoutubeClass.Check_LiveBroadcast_Status( 'testing', videoDetails['LivebroadcastDetails']['id'],socialDetails['livestreamingaccount']):
				
					result_update = pyYoutubeClass.Update_LiveBroadcast(  videoDetails['LivebroadcastDetails']['id'], "testing" , socialDetails['livestreamingaccount'], socialDetails['continuousLive' ] )

					# verificare che sia veramente a testing
					args = ()
					kwargs = { 'status_param' : 'testing', 'item_id' : videoDetails['LivebroadcastDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
					if not wait_until( pyYoutubeClass.Check_LiveBroadcast_Status, 25, 1.30,*args, **kwargs ):
						_account_details = Smonta_Details( _account_details )
						_account_details = {}
						continue
			
					# e setto il valore di status 
					videoDetails['LivebroadcastDetails']['status']['lifeCycleStatus'] = 'testing'


			YTEntry['GeoBlockDetails'] = copy.deepcopy( _account_details['GeoBlockDetails'] )
			YTEntry['VideoDetails'] = copy.deepcopy( _account_details['VideoDetails'] )
			YTEntry['WowzaDetails'] = copy.deepcopy( _account_details['WowzaDetails'] )
			YTEntry['SocialDetails']['notificato'] = True

			_account_details = {}

		except Exception as e:

			logger.error('ERROR: EXCEPT in PreparaYT  = ' + str(e))
			_account_details = Smonta_Details( _account_details )
			_account_details = {}
			# se ho fatto un errore per questo account, devo cancellare 
			# i VideoDetails di quell'account SOLAMENTE e la entry su YT ed eventualmente 
			# le entry sul Wowza e i WowzaDetails
			pass


	logger.debug(" ---------------------- DEBUG PreparaYT  ------------------ ")
	logger.debug(item )
	logger.debug(" ---------------------- DEBUG PreparaYT  ------------------ ")

	logger.debug(" ---------------------- FINE PreparaYT  ------------------ ")
	return item

def VerificaON( item ):


	FB = True
	YT = True

	logger.debug('  ')

	if len(item['SocialFlags']['fb-livestreaming-account']) > 0:


		_fb_accounts = item['SocialFlags']['fb-livestreaming-account']
		for key,FBEntry in _fb_accounts.iteritems():

			if not 'VideoDetails' in FBEntry:
				# poi magari questa e condizione di cancellazione
				logger.debug('\n')
				FB = FB and False
				continue

			if not 'status' in FBEntry['VideoDetails']:
				logger.debug('\n')
				FB = FB and False
				continue


			logger.debug('- Status item FB - ' +  key + ' : ' + FBEntry['VideoDetails']['wow_name'] + ' = ' + FBEntry['VideoDetails']['status'])

			if 'LIVE' in FBEntry['VideoDetails']['status']:

				logger.debug('- Status item FB - ' +  key + ' : LIVE con FB !! ')
				FB = FB and True
				continue
			else:
				FB = FB and False
				continue
	
	# per guardare se e' on cerco nel Video Details se lo stato e
	# LIVE altrimenti
	if len(item['SocialFlags']['yt-livestreaming-account']) > 0:
		_yt_accounts = item['SocialFlags']['yt-livestreaming-account']
		for key,YTEntry in _yt_accounts.iteritems():

			if not 'VideoDetails' in YTEntry:
				# poi magari questa e condizione di cancellazione
				logger.debug('\n')
				YT = YT and False
				continue

			if not 'LivebroadcastDetails' in YTEntry['VideoDetails']:
				logger.debug('\n')
				YT = YT and False
				continue
				
			if not 'status' in YTEntry['VideoDetails']['LivebroadcastDetails']:
				logger.debug('\n')
				YT = YT and False
				continue
				
			if not 'lifeCycleStatus' in YTEntry['VideoDetails']['LivebroadcastDetails']['status']:
				logger.debug('\n')
				YT = YT and False
				continue

			if 'wow_name' in YTEntry['VideoDetails']:
				logger.debug('- Status item YT - ' +  key + ' : '  +  YTEntry['VideoDetails']['wow_name'] + ' = ' + YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'])
			else:
				logger.debug('- Status item YT - ' +  key + ' : '  + YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'])
				logger.debug(' OCIO che manca WOW_NAME !!! ' )
				
			if 'live' == YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus']:
					logger.debug( '- Status item YT - ' +  key + ' : LIVE con YT !! ')
					YT = YT and True
					continue

			if 'liveStarting' in YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus']:
				# vado a verificare se sono passato a live prendendo lo status del liveBroadcast
				args = ()
				kwargs = { 'status_param' : 'live', 'item_id' : YTEntry['VideoDetails']['LivebroadcastDetails']['id'], 'account' : YTEntry['SocialDetails']['livestreamingaccount'] }
				if not wait_until( pyYoutubeClass.Check_LiveBroadcast_Status, 25, 1.30,*args, **kwargs ):
					logger.debug( '- Status item YT - ' +  key + ' : LIVESTARTING con YT !! ')
					YT = YT and True
					continue
				else:
					YTEntry['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] = 'live'
					logger.debug( '- Status item YT - ' +  key + ' : LIVE con YT !! ')
					YT = YT and True
					continue
			else:
				YT = YT and False
				continue
		
	logger.debug('  ')
	logger.debug( str(FB) + ' ' +  str(YT ))
	return [ FB, YT ]

def AccendiFB( item ):

	logger.debug(" ---------------------- INIZIO AccendiFB  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# 
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna

	if len(item['SocialFlags']['fb-livestreaming-account']) > 0:
		if 'LIVE' == item['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] :
			logger.debug(' in AccendiFB siamo LIVE !! ')
			return item


		# se sono qui devo accendere il Wow


		_fb_accounts = item['SocialFlags']['fb-livestreaming-account']
		# tra queste ce ne sono alcune che devono essere notificate
		for key,FBEntry in _fb_accounts.iteritems():
			socialDetails = FBEntry['SocialDetails']
			videoDetails = FBEntry['VideoDetails']
			wowzaDetails = FBEntry['WowzaDetails']

			logger.debug(' giro in AccendiFB con account = ' + key)

			'''
			result_enable = pyWowzaClass.Action_Mapentry( item['FBStreamingDetails'][_fb_accounts]['VideoDetails']['wow_name' ] , 'enable', pySettaVars.WOW_SERVER )

			if result_enable['success']:
				item['FBStreamingDetails'][_fb_accounts]['WowzaDetails']['enabled'] = True

			'''
			if wowzaDetails['enabled'] : 
				#result_update = pyFacebookClass.Update_Live_Video_Details(  videoDetails['id'], "status", "LIVE_NOW" , socialDetails['livestreamingaccount'] )
				result_update = pyFacebookClass.Update_Live_Video_Details(  videoDetails['id'], "status", "SCHEDULED_LIVE" , socialDetails['livestreamingaccount'] )

			if 'error' in result_update:
				videoDetails['status'] = 'LIVE_STOPPED'
				return item


			videoDetails['status'] = result_update['status']
			logger.debug(" ---------------------- DEBUG AccendiFB  ------------------ ")
			logger.debug("FBEntry['VideoDetails'] ")
			logger.debug(videoDetails)


	logger.debug(" ---------------------- FINE AccendiFB  ------------------ ")
	return item


def wait_until(somepredicate, timeout, period=0.25, *args, **kwargs):

	mustend = time.time() + timeout
	while time.time() < mustend:
		if somepredicate(*args, **kwargs): 
			return True
		time.sleep(period)
	return False

def AccendiYT( item ):

	logger.debug(" ---------------------- INIZIO AccendiYT  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# 
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna

	if len(item['SocialFlags']['yt-livestreaming-account']) > 0:
		if 'live' in item['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] :
			logger.debug(' in AccendiYT siamo LIVE !! ')
			return item


		# se sono qui devo accendere il Wow

		_yt_accounts = item['SocialFlags']['yt-livestreaming-account']

		for key,YTEntry in _yt_accounts.iteritems():
			socialDetails = YTEntry['SocialDetails']
			videoDetails = YTEntry['VideoDetails']
			wowzaDetails = YTEntry['WowzaDetails']

			logger.debug(' giro in AccendiYT con account = ' + key)

			'''
			result_enable = pyWowzaClass.Action_Mapentry( videoDetails['wow_name'] , 'enable', pySettaVars.WOW_SERVER )

			if result_enable['success']:
				wowzaDetails['enabled'] = True


			# qui per accendere devo verificare ( con timeout ? ) che il livestreaming sia active
			# e quindi settare poi il broadcast 

			args = ()
			kwargs = { 'status_param' : 'active', 'item_id' : videoDetails['LivestreamsDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
			if not wait_until( pyYoutubeClass.Check_LiveStream_Status, 10, 0.25,*args, **kwargs ):
				continue

			
			# prima a testing
			result_update = pyYoutubeClass.Update_LiveBroadcast(  videoDetails['LivebroadcastDetails']['id'], "testing" , socialDetails['livestreamingaccount'], socialDetails['continuousLive' ] )

			# verificare che sia veramente a testing
			args = ()
			kwargs = { 'status_param' : 'testing', 'item_id' : videoDetails['LivebroadcastDetails']['id'], 'account' : socialDetails['livestreamingaccount'] }
			if not wait_until( pyYoutubeClass.Check_LiveBroadcast_Status, 15, 0.25,*args, **kwargs ):
				continue
		
			'''
			# e quindi a live
			result_update = pyYoutubeClass.Update_LiveBroadcast(  videoDetails['LivebroadcastDetails']['id'], "live" , socialDetails['livestreamingaccount'] , socialDetails['continuousLive' ])

			logger.debug(result_update)

			if 'error' in result_update:
				# CLAD DEBUG 
				# da verificare se devo mettere il valore di complete sotto qui oppure
				# sotto i LiveBroadcastDetails
				videoDetails['status'] = 'complete'
				return item


			# CLAD DEBUG
			# da verificare se devo aggiungere anche ai videoDetails il valore di status
			# vedere se lo usiamo per qualche verifica da qualche parte
			videoDetails['LivebroadcastDetails'] = result_update
			logger.debug(" ---------------------- DEBUG AccendiYT  ------------------ ")
			logger.debug(' Account = ' + key)
			logger.debug(YTEntry)


			#logger.debug(" ---------------------- DEBUG AccendiYT  ------------------ ")
			#logger.debug(item['SocialFlags'])
			#logger.debug(" ---------------------- DEBUG AccendiYT  ------------------ ")


			# clean per il test chiama le delete delle entry create
			#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
			#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] , pySettaVars.WOW_SERVER )
			#logger.debug(wow_delete)
			# DA TOGLIERE

	logger.debug(" ---------------------- FINE AccendiYT  ------------------ ")
	return item

def SpegniFB( item ):

	logger.debug(" ---------------------- INIZIO SpegniFB  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'LIVE_STOPPED' == item['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] :
		logger.debug(' in SpegniFB siamo STOPPATI !! ')
		return item

	# se sono qui devo spegnere il Wow

	_fb_accounts = item['SocialFlags']['fb-livestreaming-account']

	for key,FBEntry in _fb_accounts.iteritems():
		socialDetails = FBEntry['SocialDetails']
		videoDetails = FBEntry['VideoDetails']
		wowzaDetails = FBEntry['WowzaDetails']

		result_update = pyFacebookClass.Update_Live_Video_Details( videoDetails['id'] , "end_live_video", "true",  socialDetails['livestreamingaccount']  )

		result_disable = pyWowzaClass.Action_Mapentry( videoDetails['wow_name' ] , "disable", pySettaVars.WOW_SERVER )
		if result_disable['success']:
			wowzaDetails['enabled'] = False

		if 'error' in result_update:
			videoDetails['status'] = 'LIVE_STOPPED'
			return item
		videoDetails['status'] = result_update['status']

		wowzaDetails['WowDeleted'] = pyWowzaClass.Delete_Mapentry( videoDetails['wow_name' ] , pySettaVars.WOW_SERVER )
			


	logger.debug(" ---------------------- FINE SpegniFB  ------------------ ")
	return item


def SpegniYT( item ):

	logger.debug(" ---------------------- INIZIO SpegniYT  ------------------ ")
	logger.debug(item)
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'complete' == item['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] :
		logger.debug(' in SpegniYT siamo STOPPATI !! ')
		return item

	# se sono qui devo accendere il Wow
	_yt_accounts = item['SocialFlags']['yt-livestreaming-account']

	for key,YTEntry in _yt_accounts.iteritems():
		socialDetails = YTEntry['SocialDetails']
		videoDetails = YTEntry['VideoDetails']
		wowzaDetails = YTEntry['WowzaDetails']

		result_disable = pyWowzaClass.Action_Mapentry( videoDetails['wow_name' ] , "disable", pySettaVars.WOW_SERVER )
		if result_disable['success']:
			wowzaDetails['enabled'] = False

		result_update = pyYoutubeClass.Update_LiveBroadcast( videoDetails['LivebroadcastDetails']['id'] , "complete", socialDetails['livestreamingaccount'], 'dummyContinuousLive')


		if 'error' in result_update:
			videoDetails['LivebroadcastDetails']['status']['lifeCycleStatus'] = 'complete'
			wowzaDetails['WowDeleted'] = pyWowzaClass.Delete_Mapentry( videoDetails['wow_name' ] , pySettaVars.WOW_SERVER )
			return item
		videoDetails['LivebroadcastDetails'] = result_update

		wowzaDetails['WowDeleted'] = pyWowzaClass.Delete_Mapentry( videoDetails['wow_name' ] , pySettaVars.WOW_SERVER )
		

		#logger.debug(" ---------------------- DEBUG SpegniYT  ------------------ ")
		#logger.debug(item['SocialFlags'])
		#logger.debug(" ---------------------- DEBUG SpegniYT  ------------------ ")


		# clean per il test chiama le delete delle entry create
		#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
		#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] , pySettaVars.WOW_SERVER )
		#logger.debug(wow_delete)
		# DA TOGLIERE

	logger.debug(" ---------------------- FINE SpegniYT  ------------------ ")
	return item

def GestisciPREPARA( dict_items ):

	logger.debug(" ---------------------- Inizio GestisciPREPARA  ------------------ ")
	#logger.debug(" dict_items =  " + str( dict_items ))
	result_dict = {}
	
	# devo verificare che sia gia stato preparato
	# altrimenti connetterlo
	# qui giro sulla lista preaparata dalla Controlla_Social_Flags
	for key, value in dict_items.iteritems():
		#logger.debug(value)
		if len(value['SocialFlags']['fb-livestreaming-account']) > 0:

			'''
			if not 'FBStreamingDetails' in value or not value['SocialFlags']['fb-livestreaming-account'][0] in value['FBStreamingDetails']:
				logger.debug(' gestisciPrepara dice che devo farlo FB')
				#CLAD
				value['FBStreamingDetails'] =  PreparaFB( value )
			else:
				logger.debug(' gestisciPrepara dice che esiste gia FB')
				# verifico se manca la parte di Wowza ed eventualmente la metto
				
			CLAD Questo diventa : 
			'''

			logger.debug(" passo a preparare ")
			value =  PreparaFB( value )
			result_dict[key] =  value 

	# devo verificare che sia gia stato preparato
	# altrimenti connetterlo
	for key, value in dict_items.iteritems():
		if len(value['SocialFlags']['yt-livestreaming-account']) > 0:
			value = PreparaYT( value )
			result_dict[key] =  value 

	logger.debug(" ---------------------- Fine GestisciPREPARA  ------------------ ")
	return result_dict


def GestisciON( dict_items ):

	logger.debug(" ---------------------- Inizio GestisciON  ------------------ ")
	logger.debug( dict_items )
	logger.debug(" ---------------------- Inizio GestisciON  ------------------ ")
	result_dict = {}

	# devo verificare che sia gia stato acceso
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		logger.debug( 'giro per FB: ' + key)
		if len(value['SocialFlags']['fb-livestreaming-account']) > 0:
			# cioe se ci sono degli account FB da accendere
			if QualcunoMancaStreamingDetailsFB( value ) :
				logger.debug('NOT videDetails')
				value = PreparaFB( value )
			else:
				# il VerificaON ritorna [ FB , YT ] 
				# e quindi con [0] prendo FB
				if not VerificaON( value )[0]:
					logger.debug('not VerificaON')
					value = AccendiFB( value )

			logger.debug('setto il FB result = ' + str(value))
			result_dict[key] = value 	
		else:
			logger.error( 'la len di value[SocialFlags][fb-livestreaming-account] == 0')

	# devo verificare che sia gia stato acceso
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		logger.debug( 'giro per YT: ' + key)
		if len(value['SocialFlags']['yt-livestreaming-account']) > 0:
			if QualcunoMancaStreamingDetailsYT( value ) :
				logger.debug('NOT videDetails')
				value = PreparaYT( value )
			else:
				# il VerificaON ritorna [ FB , YT ] 
				# e quindi con [-1] prendo YT
				if not VerificaON( value )[-1]:
					logger.debug('not VerificaON')
					value = AccendiYT( value )

			logger.debug('setto il YT result = ' + str(value))
			result_dict[key] = value 	
		else:
			logger.error( 'la len di value[SocialFlags][yt-livestreaming-account] == 0')

	logger.debug(" ---------------------- Fine GestisciON  ------------------ ")
	return result_dict


def GestisciOFF( dict_items ):

	logger.debug(" ---------------------- Inizio GestisciOFF  ------------------ ")
	result_dict = {}
	da_archiviare_dict = {}

	# devo verificare che sia gia stato spento
	# altrimenti spegnerlo
	for key, value in dict_items.iteritems():
		if len(value['SocialFlags']['fb-livestreaming-account']) > 0:

			if not 'VideoDetails'  in value['SocialFlags']['fb-livestreaming-account'].values()[0]:
				# poi magari questa e condizione di cancellazione
				logger.debug('NOT videDetails')
				continue
			if value['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] == 'LIVE_STOPPED' :
				# per adesso lo metto ancora 
				# poi magari questa e condizione di cancellazione
				logger.debug('Video LIVE_STOPPED')
				da_archiviare_dict[key] = value 	
				continue
			else:
				value = SpegniFB( value )

			result_dict[key] = value 	

		else:
			# poi magari questa e condizione di cancellazione
			logger.error(' lunghezza di value[FBStreamingDetails] == 0' )



	# devo verificare che sia gia stato spento
	# altrimenti spegnerlo
	for key, value in dict_items.iteritems():
		if len(value['SocialFlags']['yt-livestreaming-account']) > 0:

			if not 'VideoDetails' in value['SocialFlags']['yt-livestreaming-account'].values()[0]:
				# questa verifica se ci sono i video e o i wowza details
				# poi magari questa e condizione di cancellazione
				continue

			if not 'LivebroadcastDetails' in value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']:
				continue
			if not 'status' in value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']:
				continue
			if not 'lifeCycleStatus' in value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']:
				continue

			logger.debug('liveBroadcast status : ')

			logger.debug( value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'])

			if value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] == 'complete' :
				# per adesso lo metto ancora 
				# poi magari questa e condizione di cancellazione
				da_archiviare_dict[key] = value 	
				continue
			else:
				value = SpegniYT( value )

			result_dict[key] = value 	
		else:
			logger.error(' lunghezza di value[YTStreamingDetails] == 0' )

	logger.debug(" ---------------------- Fine GestisciOFF  ------------------ ")
	return [ result_dict, da_archiviare_dict ]


def Merge_Liste( da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF, da_fare_list_NOTIFICA):
	
	da_fare_list_PREPARA.update(da_fare_list_ON)
	da_fare_list_PREPARA.update(da_fare_list_OFF)
	da_fare_list_PREPARA.update(da_fare_list_NOTIFICA)

	return da_fare_list_PREPARA

def Merge_Dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def Update( idX ):
	try:  
		audioFile = ''
		durata = '000.000'
		[ result, audioUri, duration ] = GetRestJson.GetAudioDurationFromPlay( idX )
		print [ result, audioUri, duration ]
		
		if not result :
			logger.debug( 'ERRORE in get play : ' + audioUri)
			return False
		else:
			durata = duration

		logger.debug(' che dura : ' + durata)

		Prendi_Content_Da_ECE.UpdateTransAudioDuration( idX, durata )
	except Exception as e:
		logger.error( 'ERRORE: ' + str(idX) + ' a causa di : '  + str(e))
		return False
	
if __name__ == "__main__":


	

	print '-\n'
	print '------------ INIT ---------- pyRipassoDuration.py ---------------------'
	#print os.environ

	# questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
	SettaEnvironment( False )

	print '\n\n log configuration file : ' +  pySettaVars.LOGS_CONFIG_FILE + '\n'
	logging.config.fileConfig(pySettaVars.LOGS_CONFIG_FILE)
	logger = logging.getLogger('pyTransAudioDuration')
	print ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n'

	###### DEBUGGGG ##############
	#lista =  Put_Content_In_ECE.Put_IdProd( 161451, './_cambiamento_') 
	#logger.debug(lista)
	#lista_ripasso = [ 10251143, 3980056, 6907184, 6454951, 2544508, 6555820, 6029485, 8222842, 8447953, 6144238, 6294178, 7160693, 6432505, 12106051, 4945354, 3190456, 7953941, 8831782, 7643930, 6157594, 1027093, 6039469, 9150010, 9927169, 12097705, 11071449, 12428157, 10370309, 10638753, 12130966, 4982923, 2719681, 6041662, 4921348, 5730289, 5349802, 5783044, 3823543, 10928609, 2415421, 8790019, 1463308, 7100216, 9103555, 3744619, 11774336, 11685887, 9785419, 6207748, 6493780, 9033592, 5752168, 4136216, 7480313, 11088465, 3654145, 7118432, 9630343, 3519804, 12060816 ]
	#lista_ripasso = [ 3980056, 6454951, 6555820, 6029485, 6294178, 7643930, 9927169, 12097705, 5783044, 3823543, 2415421, 6207748, 6493780, 7480313, 11088465, 9630343, 3519804 ]

	#lista_ripasso = [ 9411334, 11746118, 5993236, 11357016, 8327869, 4750153, 4105472, 12520719, 9107041, 4046969, 9821614, 5999605, 6565573, 3721054, 7462469, 7462460, 7462466, 1181077, 11048337, 6450511, 4405186, 9783310, 10776887, 7250180, 555151, 6605161]
	#lista_ripasso = [ 5993236, 4750153, 4105472, 4046969, 9821614, 5999605, 3721054, 7462469, 7462460, 7462466, 6450511, 4405186, 6605161 ]
	#lista_ripasso = [ 12623712, 12623712, 12623712, 12619185, 12619176, 12619170, 12616554, 12616227, 12615327, 12615324, 12615321, 12615303, 12615300, 12615264, 12615261, 12601311, 12601296, 12601272, 12601263, 12601173, 12601134, 12600552, 12596400, 12589275, 12556383, 12395490, 12384249, 12328806 ]

	#lista_ripasso = ['12683328', '12683325', '12683310', '12682761', '12680178', '12680175']
	
	#lista_ripasso = [ 12883547, 12886073, 12883547, 12883547, 12808014, 12808017, 12808020, 12808023, 12808026, 12800346, 12814596, 12834409, 12828460, 12810903, 12875876, 12897845, 12800094, 12800097, 12800106, 12800091, 12800103, 12823293, 12823299, 12823302, 12881681, 12881651, 12898670, 12812808, 12812808, 12886073, 12886073, 12807435, 12810651, 12779364, 12779367, 12807603, 12807606, 12807609, 12807612, 12807618, 12807789, 12807792, 12807795, 12807798, 12807804, 12779370, 12779373, 12779376, 12807615, 12855178, 12855187, 12855181, 12878825, 12881942, 12881945, 12881948, 12881951, 12881954, 12897398, 12897401, 12897413, 12897407, 12897410, 12789318, 12789321, 12789324, 12885212, 12885203, 12885206, 12885185, 12885191, 12863531, 297318, 12807897, 12804450, 12800070, 12814563, 12814566, 12823290, 12850711, 12855139, 12855160, 12878660, 12893414, 12830968, 12830977, 12830974, 12830998, 12830980, 12805506, 12828469, 12814578, 12834391, 12858655, 12882674, 12814488, 12801291, 12834403, 12855049, 12821280, 12870719, 12858784, 12875996, 12810897, 12836479, 12801279, 12805629, 12814524, 12814527, 12814530, 12814494, 12827656, 12866072, 12866069, 12894107, 12814515, 12835267, 12835270, 12881984, 12811410, 12835276, 12807897, 12807897, 12892421, 12892421, 12855040, 12878615, 12896771, 12896771, 12801039, 12830527, 12858700, 12859108, 12811479, 12861733, 12861733, 12834418, 12858640, 12801765, 12878447, 12810651, 12810651, 12828541, 12828541, 12882938, 12882938, 12898670, 12898670, 12831070, 12654783, 12654855, 12654951] 
	
	lista_ripasso_settembre = [13446358,13446505,13446553,13446559,13446559,13446634,13447000,13447426,13447432,13447471,13447471,13447474,13447474,13447867,13447867,13448038,13448038,13448041,13448041,13448110,13448185,13448197,13448200,13448254,13448296,13448296,13448302,13448947,13448998,13449001,13449001,13449034,13449952,13450075,13450096,13450096,13450150,13450384,13450402,13450414,13450414,13450417,13450417,13450420,13450489,13450591,13450774,13450951,13450954,13450954,13451014,13451044,13452712,13452736,13452736,13452754,13452754,13452772,13452781,13452781,13452832,13453858,13453858,13453906,13453978,13453978,13453981,13453987,13453990,13454029,13454107,13454110,13454116,13454173,13454428,13454770,13454782,13454782,13454917,13454998,13455025,13455049,13455328,13455328,13455793,13455823,13455823,13455826,13455826,13455847,13455871,13455871,13455889,13456807,13456840,13456861,13456861,13456867,13456867,13456870,13456870,13456876,13456912,13457083,13457485,13457485,13457584,13457587,13457611,13457626,13457626,13457776,13458271,13458274,13458280,13458586,13458970,13459027,13459033,13459033,13459273,13459279,13459696,13459756,13459756,13459834,13459834,13459837,13459837,13459840,13459840,13460176,13460176,13460179,13460179,13460182,13460182,13460326,13460356,13460356,13461163,13461184,13461226,13461226,13462531,13462747,13462747,13462750,13462750,13462753,13462753,13462759,13462759,13462819,13462819,13463059,13463257,13463257,13464184,13464220,13464220,13464223,13464223,13464280,13464424,13464424,13465381,13465432,13465432,13465435,13465435,13465459,13465471,13465471,13465519,13465567,13466041,13466050,13466107,13466236,13466335,13466623,13466626,13466626,13466662,13466662,13466770,13466776,13466776,13467823,13469794,13469830,13469833,13469833,13469980,13470310,13470310,13470391,13470427,13470427,13470451,13470469,13470469,13470493,13470583,13471330,13471333,13471339,13471342,13471345,13471354,13471360,13471369,13471537,13471540,13471540,13471579,13471579,13471636,13471666,13471669,13471717,13472473,13472581,13472584,13472584,13472632,13472743,13472743,13473325,13473553,13473562,13473562,13473604,13473703,13473997,13474000,13474018,13474018,13474021,13474021,13474024,13474024,13474057,13474168,13474576,13474582,13474582,13474633,13475512,13475518,13475809,13475848,13475851,13475851,13475977]

	lista_ripasso_ottobre = [13476943,13477054,13477054,13477132,13477432,13477450,13477510,13477513,13477729,13477729,13477768,13477954,13478368,13478695,13479574,13479574,13479583,13479583,13479628,13479628,13480045,13480108,13480627,13480627,13480798,13480801,13480948,13481197,13481164,13481164,13481140,13481140,13481131,13481131,13481098,13481098,13482061,13482061,13482130,13482130,13482826,13482826,13483246,13483246,13483663,13483663,13483759,13483759,13484536,13484536,13484917,13484917,13485208,13485208,13485241,13485241,13485373,13485373,13485502,13485502,13485517,13485517,13485526,13485526,13485559,13485559,13485562,13485562,13485580,13485580,13485754,13485754,13485769,13485835,13485835,13487122,13487122,13487149,13487149,13487845,13489378,13489378,13489723,13489723,13489771,13489771,13490026,13490026,13490908,13491505,13491505,13491517,13491541,13491541,13492189,13492189,13492243,13492315,13492846,13493149,13493149,13493203,13493203,13493701,13494121,13494520,13494532,13494589,13494592,13494595,13494628,13494631,13494679,13494688,13494730,13494742,13494742,13494745,13494967,13494991,13494994,13495003,13495003,13495030,13495039,13495087,13495135,13495138,13495156,13495333,13495333,13495711,13495711,13495894,13495894,13496044,13496044,13496065,13496065,13496164,13496164,13496170,13496170,13497214,13497214,13498543,13498543,13498678,13498678,13499056,13499056,13499119,13499119,13500379,13500928,13501117,13501222,13501222,13501288,13501288,13501315,13501315,13501336,13501336,13501339,13501339,13501774,13501774,13501972,13501972,13502425,13502488,13502518,13502518,13502524,13502524,13502503,13502503,13502674,13502674,13502728,13502728,13502803,13502803,13502818,13502818,13502824,13502824,13503370,13503370,13503376,13503376,13503679,13503694,13503694,13503907,13503907,13504159,13504159,13504651,13504651,13505713,13505713,13505866,13505866,13505887,13505887,13505914,13505914,13505926,13505926,13505950,13505950,13506076,13506076,13506739,13506739,13506904,13506904,13506973,13506973,13506985,13506985,13506991,13506991,13506997,13506997,13507009,13507009,13507015,13507015,13507027,13507027,13507054,13507054,13507081,13507081,13507189,13507189,13507225,13507225,13507318,13508419,13508419,13508446,13508446,13508521,13508521,13508671,13508698,13508767,13508761,13508797,13508824,13508851,13508872,13508995,13509034,13509094,13509100,13509142,13509205,13509208,13509232,13510522,13510624,13511377,13511377,13512484,13513069,13513861,13514065,13514065,13514074,13514074,13514236,13514236,13514332,13514371,13514371,13514380,13514380,13514407,13514407,13514413,13514413,13514428,13514428,13515043,13515481,13515481,13515487,13515487,13515493,13515493,13515679,13515802,13515823,13515826,13515937,13515937,13516021,13516021,13516129,13516129,13516192,13516192,13516195,13516198,13516204,13516210,13516219,13516276,13516300,13516300,13516312,13516312,13517008,13517008,13517374,13517374,13517560,13517578,13517812,13517839,13517839,13517869,13517869,13517896,13517896,13518082,13518082,13518373,13518373,13518403,13518844,13518844,13519198,13519198,13519669,13519669,13520221,13520524,13521163,13521163,13521205,13521205,13521190,13521190,13521835,13521835,13521844,13521844,13521886,13521886,13521913,13521913,13522441,13522441,13522447,13522447,13522453,13522453,13520104,13523011,13523011,13523044,13523044,13523065,13523065,13523086,13523086,13523131,13523131,13523155,13523155,13523173,13523173,13523212,13523212,13523590,13523590,13523617,13523617,13523632,13523632,13523644,13523644,13523650,13523650,13523761,13523761,13523785,13523785,13524019,13524019,13524067,13524067,13524076,13524076,13524094,13524094,13524109,13524109,13524115,13524115,13524151,13524151,13524304,13524304,13524418,13524418,13524451,13524451,13524484,13524484,13524178,13524178,13524763,13524763,13524964,13524964,13525462,13525462,13525477,13525477,13525513,13525513,13525525,13525525,13525537,13525549,13525549,13525573,13525573,13525777,13525777,13525786,13525786,13525795,13525795,13525807,13525807,13525819,13525819,13525837,13525837,13526221,13526221,13526257,13526257,13526275,13526275,13527298,13527298,13527331,13527331,13527388,13527388,13527394,13527394,13527451,13527451,13528654,13528654,13528984,13528984,13529017,13529017,13529086,13529086,13529104,13529104,13529119,13529119,13529137,13531534,13531534,13531591,13531591,13533535,13533850,13533850,13535095,13535095,13535101,13535101,13535107,13535107,13535131,13535131,13535311,13535311,13535506,13535875,13535887,13535887,13536100,13536712,13536712,13538167,13538167,13538173,13538173,13538191,13538191,13538770,13538770,13539673,13539673,13539889,13539901,13540741,13540741,13541098,13541098,13543549,13543549,13543561,13543561,13543600,13543600,13543636,13543636,13543891,13543891,13544449,13544449,13544578,13544578,13544629,13544629,13544719,13544719,13544788,13544788,13544827,13544827,13545853,13545853,13546786,13546786,13546813,13546813,13546849,13546849,13546855,13546855,13546867,13546867,13546975,13547404,13547404,13547503,13547503,13547623,13547623,13547767,13547767,13547788,13547788,13548082,13548082,13548205,13548205,13548238,13548238,13549681,13550770,13550770,13553893,13554928,13554928,13555591,13555591,13555597,13555597,13555642,13555642,13555648,13555648,13555654,13555654,13555660,13555660,13555672,13555672,13555678,13555678,13556449,13556449,13556479,13556479,13556503,13556503,13556416,13556416,13556572,13556572,13556509,13556509,13556758,13556758,13556926,13556926,13557277,13557277,13557415,13557415,13557454,13557454,13557553,13557553,13557577,13557577,13557604,13557604,13557610,13557610,13557808,13557808,13558921,13558921,13558927,13558927,13558951,13558951,13560364,13562779,13562779,13562797,13562797,13563397,13563397,13563400,13563400,13563412,13563412,13563418,13563418,13563424,13563424,13563892,13563892,13564030,13564030,13477135,13477138,13477144,13477144,13477147,13477150,13477153,13477192,13477195,13477198,13477207,13477213,13477222,13477225,13477405,13477441,13477771,13477777,13477777,13477966,13477960,13477987,13478014,13478113,13478236,13478377,13478662,13478722,13478725,13478725,13478788,13479679,13479694,13479748,13479778,13479778,13479829,13479742,13479742,13479736,13479736,13479730,13479730,13479733,13479733,13479940,13479943,13473898,13480072,13480540,13480546,13480546,13480549,13480549,13480564,13480720,13480957,13481083,13481263,13481317,13481317,13481551,13481545,13481509,13482343,13482532,13482547,13482574,13482589,13482589,13482580,13482580,13482577,13482577,13482658,13482691,13482712,13482643,13482643,13482844,13482859,13482859,13482847,13482847,13482850,13482850,13482952,13483108,13483483,13484602,13484605,13484626,13484683,13484683,13484620,13484620,13484713,13484815,13484818,13484818,13484809,13484809,13484812,13484812,13484821,13484821,13484806,13484806,13484875,13485004,13485016,13485016,13378392,13485079,13485229,13485256,13485496,13485571,13485571,13486459,13486552,13486576,13486621,13486627,13486627,13486687,13486624,13486624,13486747,13486762,13486777,13485376,13486885,13487035,13487038,13487074,13487002,13487221,13487257,13487314,13487320,13487467,13487473,13487473,13487179,13487551,13487665,13487659,13487656,13487671,13487686,13487776,13487932,13487479,13487479,13487833,13488115,13489411,13489684,13489699,13489726,13489726,13489774,13490335,13490359,13490365,13490368,13490407,13490422,13490422,13490452,13490518,13490554,13490707,13491004,13487560,13490428,13490428,13491514,13491514,13491511,13491547,13491547,13491727,13492882,13493935,13493491,13493491,13494070,13494331,13494331,13494421,13494931,13495111,13495144,13495147,13495339,13495231,13495231,13495366,13495369,13495369,13495393,13495939,13496248,13496389,13496431,13496434,13496434,13496476,13496611,13496638,13496425,13496425,13496428,13496428,13496803,13491859,13497793,13497823,13497823,13497997,13498009,13498012,13498075,13498129,13498498,13498531,13498969,13498969,13499464,13499464,13499527,13498918,13499674,13497787,13497787,13498663,13498927,13498927,13498852,13498972,13497553,13497553,13498336,13498552,13499506,13499719,13499767,13499800,13499800,13499815,13499959,13499974,13499992,13499992,13500001,13500001,13499989,13499989,13499998,13499998,13499995,13499995,13500082,13500706,13500709,13500709,13500730,13500844,13501087,13501213,13501432,13501672,13501741,13501744,13501777,13501777,13501798,13501810,13502479,13502806,13502899,13502938,13502941,13502956,13502974,13502974,13502986,13502986,13503088,13503208,13502983,13502983,13503319,13503487,13503508,13503508,13503583,13503820,13503916,13504258,13504333,13504408,13504414,13504639,13504639,13505953,13505989,13505992,13506013,13506148,13506187,13506193,13506193,13506184,13506184,13506181,13506181,13506175,13506175,13506241,13506367,13506367,13506364,13506364,13506466,13506541,13506589,13506742,13506787,13506961,13507003,13507063,13507063,13507234,13507234,13507240,13507240,13507612,13507627,13507651,13507678,13507678,13507729,13507852,13507855,13507945,13508017,13508020,13508023,13508095,13507696,13507696,13508263,13507693,13507693,13508410,13508413,13508413,13508284,13508284,13508281,13508281,13508437,13508455,13508440,13508416,13508605,13508602,13508611,13508638,13508641,13508644,13508647,13508758,13508734,13508740,13508743,13508746,13508854,13508857,13508860,13508863,13508878,13508929,13508932,13508938,13508962,13508953,13509103,13509121,13509196,13509649,13510543,13510870,13510912,13510918,13510918,13510915,13510915,13510747,13510747,13510963,13511452,13511560,13511578,13511593,13511608,13511608,13511605,13511605,13511638,13511749,13511752,13500280,13511911,13512049,13512052,13512493,13512529,13512538,13512541,13512544,13512547,13512694,13512586,13512586,13512766,13512775,13512778,13512808,13512583,13512583,13512832,13512925,13513120,13513381,13513390,13513552,13513489,13513525,13513750,13513789,13513798,13513798,13513951,13513684,13513684,13514308,13514491,13514515,13514536,13514545,13514545,13514605,13514635,13512811,13512811,13514731,13514737,13514851,13481536,13481536,13515523,13515559,13515559,13515730,13515883,13515982,13493851,13516075,13516066,13516183,13516426,13516465,13516471,13516471,13516522,13517260,13517380,13517437,13517437,13517455,13517536,13517581,13517590,13517692,13517860,13518547,13521367,13520587,13528177,13528324,13528996,13529605,13529644,13529677,13529662,13529725,13529683,13529809,13529830,13529773,13531123,13532530,13532545,13532662,13532689,13532761,13533187,13535839,13536310,13536331,13536337,13536340,13539784,13539814,13539829,13539859,13539082,13540726,13540096,13540366,13542265,13542814,13550635,13550644,13550662,13550668,13550698,13550713,13550788,13550791,13550797,13550824,13550890,13551154,13551766,13552201,13552213,13552228,13552339,13553377,13553776,13553776,13559515,13559752,13562239,13562458,13481317,13507945,13510915,13512775,10606626,13550698,13501345,13370085,13370307,13370343,13370343,13370355,13370361,13370361,13370415,13370415,13370445,13370451,13370451,13370475,13370589,13370616,13370583,13370583,13370580,13370580,13370706,13370757,13370805,13370805,13370835,13370874,13367772,13370934,13371057,13371018,13371105,13371105,13371114,13370847,13371153,13371153,13371021,13371162,13371186,13371264,13371303,13371303,13371516,13371522,13371567,13371570,13371576,13371582,13371627,13371651,13371663,13371534,13371732,13371756,13371759,13371891,13371978,13204479,13371990,13371999,13371987,13372005,13372002,13372011,13372044,13372068,13372068,13372116,13372182,13372182,13372185,13372185,13372203,13372209,13372209,13372230,13372230,13372236,13372236,13372275,13372275,13372416,13372419,13372419,13372428,13373310,13373310,13373355,13373367,13373367,13373460,13373460,13371084,13373532,13373622,13373655,13373655,13373676,13373718,13373718,13373733,13373733,13373748,13371870,13371870,13373751,13373772,13373772,13373787,13373790,13373793,13373853,13373868,13373940,13374249,13374276,13374282,13374291,13367829,13374309,13374315,13374330,13374342,13374378,13374420,13374426,13374426,13374306,13373838,13373838,13374570,13374573,13374573,13374993,13374993,13375134,13375167,13375176,13375179,13375197,13375218,13375242,13375242,13375233,13375317,13375335,13375374,13375392,13375455,13375521,13375521,13375578,13375578,13375575,13375575,13375569,13375569,13375581,13375581,13375503,13375503,13375665,13375557,13375557,13375680,13375680,13375554,13375554,13375668,13375668,13375671,13375671,13373829,13373829,13375674,13375674,13375677,13375677,13375710,13375740,13375743,13375743,13375767,13375767,13376124,13376124,13376745,13375434,13373928,13376958,13377045,13377099,13377099,13377108,13377138,13377138,13377141,13377141,13377201,13377228,13377087,13377087,13377084,13377084,13377090,13377090,13377264,13377273,13377270,13377261,13377456,13377456,13377462,13377459,13377477,13377468,13377528,13377591,13377615,13377648,13377675,13377675,13377774,13377846,13375107,13377948,13377954,13377960,13371735,13378128,13378212,13378212,13378248,13378248,13378257,13378257,13378314,13378314,13378416,13378425,13378428,13378449,13378476,13378506,13378530,13378548,13378548,13378602,13378776,13378776,13378785,13378785,13378821,13378821,13378842,13378893,13378905,13378932,13378932,13378974,13378989,13378989,13379055,13379055,13378992,13378992,13379061,13379061,13379064,13379064,13379067,13379067,13379100,13379109,13379109,13379115,13379142,13379142,13379160,13379160,13379442,13379445,13379445,13354635,13379961,13377798,13380135,13380135,13380216,13380216,13377612,13380333,13380531,13380531,13380558,13380567,13380567,13380528,13380528,13380522,13380522,13380576,13380576,13380603,13380630,13380693,13380675,13380699,13380702,13380705,13380729,13380756,13380735,13380768,13380732,13380816,13380876,13380825,13380864,13380888,13380900,13380930,13380963,13380942,13380984,13381008,13381062,13381059,13381128,13381176,13381176,13381185,13381221,13381257,13381257,13381344,13381344,13381362,13381533,13381533,13381551,13381551,13381281,13381281,13381797,13381839,13381860,13381896,13381914,13381950,13381953,13381959,13381986,13381980,13382031,13382031,13382049,13382232,13382241,13382382,13382382,13382388,13382388,13382391,13382391,13382424,13382406,13382406,13382415,13382415,13382394,13382394,13382403,13382403,13382412,13382412,13382397,13382397,13382421,13382421,13382481,13382481,13382499,13382679,13382514,13382514,13383501,13383501,13383564,13380939,13383585,13383585,13381122,13383705,13383810,13383849,13383891,13383909,13383909,13353516,13384020,13375449,13383858,13383858,13383867,13383867,13383864,13383864,13383861,13383861,13384182,13384185,13384185,13384248,13384302,13384302,13384314,13384563,13384563,13384341,13384341,13384338,13384338,13384590,13384617,13384617,13384638,13384659,13384659,13384662,13384662,13378542,13349229,13384737,13381851,13384794,13384794,13384803,13384863,13384854,13384881,13384914,13384914,13384962,13384971,13384971,13384980,13384980,13385013,13385037,13385037,13385460,13385460,13381113,13385859,13385859,13385016,13385016,13385019,13385019,13386240,13386261,13386270,13386297,13377564,13386441,13386411,13386411,13386408,13386408,13386414,13386414,13386597,13386603,13386603,13386606,13386606,13386681,13386684,13386684,13378794,13386732,13386732,13386735,13386756,13386756,13386513,13386513,13386768,13386960,13386960,13386762,13386762,13386774,13378383,13377828,13387026,13387026,13387032,13386825,13386831,13387140,13387140,13387158,13387857,13380978,13387872,13387872,13387929,13387929,13387965,13388025,13388040,13388040,13388070,13388070,13380927,13389036,13389045,13389045,13389252,13389309,13389309,13389330,13389351,13389351,13389357,13389357,13389333,13389333,13389339,13389339,13389342,13389342,13389345,13389345,13389375,13389408,13389447,13389555,13389558,13389567,13389576,13389573,13389624,13371765,13389795,13389780,13389918,13389918,13389924,13389924,13378791,13389984,13389999,13390023,13390104,13390107,13390053,13390110,13390119,13390113,13389675,13390116,13390125,13390128,13390152,13390143,13390143,13390308,13390440,13390440,13390443,13390446,13390620,13390644,13390701,13390698,13390695,13390689,13390665,13390683,13390854,13390875,13390896,13390998,13391004,13391004,13391010,13390869,13391043,13391043,13391052,13391067,13391067,13391100,13391088,13391112,13391112,13391190,13391190,13391106,13391208,13391214,13391232,13391232,13391277,13391277,13391274,13391274,13391268,13391268,13391280,13391289,13391289,13391328,13391340,13391340,13391361,13391703,13391703,13391337,13391337,13391331,13391331,13345473,13392075,13392075,13392081,13392081,13392183,13392255,13392255,13382091,13392339,13392387,13392405,13392408,13392408,13392465,13392585,13392630,13392633,13392636,13392603,13392645,13392675,13392675,13392780,13392819,13392825,13392831,13392837,13392861,13392870,13392888,13392891,13392897,13284240,13284240,13284360,13284360,13310688,13310688,13311927,13311927,13312398,13312398,13320177,13320177,13332204,13332204,13338006,13338006,13343508,13343508,13343514,13343514,13354575,13354575,13355661,13355661,13361148,13361148,13110387,13110387,13122375,13122375,13129953,13129953,13150917,13150917,13161561,13161561,13170213,13170213,13175814,13175814,13177041,13177041,13182168,13182168,13184067,13184067,13186125,13186125,13187822,13187822,13188813,13188813,13195677,13195677,13220430,13220430,13226052,13226052,13226871,13226871,13226886,13226886,13243878,13243878,13251081,13251081,13253502,13253502,13253469,13253469,13254945,13254945,13254963,13254963,13271451,13271451,13393113,13393116,13393245,13393428,13393428,13393353,13393494,13393506,13393518,13393518,13393524,13393524,13393548,13393098,13393098,13393101,13393101,13392375,13392375,13393647,13393680,13393836,13393836,13393920,13393944,13394112,13394112,13394268,13394274,13394280,13394280,13394277,13394304,13394322,13394322,13394298,13394358,13394421,13394424,13394427,13394430,13394472,13389495,13389765,13378983,13370736,13394496,13394496,13375716,13375716,13375713,13375713,13351551,13371573,13377471,13377471,13372071,13394511,13394532,13394619,13394676,13394676,13394673,13394673,13394679,13394679,13394685,13394685,13394607,13394607,13394610,13394610,13394721,13394721,13394724,13394724,13394727,13394739,13394739,13394787,13394730,13394730,13394793,13394793,13394835,13375185,13395762,13389783,13377618,13390986,13381869,13396479,13399182,13399182,13389786,13396572,13399296,13399359,13399386,13399383,13399383,13399389,13399398,13399398,13399404,13399404,13399497,13399518,13399551,13399557,13399584,13380828,13399587,13399611,13399659,13399521,13399728,13399755,13399752,13399761,13399827,13399806,13399869,13399845,13399998,13400001,13400010,13400019,13400013,13400037,13400085,13400058,13400055,13400121,13400130,13400064,13400067,13400145,13400151,13399614,13400187,13400190,13400340,13400235,13400235,13400511,13400511,13400532,13400616,13400676,13400730,13400778,13400799,13400943,13400943,13400949,13400991,13401012,13401045,13401123,13401156,13401156,13401192,13401192,13401330,13401354,13401354,13401351,13401351,13401393,13401402,13401402,13401408,13354650,13402086,13402086,13398063,13396518,13398000,13398000,13396608,13397832,13397832,13398018,13398018,13396203,13397034,13397034,13398024,13398024,13396299,13396053,13396053,13399125,13399125,13397814,13398135,13396182,13396182,13398195,13398195,13398120,13396638,13396638,13396035,13396035,13396038,13396038,13398057,13397976,13397976,13396641,13396641,13396575,13396644,13396644,13396242,13396326,13402185,13402188,13402188,13389789,13402320,13402320,13402338,13402341,13402344,13402344,13402374,13402374,13402389,13402395,13402395,13402428,13402515,13402503,13402539,13402539,13402551,13402557,13402365,13402365,13402362,13402362,13399887,13380831,13402359,13402359,13402356,13402356,13402698,13402719,13402719,13402716,13402716,13402737,13402746,13402755,13402803,13402791,13402833,13402842,13402860,13402857,13402854,13402848,13402851,13402869,13402989,13402980,13403004,13403094,13403172,13403172,13403196,13403220,13403202,13403199,13403232,13403226,13403247,13403250,13403253,13403256,13403265,13403289,13403271,13403274,13403286,13403301,13403304,13403313,13403319,13403376,13403325,13403325,13403475,13403589,13403589,13403592,13403592,13403616,13403661,13403661,13403742,13403709,13403769,13403811,13403745,13403976,13404015,13404018,13404024,13404102,13404150,13404252,13404264,13404291,13404291,13404333,13404333,13404357,13404357,13404399,13404417,13404420,13404420,13404447,13404450,13390656,13405191,13401372,13390812,13404453,13404453,13389792,13405389,13405389,13404009,13405413,13405416,13405437,13402995,13405434,13405434,13405440,13405440,13405485,13405485,13405629,13405656,13405656,13405659,13405659,13405689,13401378,13405815,13405881,13405890,13405890,13405911,13405917,13405917,13406097,13406313,13406313,13406337,13406337,13406232,13406232,13406163,13406163,13406346,13349274,13406403,13406403,13406445,13381854,13406610,13406634,13406781,13406781,13406838,13406577,13406577,13406886,13406886,13406901,13406958,13406958,13400076,13406778,13406778,13407960,13408092,13408092,13404012,13408122,13408149,13408152,13408185,13401339,13408296,13408164,13408164,13408167,13408167,13408404,13408404,13408419,13408461,13408461,13408482,13408569,13408467,13408467,13408464,13408464,13408458,13408458,13408887,13409037,13409037,13408833,13408833,13408830,13408830,13408836,13408836,13378389,13390659,13409112,13409166,13409253,13409391,13390740,13409631,13409631,13409793,13409859,13409886,13409886,13409856,13409856,13409853,13409853,13380966,13390647,13358547,13410789,13396161,13410819,13410939,13410966,13410978,13410978,13410984,13410984,13410987,13410987,13411020,13411038,13411023,13411026,13411029,13411032,13411035,13411065,13411041,13411089,13411089,13411047,13411128,13411134,13411095,13411146,13411149,13410993,13410993,13411071,13410996,13410996,13411374,13411374,13411383,13411383,13411437,13411455,13410990,13410990,13411548,13411551,13411569,13411572,13411602,13411602,13411605,13411605,13411614,13411431,13411677,13411680,13411686,13411740,13411740,13411746,13411698,13411752,13411785,13411803,13411803,13411788,13411827,13411827,13411809,13411815,13411818,13411848,13411851,13411914,13396704,13411917,13411854,13411854,13411962,13412043,13412043,13412061,13412064,13412067,13412091,13412094,13412097,13412121,13412118,13412115,13412112,13412148,13412232,13401336,13412400,13412421,13412421,13412463,13412466,13412628,13412793,13412865,13412865,13412889,13412889,13412904,13412997,13412997,13413030,13413030,13413066,13413072,13413084,13413069,13413120,13413150,13413159,13413159,13413174,13413435,13413435,13404147,13410957,13410957,13413885,13414005,13414005,13414056,13414083,13414086,13414086,13414101,13414110,13414110,13414143,13414149,13414149,13414161,13414191,13414197,13414206,13414227,13414188,13414233,13414290,13414290,13414320,13414320,13414314,13414332,13414332,13414359,13414359,13414452,13414455,13414458,13414476,13414293,13414293,13414329,13414851,13414854,13390839,13414932,13414932,13414935,13414935,13414938,13412862,13415079,13415082,13415082,13415088,13415088,13415085,13415085,13415076,13415076,13415283,13414857,13418127,13418151,13414230,13418202,13418202,13418172,13418247,13418298,13418304,13418322,13418325,13418379,13418418,13418403,13418454,13418475,13418520,13418532,13418553,13418592,13418592,13418541,13418715,13418715,13418838,13418874,13418874,13418721,13418721,13418928,13419642,13419642,13419603,13419603,13419735,13419750,13419750,13414326,13420551,13420626,13420626,13420782,13420782,13418181,13421088,13424841,13425234,13425234,13425236,13425236,13425278,13425278,13425312,13425372,13425390,13425394,13425398,13425358,13425400,13425402,13425482,13425482,13425472,13425484,13425536,13425653,13425683,13425722,13425027,13425027,13424967,13424967,13425776,13425776,13425779,13425779,13425902,13425902,13425905,13425920,13426001,13411458,13426061,13425764,13426613,13426655,13426655,13426652,13426652,13426649,13426649,13426616,13426694,13426760,13426793,13425129,13426400,13426598,13426598,13426940,13426979,13426979,13427006,13427051,13427051,13427066,13415274,13415274,13427153,13427186,13427171,13427408,13427456,13427456,13427702,13427747,13427747,13427768,13427768,13427771,13427771,13427780,13427777,13427777,13427795,13427795,13427906,13427948,13427978,13427981,13427981,13428290,13428290,13425638,13425638,13424772,13424772,13427453,13425551,13428839,13426010,13429070,13429133,13429145,13429145,13429151,13429151,13429172,13429172,13429178,13429181,13429160,13429160,13429202,13429229,13429244,13429199,13429247,13429250,13429262,13429271,13429514,13429622,13429622,13429628,13429694,13429700,13425270,13429952,13429952,13430036,13430045,13430180,13430180,13430246,13430291,13430291,13430039,13430039,13430312,13430408,13430450,13430459,13412100,13430489,13430489,13430528,13430507,13430507,13430564,13430567,13430600,13430600,13430903,13430858,13430912,13430912,13430954,13430993,13430993,13430990,13430990,13431002,13431002,13431005,13431005,13431059,13431035,13431035,13431080,13431083,13431083,13431068,13431068,13431074,13431074,13431176,13382157,13432072,13432072,13432078,13429784,13411821,13432342,13432378,13432378,13432387,13432387,13432408,13432429,13432429,13432435,13432483,13432495,13432522,13432531,13432531,13432633,13432636,13432639,13432645,13432663,13432666,13432669,13432672,13432675,13432399,13432399,13432690,13432402,13432402,13432717,13432756,13432876,13432918,13432930,13432939,13432957,13432960,13432987,13432984,13433032,13433065,13433077,13433113,13433146,13433152,13433155,13433143,13433140,13433176,13433191,13433191,13433266,13433266,13433308,13433446,13433446,13433560,13433626,13433695,13433758,13433347,13433905,13433950,13433125,13433125,13433116,13433116,13434034,13434034,13434052,13434052,13434151,13434271,13434304,13434304,13434319,13434319,13434358,13434358,13434361,13434361,13434427,13434427,13434397,13434517,13434517,13434553,13434580,13434355,13434355,13434592,13434592,13434598,13434598,13434637,13434640,13434655,13434655,13434754,13434754,13432921,13411824,13433128,13436275,13436305,13436314,13436320,13436320,13436371,13433698,13436455,13408500,13436596,13436596,13436599,13436599,13436695,13436779,13436764,13436764,13436770,13436770,13436773,13436773,13436941,13437082,13437082,13437115,13437136,13437136,13425318,13355493,13437259,13381848,13437322,13437325,13437406,13437505,13437505,13437514,13437514,13437544,13437544,13437595,13437622,13438351,13438351,13433044,13438378,13438378,13438594,13438594,13438606,13438621,13438621,13433137,13433311,13438915,13438933,13438942,13438951,13438951,13433011,13439008,13439056,13439098,13439098,13439158,13439158,13439167,13439185,13439185,13439284,13439284,13439206,13439206,13439209,13439209,13439203,13439203,13439191,13439191,13433755,13439296,13439413,13439413,13439416,13439416,13439461,13439590,13439590,13378386,13439629,13439629,13430537,13439650,13439680,13439680,13439722,13439746,13439755,13439842,13439854,13439854,13439878,13439959,13390749,13429619,13440175,13440244,13440244,13440391,13440391,13440394,13440394,13440421,13358550,13440271,13440271,13440274,13440274,13441204,13441204,13441276,13441276,13441279,13396158,13441333,13441405,13441429,13441429,13441438,13441444,13441444,13441483,13441462,13441462,13441492,13441564,13441606,13441585,13441588,13441618,13433134,13441639,13441687,13411557,13441690,13441792,13441798,13441768,13441774,13441777,13441780,13441819,13441783,13441858,13441882,13442050,13442050,13441954,13442104,13442119,13442164,13442272,13442281,13442278,13442308,13442395,13442395,13442446,13433962,13442485,13442506,13442563,13442566,13442578,13442611,13442689,13442824,13442866,13442803,13442794,13442788,13442800,13442881,13443394,13443403,13443469,13443463,13443595,13443592,13443607,13443607,13443601,13443658,13443658,13443691,13443697,13443697,13443703,13443703,13443742,13443784,13443799,13443799,13443826,13443826,13443829,13411626,13446007,13446007,13446010,13446010,13441426,13441426,13446058,13441861,13446073,13446073,13446358,13446505,13446508,13446529,13446553,13446559,13446559,13446634,13446715,13446790,13442437,13446739,13447324,13447324,13447330,13447330,13447360,13447375,13447384,13447000,13447003,13447426,13447432,13447429,13447471,13447471,13447474,13447474,13447537,13447537,13447540,13447540,13447594,13447597,13442404,13447729,13447744,13447744,13447735,13447702,13447801,13447921,13447921,13447960,13447960,13447867,13447867,13448041,13448041,13448038,13448038,13433965,13448110,13448137,13448137,13448245,13448290,13448296,13448296,13448293,13448299,13448302,13448254,13448320,13448332,13448374,13448197,13448308,13448185,13448200,13448458,13448455,13448455,13448461,13448515,13448605,13448701,13448701,13448722,13448782,13448830,13448830,13448884,13448884,13448752,13448752,13448947,13448983,13448983,13448998,13449001,13449001,13449025,13449025,13449034,13418301,13449196,13449196,13449403,13449403,13442887,13449634,13449634,13449952,13450021,13450021,13450048,13450075,13450087,13450087,13450093,13450093,13450096,13450096,13450150,13450165,13450333,13450330,13450354,13450387,13450384,13450402,13447369,13450411,13450420,13450414,13450414,13450417,13450417,13450426,13450489,13450516,13450537,13450666,13450681,13442428,13450702,13450747,13450750,13450768,13450774,13450792,13450792,13442422,13450801,13450825,13450825,13448002,13450948,13450948,13450954,13450954,13450993,13433971,13451014,13451044,13451056,13451050,13451140,13451158,13451185,13450951,13451209,13451206,13451218,13451374,13451899,13452229,13452430,13452430,13452439,13452493,13452520,13452520,13452541,13452577,13452577,13452589,13452589,13452595,13452595,13452424,13452424,13452712,13452718,13452718,13452745,13452745,13452772,13452775,13452775,13452781,13452781,13452754,13452754,13452736,13452736,13452829,13452829,13452832,13452976,13452976,13453231,13453231,13447633,13442890,13453834,13453834,13453843,13453843,13453906,13453975,13453978,13453978,13453981,13453984,13453990,13453987,13454029,13454020,13454026,13454059,13450591,13454089,13454173,13454098,13454233,13454239,13454296,13454119,13454116,13454113,13454110,13454107,13454101,13454362,13453858,13453858,13454425,13454428,13442467,13454473,13454476,13454497,13454548,13450348,13454626,13454653,13454653,13454770,13454782,13454782,13454827,13433974,13454899,13454935,13454941,13454905,13454908,13454911,13454917,13454998,13455025,13455049,13455061,13455109,13455115,13455115,13455118,13455124,13455130,13455292,13455349,13455391,13455421,13455538,13455586,13455676,13455676,13455688,13455700,13455700,13455754,13455754,13455751,13455751,13455766,13455766,13455793,13455847,13455871,13455871,13455889,13455328,13455328,13455898,13455898,13455823,13455823,13404030,13456504,13456504,13454125,13456648,13456648,13454188,13442893,13456756,13456756,13456807,13455826,13455826,13456810,13456840,13456852,13456852,13456861,13456861,13456876,13456888,13456888,13456912,13456915,13456948,13456870,13456870,13456867,13456867,13456924,13456957,13456957,13456972,13456990,13456990,13456987,13457008,13457014,13457029,13457068,13457068,13457092,13457083,13457104,13457122,13457119,13457116,13457188,13457188,13457275,13457275,13457236,13457293,13457365,13457374,13457377,13457452,13457452,13457272,13457455,13457455,13457575,13457584,13457587,13457626,13457626,13457611,13457485,13457485,13457629,13457644,13457683,13433977,13457704,13457719,13457800,13457776,13457803,13457809,13457809,13457818,13457875,13457893,13458199,13458217,13458280,13458271,13458358,13458274,13458391,13458391,13458409,13458478,13457131,13458538,13458586,13458580,13458592,13458592,13458607,13458610,13458691,13458751,13458757,13458832,13458832,13458970,13459027,13459033,13459033,13459273,13459279,13412430,13454437,13442896,13457632,13459546,13459696,13459699,13459717,13459750,13459756,13459756,13459900,13459936,13459936,13459966,13459966,13433512,13459834,13459834,13460023,13459840,13459840,13459837,13459837,13460035,13460074,13460074,13460107,13460143,13460161,13460161,13460125,13460125,13460212,13460326,13460179,13460179,13460356,13460356,13460182,13460182,13460176,13460176,13460407,13460407,13460440,13460440,13460446,13454794,13460602,13460602,13460617,13457581,13457353,13460725,13460770,13460902,13460902,13460944,13461109,13461109,13460893,13460893,13461157,13461157,13461163,13461184,13461226,13461226,13454350,13459954,13459954,13462276,13462276,13462237,13457638,13462531,13462567,13462570,13462582,13457338,13462702,13462780,13462780,13462786,13462819,13462819,13462894,13462759,13462759,13462747,13462747,13462750,13462750,13462753,13462753,13462930,13463059,13463218,13463257,13463257,13451200,13463365,13463542,13463542,13463587,13463752,13390752,13429631,13464061,13464061,13464100,13464100,13464184,13464280,13464424,13464424,13457362,13464220,13464220,13464223,13464223,13359201,13465156,13465156,13441741,13465381,13465429,13465459,13465465,13465465,13465471,13465471,13465492,13465492,13465519,13465567,13465954,13465435,13465435,13465432,13465432,13465978,13465978,13465960,13466032,13466032,13466047,13466041,13466050,13466056,13466065,13466107,13466098,13466119,13466212,13466236,13466260,13466281,13466314,13466335,13466389,13466398,13466449,13458613,13466596,13466749,13466749,13466662,13466662,13466770,13466776,13466776,13466785,13466785,13433980,13434019,13466626,13466626,13466935,13466935,13466938,13467010,13467154,13467235,13467262,13468279,13469581,13467823,13469635,13469716,13469716,13469713,13469713,13469722,13469725,13469761,13469776,13469776,13469794,13469800,13469800,13469821,13469821,13469830,13469833,13469833,13469980,13470235,13470235,13470310,13470310,13457647,13470391,13470421,13470451,13470457,13470457,13470469,13470469,13470493,13470529,13470580,13470586,13470586,13470583,13470544,13470427,13470427,13457806,13470553,13470616,13470643,13470676,13470682,13470688,13470688,13470694,13470562,13471219,13471249,13471309,13471309,13471204,13471330,13471333,13471342,13471357,13471354,13471351,13471348,13471360,13471339,13471345,13466623,13471366,13471366,13471369,13471378,13471279,13471393,13471414,13471414,13471537,13471540,13471540,13471549,13471549,13471636,13471669,13471672,13471672,13471666,13471717,13471579,13471579,13471840,13471840,13471864,13471891,13472041,13472074,13472074,13472119,13472194,13472200,13472350,13472350,13472344,13472344,13472347,13472347,13472341,13472341,13472389,13472419,13472440,13472473,13472581,13472584,13472584,13472605,13472605,13472632,13472743,13472743,13472956,13472956,13473082,13473082,13470355,13470355,13473160,13473160,13473280,13473280,13466215,13473325,13473487,13473553,13473562,13473562,13473523,13473523,13473577,13473577,13473604,13473601,13473601,13473664,13473652,13472422,13472422,13473676,13473703,13473754,13473778,13473802,13473793,13473871,13473961,13473997,13474000,13474012,13474057,13474063,13474063,13474123,13474120,13458067,13474168,13474294,13474312,13474318,13474318,13474024,13474024,13474108,13474396,13474396,13473526,13473526,13474543,13474543,13474567,13474567,13474576,13474582,13474582,13474591,13474591,13474672,13474672,13434022,13474798,13474018,13474018,13475131,13475131,13475134,13475134,13474021,13474021,13475191,13475182,13475146,13475257,13475332,13475371,13475416,13475416,13475506,13475509,13475518,13475539,13475641,13475641,13475689,13475692,13475722,13475746,13475746,13475764,13475764,13475803,13475803,13475797,13475797,13475809,13475800,13475800,13475824,13475824,13475818,13475827,13475827,13475848,13475851,13475851,13475977,13476301,13476301,13476460,13476460,13473613,13473613,13476586,13474633,13475512,13370343,13370415,13370451,13370583,13370580,13370805,13371870,13098699,13372068,13372209,13372185,13372230,13372236,13372275,13372182,13373310,13373367,13373460,13373655,13373733,13366959,13373772,10443680,13374426,8603374,8518315,13373850,13373838,13375242,13131180,13375521,13375575,13375569,13375578,13375557,13373829,13375674,13364232,13375503,13375716,13375713,13375668,13375581,13377099,13377138,13377141,13377087,13377090,13377084,13377093,13377471,13377456,13377675,13377849,13364232,13378248,13378314,13378257,13378776,13378785,13378821,13376124,13378932,13378989,13378992,13379055,13379061,13379160,13287453,13379142,13380531,13380576,13380528,13380522,13380135,13381176,13381257,13380135,13380216,13048845,13382031,10922384,13381344,13381344,13129245,13382382,13382406,13382415,13382481,13379160,13382391,13382388,13382514,13382394,13383501,13381281,13383585,13383858,13383867,13383864,13384185,13383861,13384302,13384341,13384338,13384617,13384659,13384662,13381851,13384794,13384914,13384971,13384980,13385460,13385859,13385016,13385019,13386606,13386411,13386408,13386414,13386684,13386732,13386762,13386756,13386606,13387026,13387140,13387872,13387929,13388070,13389045,13389309,13389339,13389345,13389342,13389333,13387026,13389918,13389924,13389447,13391004,13391043,13391067,13391190,13391232,13391268,13391289,13391274,13391277,13391112,13391703,13391331,13392075,13392081,13392255,12166456,9523678,7992023,7992107,13392675,13393428,13392375,13393836,13393101,13392675,13394112,13394280,13394322,13393098,13394496,13391289,13394679,13394673,13394724,13394721,13394685,13394739,13394730,13394730,13399182,13399383,13378983,13400235,13400943,13401156,13401192,13401351,13401354,13402188,13373310,13402344,13402539,13402362,13402719,13402716,13402365,13402557,13403172,13402356,13403325,13402719,13403661,13404291,13404333,13404357,13405389,13405485,13405434,13405440,13405659,13405890,13405917,13406337,13406163,13406232,13406403,13406778,13406781,13406886,13406577,13408164,13408167,13408404,13402557,13408467,13408464,13408458,13408092,13408836,13408836,13408833,13381854,13408830,13409631,13409856,13410978,13411089,13410993,13411374,13411383,13411020,13410996,13410990,13411602,13411605,13411740,13391004,13411803,13411854,13412865,13412889,13412421,13412997,13413030,13414005,13414149,13414290,13414320,13414332,13414359,13414293,13414332,13406337,13414935,13414932,13415088,13415076,13415085,13418592,13418715,13418874,13419603,13419750,13420626,13420782,13425236,13425278,13425234,13425482,13424967,13425776,13425779,13141071,9472399,13426979,13427051,13412865,13427456,13427768,13427771,13427777,13418721,13415076,13427795,13428290,13415274,13411803,13429145,13429160,13401123,13429622,13429172,13429172,13429952,13430291,13430489,13430507,13430600,13430954,13430993,13430291,13430990,13430039,13430912,13431002,13431035,13431068,13431074,13432072,13432378,13432387,13432531,13432399,13432402,13209642,13394679,13433191,13433125,13434034,13434052,6159976,13434304,13434319,13434361,13434358,13434427,13434517,13211115,13434355,13434598,13434655,13436320,13436596,13436599,11450627,13436773,13436770,13437136,13437514,13437505,13437544,13438351,13438378,13438594,13438951,13439098,13439158,13439284,13439203,13439209,13439191,13439206,13439416,13439413,13439209,13439629,13439680,13439854,13440244,13440391,13440271,13441204,13441276,13440274,13441426,13441429,13441687,13442050,13439284,13439854,13443607,13443697,13443703,13443826,13443658,13443607,13447324,13447330,13447540,13447537,13447744,13447960,13447921,13448137,13448455,13448137,13448701,13448884,13448983,13449025,13429622,13449196,13449403,13449634,13450021,13450087,13450093,13450792,13450825,13450948,13452430,13084722,13452520,13452577,13452589,13452595,13452718,13452424,13452745,13452775,13452829,13452976,13448752,13448983,13453231,13453834,13453843,7093913,10865432,13454653,13455115,13143636,13329114,13455676,13455700,13455751,13455766,13455754,13454653,13455898,13456504,13456648,13456852,13456888,13456990,13457068,13457275,13457452,13457455,13381848,13159671,13458391,13457809,13458592,13458832,13459936,13459954,13459966,13460074,13460161,13460407,13460440,13460602,13457581,13460902,13461109,13460893,13461157,13462276,13462780,13398000,13463542,13464061,13464100,13463542,13465465,13465492,13465978,13463542,13466032,9102679,9102634,9028708,13466749,13466785,13457353,11157693,13469713,13469716,13469776,13469800,13469821,13470235,13470355,13470457,13470586,13470688,13471309,13471366,13471414,13471549,13471672,13472341,13472350,13472344,13472347,13472422,13472074,13472605,13473082,13471840,13472956,13473160,13473280,13473577,13473601,13473613,13473577,13474063,13474396,13474318,13473523,13473526,13474543,13474672,13475131,13475134,13475146,13475257,13475416,13475641,13475746,13475764,13475800,13475824,13475827,13475797,13476301,13476460]

	lista_ripasso_ottobre = [13485502,13485502,13485517,13485517,13485526,13485526,13485559,13485559,13485562,13485562,13485580,13485580,13506739,13506739,13479679,13481551,13481545,13481509,13482343,13485571,13485571,13492882,13493491,13493491,13494331,13494331,13494421,13499464,13499464,13481536,13481536,13540366,13542265,13553776,13553776,13562458,13501345]
	lista_ripasso_novembre = [13568950, 13578850, 13587196, 13596115, 13596136, 13601860, 13603216, 13605472, 13613500, 13615828, 13618297, 13618633, 13620307, 13628311, 13629529, 13629718, 13631929, 13631176, 13632310, 13634881, 13634890, 13637257, 13637470, 13641163, 13641832, 13645840, 13646269, 13579624, 13579468, 13579492, 13455043, 13583092, 13597543, 13597549, 13597552, 13597561, 13597582, 13597600, 13597588, 13597609, 13606849, 13611007, 13614250, 13617364, 13617448, 13617814, 13622401, 13622422, 13623736, 13623766, 13624600, 13624666, 13624771, 13627081, 13627252, 13627801, 13627249, 13629382, 13630006, 13630009, 13630012, 13630285, 13630696, 13630897, 13632838, 13632895, 13632916, 13632907, 13633489, 13634032, 13639624, 13638205, 13640353, 13643005, 13644208, 13589698, 13644403, 13645942, 13592734, 11039469]

	lista_ripasso_dicembre = [13648375,13648375,13648825,13648825,13650301,13650301,13651318,13651318,13651465,13651465,13651477,13651477,13653787,13653787,13656724,13656724,13656736,13656736,13656745,13656745,13656748,13656748,13657474,13657474,13657489,13657489,13657519,13657519,13657927,13657927,13657948,13657948,13657981,13657981,13658290,13658290,13658302,13658302,13658311,13658311,13658317,13658317,13658419,13658419,13658440,13658440,13658779,13658779,13659172,13659172,13659292,13659292,13659310,13659310,13659319,13659319,13659328,13659328,13660726,13660726,13660864,13660864,13660873,13660873,13660891,13660891,13660903,13660903,13661479,13661479,13661509,13661509,13661602,13661602,13662025,13662025,13662133,13662133,13662175,13662175,13662184,13662184,13662715,13662715,13663372,13663372,13666216,13666216,13668154,13668154,13668991,13668991,13669018,13669018,13669081,13669081,13669318,13669318,13670188,13670188,13670260,13670260,13670746,13670746,13671748,13671748,13672477,13672477,13673854,13673854,13673944,13673944,13673992,13673992,13674001,13674001,13674013,13674013,13673974,13673974,13674553,13674553,13674694,13674694,13674739,13674739,13673950,13673950,13674988,13674988,13675195,13675195,13675213,13675213,13675258,13675258,13675630,13675630,13675795,13675795,13675807,13675807,13676659,13676659,13676668,13676668,13673311,13673311,13677202,13677202,13677214,13677214,13677226,13677226,13677241,13677241,13677454,13677454,13677478,13677478,13677685,13677685,13677958,13677958,13677970,13677970,13677988,13677988,13677994,13677994,13678069,13678069,13678075,13678075,13678129,13678129,13678345,13678345,13678420,13678420,13678426,13678426,13678432,13678432,13678483,13678483,13678489,13678489,13678495,13678495,13678513,13678513,13678525,13678525,13679038,13679038,13679074,13679074,13679098,13679098,13679110,13679110,13679125,13679125,13679701,13679701,13679794,13679794,13679842,13679842,13679869,13679869,13679893,13679893,13679932,13679932,13682683,13682683,13685740,13685740,13686229,13686229,13686268,13686268,13686274,13686274,13686337,13686337,13686931,13687630,13687630,13687783,13688035,13688035,13688059,13688059,13688308,13688308,13688521,13688521,13688533,13688533,13688554,13688554,13688584,13688584,13688836,13688836,13690243,13690243,13690603,13690603,13690702,13690702,13690825,13690825,13690840,13690840,13691497,13691497,13691569,13691569,13691581,13691581,13691596,13691596,13691608,13691608,13691629,13691629,13691641,13691641,13691668,13691668,13691677,13691677,13691716,13691716,13691734,13691734,13693504,13693504,13693750,13693750,13693801,13693801,13693807,13693807,13693816,13693816,13693828,13693828,13693834,13693834,13693852,13693852,13693867,13693867,13693882,13693882,13693888,13693888,13693894,13693894,13693921,13693921,13693942,13693942,13694044,13694044,13694065,13694065,13694071,13694071,13694104,13694104,13694113,13694113,13694257,13694257,13694452,13694452,13695025,13695025,13695469,13695469,13695478,13695478,13695490,13695490,13695496,13695496,13695523,13695523,13695568,13695568,13695580,13695580,13695589,13695589,13695607,13695607,13695646,13695646,13695667,13695667,13695673,13695673,13695679,13695679,13695688,13695688,13695718,13695718,13695868,13695868,13695883,13695883,13695892,13695892,13695904,13695904,13695919,13695919,13695946,13695946,13695955,13695955,13695964,13695964,13696024,13696024,13696033,13696033,13696042,13696042,13696063,13696063,13696087,13696087,13696111,13696111,13696594,13696594,13696609,13696609,13696615,13696615,13696618,13696618,13696624,13696624,13696804,13696804,13696810,13696810,13696819,13696819,13696840,13696840,13696864,13696864,13696867,13696867,13696882,13696882,13696927,13696927,13696981,13696981,13697017,13697017,13697059,13697059,13697842,13697842,13697848,13697848,13697896,13697896,13698217,13698217,13698313,13698313,13698403,13698403,13698553,13698553,13698565,13698565,13698604,13698604,13700380,13700380,13702159,13702159,13703098,13703098,13703113,13703113,13703179,13703179,13703311,13703311,13703647,13703647,13703722,13703722,13703731,13703731,13703743,13703743,13703761,13703761,13704829,13704829,13705585,13705585,13689742,13689742,13705888,13705888,13705906,13705906,13705912,13705912,13705936,13705936,13705942,13705942,13705945,13705945,13705984,13705984,13706020,13706020,13688932,13688947,13688977,13688977,13701154,13701172,13700923,13701199,13701202,13700794,13701091,13700611,13711471,13711471,13711492,13711492,13711507,13711507,13712089,13712089,13712617,13712617,13714399,13714399,13715212,13715212,13715221,13715221,13715233,13715233,13715239,13715239,13715251,13715251,13715263,13715263,13715281,13715281,13715344,13715344,13715374,13715374,13715659,13715659,13715665,13715665,13715710,13715710,13715719,13715719,13715752,13715752,13716004,13716004,13716016,13716016,13716058,13716058,13716085,13716085,13716160,13716160,13716166,13716166,13716175,13716175,13716184,13716184,13716190,13716190,13716253,13716253,13716289,13716289,13716820,13716820,13717102,13717102,13717135,13717135,13717141,13717141,13717153,13717153,13717294,13717294,13718116,13718116,13646716,13646902,13646902,13646908,13646908,13647007,13647043,13647043,13647259,13647259,13589701,13647778,13647778,13647991,13647991,13648162,13648162,13592755,13648906,13648906,13648975,13648975,13648999,13649017,13649341,13649341,13649647,13649647,13589704,13650061,13650061,13650091,13592770,13651915,13651942,13651954,13652155,13652155,13589707,13653031,13653031,13592782,13654006,13654006,13654186,13654192,13654732,13654732,13589710,13650112,13656934,13592800,13657762,13657762,13657984,13657984,13658011,13658011,13657957,13658035,13658116,13658116,13658122,13658176,13658176,13658584,13658584,13658587,13658587,13659031,13659031,13659418,13659418,13659580,13659580,13659721,13659721,13600858,13660231,13660231,13661101,13661101,13661149,13661149,13662013,13662820,13662820,13592830,13663501,13663501,13663798,13663825,13663849,13659196,13663924,13665202,13665202,13592842,13666309,13666336,13666438,13666531,13667812,13667998,13667998,13592851,13668868,13668868,13670335,13670812,13670812,13671082,13671082,13671085,13671085,13592857,13671856,13671856,13672129,13672171,13672171,13672222,13672378,13672378,13672720,13672720,13673374,13673374,13673440,13673482,13674049,13674049,13592860,13651774,13675387,13675387,13675585,13675585,13672636,13677403,13677403,13678642,13678642,13679164,13679164,13679251,13679320,13679320,13679872,13680193,13681939,13681939,13681978,13681978,13592911,13682317,13682317,13682320,13682320,13681963,13681963,13682962,13682983,13683538,13683538,13683457,13683691,13683691,13683628,13683628,13683919,13683919,13684048,13684048,13685860,13685860,13686136,13686148,13686190,13686223,13686223,13686238,13686415,13686448,13679281,13686568,13686568,13687372,13687372,13687969,13687969,13592920,13688479,13688479,13688803,13688827,13689580,13689580,13689733,13690210,13690297,13690297,13690405,13690405,13592923,13691077,13691095,13691215,13691215,13686430,13592929,13693945,13693945,13693927,13697533,13697533,13697584,13697650,13698019,13698019,13698028,13698028,13698031,13698031,13698823,13700485,13700485,13701499,13701499,13701580,13701580,13701601,13701655,13701655,13701799,13702288,13702288,13702345,13702345,13702936,13702936,13704121,13704130,13704181,13704430,13691500,13705384,13705384,13705468,13706290,13706686,13706686,13707769,13707769,13704472,13706302,13707928,13707928,13708216,13708216,13702612,13709494,13709494,13705150,13712143,13712518,13714564,13714564,13714732,13714732,13714810,13714810,13715080,13715383,13715383,13715500,13715500,13715647,13715647,13715698,13716844,13716844,13717369,13717369,13717375,13717375,13717477,13646902,13646908,13647043,13647259,13647778,13647991,13648162,13648906,13649341,13649647,13650061,13648906,13657690,13658011,13657984,13658116,13658176,13658584,13658587,13659031,13657690,13659418,13659580,13659031,13659721,13660231,13661101,13661149,13659580,13662820,13663501,13665202,13667998,13668868,13663501,13670812,13671082,13671085,13648975,13672378,13672720,13674049,13678642,13679164,13679320,13658035,13681939,13681978,13681978,13682317,13683538,13683691,13682320,13685860,13686223,13686568,13687372,13688479,13689580,13691215,13698028,13698031,13698019,13699039,13700485,13701655,13702345,13702936,13714564,13714810,13715383,13715500,13715647,13714732,13716844,13717375]

	lista_ripasso_gennaio = [13721731,13721731,13721740,13721740,13721761,13721761,13724458,13724458,13726951,13726951,13729954,13729954,13732387,13732387,13732414,13732414,13738969,13738969,13740748,13740748,13740862,13740862,13741612,13741612,13745515,13745515,13745527,13745527,13745533,13745533,13745539,13745539,13745548,13745548,13745560,13745560,13745911,13745911,13747111,13747111,13748473,13748473,13750408,13750408,13754536,13754536,13754563,13754563,13754578,13754578,13754584,13754584,13754587,13754587,13757905,13757905,13758883,13758883,13759957,13760122,13760161,13760410,13760491,13760419,13764388,13764388,13771246,13771249,13771312,13773760,13773760,13779364,13779364,13779766,13779766,13780657,13780657,13781158,13783327,13783327,13783390,13783390,13785481,13785481,13787209,13787209,13787935,13787935,13789081,13789081,13789423,13789423,13748314,13748314,13720591,13725355,13729891,13729891,13730035,13730035,13730659,13730659,13731799,13735990,13731889,13738543,13738543,13738882,13738882,13740820,13741252,13741501,13746094,13746094,13747657,13747657,13749607,13749805,13750663,13749604,13753810,13753810,13698919,13755427,13755427,13757479,13757479,13759912,13760065,13760551,13760551,13760965,13760965,13760815,13763917,13763185,13763185,13698946,13769320,13769320,13770658,13770658,13772245,13773871,13773871,13775239,13775239,13775248,13775248,13775251,13775251,13777231,13777402,13777402,13777915,13777915,13778221,13778221,13702468,13779307,13779307,13779475,13779295,13779295,13779616,13779694,13779691,13779757,13779757,13779769,13779553,13780213,13780213,13780282,13780282,13780288,13780303,13780303,13780264,13780264,13780261,13780261,13780342,13780387,13780396,13780414,13780540,13780627,13780642,13780780,13780813,13780891,13780894,13780912,13780924,13780771,13780771,13780921,13780942,13780942,13780798,13780999,13780999,13781014,13781044,13781071,13781071,13781107,13781116,13781116,13781134,13781134,13781110,13781110,13781140,13702471,13781728,13781869,13781983,13781983,13782010,13782007,13782007,13782109,13781971,13781971,13781968,13781968,13782118,13782049,13782130,13782148,13782208,13782280,13782523,13782544,13782544,13782547,13782538,13782514,13782631,13782676,13782721,13782739,13782769,13782769,13782799,13782829,13782829,13782826,13782826,13782952,13782958,13782958,13782955,13782955,13782994,13782994,13782949,13782949,13783213,13783387,13783399,13783450,13783600,13783585,13783585,13783597,13783597,13783588,13783588,13783594,13783594,13783591,13783591,13783684,13783747,13783747,13783750,13783789,13783807,13783813,13783813,13779775,13784794,13784833,13784833,13784839,13784851,13784851,13784857,13784857,13784881,13784881,13784899,13784923,13785034,13785142,13785166,13785175,13785193,13785208,13785217,13784830,13784830,13785259,13785283,13785304,13785322,13785349,13783402,13785385,13785247,13785388,13785424,13785463,13785472,13785475,13785475,13785493,13785595,13785595,13785598,13785598,13785589,13785589,13785592,13785592,13785586,13785586,13785661,13785661,13785682,13785685,13785685,13785742,13785784,13785826,13785835,13785853,13784812,13784812,13786123,13786138,13786141,13785490,13785490,13785415,13785415,13786156,13786162,13786216,13786261,13786264,13786303,13786303,13786306,13786306,13786309,13786312,13786312,13786315,13786315,13786324,13786324,13786357,13786372,13786372,13786405,13786420,13786420,13786381,13786381,13786384,13786384,13786441,13702477,13787062,13787125,13787125,13787128,13787128,13787140,13787122,13787122,13787164,13787164,13787239,13787275,13787308,13787308,13787386,13786180,13787470,13787479,13787509,13787482,13787635,13787668,13787689,13787722,13787731,13787146,13787146,13787137,13787137,13787806,13787863,13787863,13787860,13787860,13787650,13787857,13787857,13787854,13787854,13788025,13788043,13788046,13788046,13787959,13787959,13788142,13788157,13788157,13788175,13788184,13788205,13788205,13788292,13788301,13785052,13785058,13788307,13788345,13788351,13788363,13788391,13788517,13788565,13788886,13788886,13788955,13788955,13788967,13788967,13788973,13789027,13789036,13789036,13789051,13789072,13702483,13788259,13789891,13790098,13790167,13788367,13790176,13789981,13789981,13789978,13789978,13789975,13789975,13789972,13789972,13790383,13790452,13790578,13790578,13790620,13790635,13790635,13790623,13790623,13790614,13790614,13790617,13790617,13786243,13790815,13790815,13790836,13782823,13788442,13782781,13790983,13790983,13791040,13791040,13791061,13790971,13790971,13791091,13791121,13791121,13791112,13791112,13791115,13791115,13788316,13786363,13791745,13787539,13791763,13791763,13787398,13791766,13791766,13791916,13791916,13791919,13791919,13791949,13791949,13787569,13791979,13788439,13792303,13792303,13792297,13792297,13792306,13792306,13792402,13792432,13792432,13792423,13792423,13788118,13787548,13792429,13792429,13788673,13792825,13792825,13792939,13792936,13792936,13793050,13793101,13793101,13787680,13793113,13793113,13793110,13793110,13793107,13793107,13784905,13787617,12655338]

	lista_ripasso_febbraio = [13797562,13797562,13785904,13785904,13802038,13802038,13804747,13804747,13804930,13804930,13805044,13805044,13805107,13805107,13805143,13805143,13806754,13806754,13806862,13806862,13801720,13816786,13816786,13816843,13816843,13819378,13819378,13819402,13819402,13821322,13821322,13822570,13822570,13828120,13828120,13829746,13829758,13829785,13829962,13829962,13830190,13830190,13830199,13830199,13836031,13836031,13836061,13836061,13836067,13836067,13836085,13836085,13855687,13855687,13793698,13793764,13793764,13793770,13793779,13793779,13793794,13793797,13793797,13793857,13793866,13793887,13793893,13793890,13793926,13793959,13793980,13794022,13794028,13794040,13794064,13794133,13794241,13787620,13794337,13794334,13794331,13794328,13787557,13794448,13794448,13794229,13794478,13794595,13794718,13794718,13794715,13794715,13794607,13793791,13793791,13793788,13793788,13794751,13794751,13788475,13794781,13794781,13794805,13794844,13794880,13794877,13794874,13794862,13794856,13794961,13797511,13797565,13797568,13797568,13797607,13797658,13797658,13797664,13797664,13797694,13797694,13797715,13797718,13797718,13797721,13797721,13797781,13797781,13797784,13798009,13798015,13798015,13798012,13785973,13798294,13798351,13798351,13798405,13798420,13798489,13794151,13798546,13798609,13798603,13798318,13798318,13798321,13798321,13798693,13798693,13798690,13798840,13798873,13798873,13798861,13798870,13798870,13798921,13798909,13794154,13798924,13798939,13798936,13787656,13794814,13799026,13799044,13799044,13799047,13799047,13799050,13799050,13799071,13799071,13799131,13799131,13799176,13799179,13799179,13799182,13799143,13799143,13799215,13788484,13799314,13799314,13799428,13783717,13783717,13799494,13799542,13799650,13799743,13799752,13799737,13799785,13799785,13799788,13799788,13799890,13799866,13799866,13799947,13799947,13799971,13799977,13799977,13799968,13799968,13799962,13799962,13799965,13799965,13800010,13800013,13800013,13800109,13785982,13794340,13800607,13800676,13800676,13800679,13800679,13800709,13800709,13800748,13800748,13800751,13800751,13800754,13800775,13800805,13800805,13794013,13800895,13800916,13800931,13800949,13800967,13800979,13800997,13801015,13801033,13801039,13801054,13787791,13787692,13800688,13800688,13800964,13801270,13801270,13801255,13801267,13801267,13801264,13801264,13801378,13801381,13801381,13801258,13801258,13801426,13801426,13801441,13788496,13801456,13801693,13801723,13801771,13801864,13801864,13801861,13801861,13801933,13799764,13799764,13802002,13802002,13802035,13802059,13802059,13802074,13802098,13802098,13801261,13801261,13802131,13802131,13802146,13802146,13802149,13802149,13802152,13802212,13802212,13802218,13802218,13802233,13802242,13802242,13802245,13802245,13802248,13802248,13802251,13802254,13802254,13802263,13792003,13785988,13798996,13802845,13802899,13802899,13802953,13802962,13802989,13803013,13801714,13803091,13803118,13803106,13803145,13803157,13803184,13803205,13803211,13803235,13802938,13802938,13802932,13802932,13803469,13803469,13803511,13801645,13803508,13803550,13803550,13800592,13800592,13803577,13803577,13794196,13803613,13803070,13801087,13803589,13803589,13803595,13803595,13803781,13803784,13803784,13803793,13803793,13803796,13788499,13803913,13804036,13804015,13804015,13803778,13803778,13804129,13804099,13804255,13804309,13804306,13804324,13804399,13804399,13804474,13804474,13804396,13804396,13804495,13804495,13804519,13804519,13804561,13804561,13804564,13804567,13804570,13804558,13804558,13804582,13804597,13804600,13804600,13804615,13804615,13804612,13804612,13804639,13794820,13786414,13794967,13786003,13801108,13805239,13805299,13805299,13805308,13805308,13805362,13805395,13805314,13805314,13805428,13805431,13805800,13805806,13805824,13805827,13805842,13805842,13805851,13794202,13805887,13805896,13805317,13805317,13806004,13806058,13806058,13806139,13806202,13806223,13806223,13806385,13806388,13806388,13806340,13806136,13806136,13806439,13806439,13806442,13806442,13806601,13806601,13806433,13806604,13806586,13806586,13805371,13806628,13806634,13788505,13806802,13806883,13803697,13803700,13806949,13807006,13807051,13807243,13807333,13807381,13807381,13807432,13807441,13807441,13807456,13807456,13807504,13807552,13807576,13807576,13807567,13807567,13807570,13807570,13807591,13807606,13798903,13786006,13806286,13808050,13808101,13808101,13808113,13808113,13808251,13808251,13806412,13808092,13808092,13808089,13808089,13808389,13808446,13808509,13808629,13808653,13808653,13808677,13808677,13799407,13808887,13806280,13809046,13809046,13809151,13809151,13809172,13809172,13809178,13809178,13809184,13809190,13809193,13809193,13809238,13809238,13806100,13809790,13805872,13806937,13810000,13810000,13809985,13809985,13810036,13810036,13810033,13810033,13810051,13810051,13810060,13810060,13805884,13810081,13810135,13810141,13810141,13805890,13810321,13810342,13810342,13805407,13805905,13810585,13810585,13810810,13810810,13810882,13807303,13803472,13811023,13811023,13805908,13803967,13811383,13811383,13811431,13811431,13811392,13811392,13811395,13811395,13811389,13811389,13806061,13803142,13811470,13811533,13811542,13811542,13811548,13811548,13811581,13811581,13811614,13811647,13811659,13811659,13811692,13811722,13811713,13811731,13811695,13811710,13811791,13804006,13811824,13811839,13806367,13811884,13811884,13811893,13811926,13811926,13811956,13811956,13811980,13811980,13812031,13812031,13805938,13812145,13812316,13805947,13805989,13812436,13812481,13812481,13812535,13812505,13812538,13812607,13812586,13812661,13812670,13812670,13812667,13812673,13812673,13812733,13788541,13812805,13812862,13812868,13812997,13812994,13813003,13813069,13813153,13814575,13814575,13814623,13814623,13814626,13814626,13814635,13814632,13814638,13814638,13814647,13814671,13814662,13814662,13814674,13814674,13814827,13814827,13803247,13814956,13815052,13801954,13803148,13815544,13815628,13815628,13815637,13815637,13815652,13815655,13815655,13815682,13815682,13815721,13815724,13815727,13815730,13815769,13813825,13815784,13815811,13815820,13815817,13811872,13815892,13815964,13815967,13816054,13816054,13816051,13816051,13816063,13816075,13815691,13815691,13806703,13816240,13806016,13816246,13816285,13816300,13816276,13816411,13816426,13816426,13788547,13816501,13816684,13816327,13816327,13816714,13816816,13816816,13816858,13816861,13816924,13816951,13817026,13817077,13817140,13817254,13817254,13817221,13817221,13817272,13817320,13817320,13817356,13817323,13817323,13817326,13817326,13817407,13817407,13817404,13817422,13803250,13801957,13817185,13818064,13818202,13818202,13818214,13818223,13818223,13818226,13818226,13818232,13818232,13818247,13818247,13818268,13818271,13818280,13818289,13818295,13818331,13818388,13818445,13818445,13818463,13818475,13818481,13818250,13818250,13818244,13818244,13818550,13818697,13806031,13818784,13818847,13818847,13818844,13818844,13811938,13818883,13818883,13818886,13818886,13818565,13818976,13819093,13819132,13819132,13788553,13819165,13819195,13819213,13819210,13819207,13819204,13819198,13819201,13819246,13818925,13818925,13818922,13818922,13818919,13818919,13819558,13819570,13819570,13819564,13819648,13819666,13819699,13819699,13819702,13819735,13819867,13819891,13819915,13819915,13819921,13819921,13819930,13819930,13819945,13819963,13819963,13819999,13820014,13820014,13820026,13803253,13801960,13818706,13819774,13820701,13820818,13820818,13820842,13820845,13820845,13820848,13820848,13820863,13820863,13820899,13820914,13820935,13820962,13820962,13820920,13820920,13820923,13820923,13820926,13820926,13820998,13821013,13821019,13821064,13818901,13821112,13821223,13821358,13821271,13819126,13821388,13819153,13821457,13821496,13821496,13821493,13821493,13821487,13821487,13821484,13821484,13821667,13821691,13821691,13821757,13821769,13788556,13821982,13821982,13822150,13822180,13822246,13822327,13822366,13821664,13821664,13821661,13821661,13821679,13821679,13822456,13822465,13822486,13822498,13822507,13822585,13822585,13822624,13822627,13822639,13822648,13822648,13822663,13822690,13822693,13822693,13803256,13822699,13822699,13823302,13823302,13801963,13823374,13823404,13823404,13823428,13823446,13823446,13823494,13823506,13823503,13823497,13823509,13823554,13823581,13822444,13823677,13823680,13823698,13823698,13823833,13823983,13824052,13824052,13824082,13824082,13824145,13823929,13823986,13824325,13824325,13824382,13824394,13824394,13824427,13824427,13824451,13788562,13824499,13823671,13824532,13824565,13817710,13824676,13824676,13824697,13824373,13824373,13824367,13824367,13824787,13824544,13824853,13824880,13824553,13824889,13824937,13825096,13825183,13825183,13825204,13825204,13825210,13825054,13825054,13825228,13824850,13824850,13825246,13825246,13825249,13825267,13803259,13819225,13825918,13801966,13825957,13824802,13826026,13826149,13826149,13825123,13826275,13826275,13824820,13826134,13826134,13826128,13826128,13826458,13826470,13826470,13826467,13826467,13826473,13826473,13826569,13826569,13826584,13826617,13826617,13826698,13826620,13826620,13826623,13826623,13826626,13826626,13823491,13818979,13826914,13826914,13827007,13827007,13827010,13827010,13827031,13826884,13826884,13827037,13827037,13827046,13826899,13826899,13827049,13826119,13826119,13825951,13827070,13827070,13821481,13827616,13827616,13824559,13827622,13827649,13827649,13823479,13821925,13827868,13827868,13827913,13827913,13827919,13821085,13827931,13827931,13821079,13828147,13828147,13828237,13828264,13828264,13817830,13828339,13828339,13828015,13828015,13828021,13828021,13828024,13828024,13828027,13828027,13818664,13828579,13828579,13828582,13828582,13828849,13828849,13828906,13828978,13829005,13829005,13818703,13829383,13829383,13808980,13829506,13829548,13829548,13829560,13829560,13829575,13829617,13829617,13829566,13829566,13829572,13829572,13829761,13829776,13829794,13829839,13829875,13829887,13829884,13829920,13829923,13806379,13830235,13830253,13830262,13830277,13830415,13830415,13830421,13818841,13830487,13830577,13830472,13830709,13830712,13830712,13830715,13830715,13830718,13830718,13830739,13830739,13830733,13830733,13830775,13830757,13830760,13830796,13830796,13830793,13830793,13830802,13788613,13830838,13830544,13830859,13830862,13830865,13830868,13831117,13831192,13831315,13831513,13831513,13831528,13831579,13831579,13831618,13831621,13831621,13831624,13831624,13831627,13831627,13831672,13831681,13831681,13831675,13831675,13831699,13803304,13831765,13832236,13832236,13821124,13832266,13831249,13832305,13832332,13832332,13832365,13832365,13832410,13832407,13832464,13832506,13832584,13832599,13832482,13832482,13832485,13832485,13832536,13832809,13832812,13832845,13832896,13833022,13833022,13833031,13833016,13833145,13833157,13830382,13833217,13833217,13833244,13833244,13833262,13833286,13833286,13833064,13833316,13833316,13833148,13833148,13833388,13833388,13833385,13833385,13833445,13833481,13833481,13833547,13788616,13834084,13834084,13833790,13834105,13834117,13834108,13834132,13834111,13834120,13834114,13834309,13834531,13834543,13834603,13834666,13834726,13834462,13834462,13834738,13834759,13834759,13834765,13832869,13803307,13834864,13834864,13835395,13835395,13821127,13831351,13835539,13835653,13835656,13835659,13835668,13835668,13835671,13835716,13835725,13835725,13835593,13835593,13835779,13835821,13835815,13835833,13835830,13835824,13830439,13835596,13835596,13835962,13835962,13835965,13835965,13835980,13836130,13836142,13818880,13836178,13836178,13836349,13836349,13836364,13836550,13836556,13836556,13836574,13836568,13836568,13836583,13836562,13836562,13788622,13836868,13836868,13836934,13836934,13836946,13837024,13822198,13837084,13837183,13837207,13837207,13837222,13837225,13837225,13837447,13837555,13837555,13837579,13837579,13837576,13837576,13837570,13837570,13837588,13837588,13837603,13837603,13837639,13837615,13837615,13837702,13837771,13837774,13837774,13803316,13816963,13835644,13838281,13838281,13838437,13838437,13821133,13838530,13838581,13838617,13838617,13838638,13838635,13838659,13838671,13838590,13838590,13838602,13838602,13838593,13838593,13838596,13838596,13838587,13838587,13838707,13838707,13838716,13838716,13838749,13838701,13835872,13838764,13838779,13838788,13838794,13838824,13838854,13838965,13838965,13839004,13839112,13839214,13839283,13839283,13839286,13839286,13839298,13839304,13836682,13839331,13839331,13839355,13836685,13839424,13839316,13839421,13839421,13839436,13839436,13839553,13839556,13839556,13839559,13839559,13839550,13839550,13839547,13839547,13788625,13839802,13839841,13839859,13839922,13840078,13840123,13840201,13840201,13840210,13840210,13840213,13840213,13840285,13840285,13840303,13840318,13840336,13840336,13840369,13840399,13840393,13840393,13840420,13840420,13840414,13840414,13840417,13840417,13840423,13840423,13840426,13840426,13840432,13840432,13840486,13840486,13840324,13840492,13840501,13840501,13840516,13840510,13840537,13840537,13840543,13840543,13840546,13840555,13840555,13840573,13839310,13803319,13804027,13821181,13839397,13841137,13841176,13841176,13841179,13841179,13841209,13841215,13841215,13841266,13841278,13841290,13841317,13841317,13841323,13841383,13841386,13841254,13841254,13841404,13841407,13841257,13841257,13841413,13841422,13841422,13830493,13841575,13841725,13841725,13841737,13841737,13841740,13841755,13841833,13841950,13841998,13842043,13842043,13842046,13842046,13841992,13841992,13842070,13841701,13842118,13842118,13842295,13842295,13842298,13842304,13842310,13842331,13788628,13842340,13842382,13842535,13842610,13842661,13842658,13842907,13842922,13842928,13842928,13843075,13843003,13843003,13843048,13843048,13843141,13843141,13843153,13843174,13843174,13843180,13843180,13843177,13843177,13843186,13843186,13843189,13843210,13843222,13843222,13843237,13803322,13836709,13843213,13843213,13843219,13843219,13843723,13843723,13821184,13843876,13842604,13843879,13843960,13843960,13843984,13843984,13843963,13843963,13841977,13843039,13843957,13843957,13843948,13843948,13843954,13843954,13844401,13844515,13844533,13844533,13844410,13844410,13844563,13844614,13835854,13841824,13841827,13841830,13841821,13841989,13842247,13842370,13844983,13844983,13844986,13844986,13845001,13845043,13845061,13845061,13845412,13845412,13843030,13845514,13845514,13845694,13842064,13845703,13841437,13841236,13845988,13845988,13821091,13846057,13846069,13846069,13846072,13846072,13846078,13846078,13846084,13846084,13846291,13846315,13846315,13846090,13846090,13846327,13846327,13808884,13835845,13836736,13846615,13846615,13818667,13846837,13846837,13846846,13846882,13846888,13846888,13846900,13846900,13846897,13846897,13846903,13846903,13839775,13847347,13847347,13847350,13847350,13847377,13847377,13834522,13808971,13847422,13847476,13847476,13847482,13847485,13847485,13847515,13847527,13847899,13839082,13847917,13847950,13847977,13847983,13848028,13848034,13848049,13848088,13848082,13848079,13848076,13848073,13848097,13848085,13848100,13848103,13848124,13848154,13848184,13848196,13848181,13848187,13848193,13848175,13847488,13847488,13848004,13847491,13847491,13848274,13848310,13848310,13848301,13848301,13848313,13842049,13848358,13848373,13847494,13847494,13848535,13848541,13848541,13848550,13788685,13848718,13848796,13849252,13851247,13851337,13851367,13851367,13851382,13851460,13851463,13851463,13848403,13848403,13851580,13851580,13851586,13851586,13851595,13851628,13851628,13851625,13851655,13851655,13851661,13803394,13851910,13852228,13852228,13852321,13852321,13852438,13852438,13837048,13848319,13852489,13852486,13852543,13852543,13852549,13852558,13852558,13852564,13852564,13852522,13852522,13852573,13852573,13852591,13852597,13852609,13852609,13852612,13852621,13852531,13852531,13852657,13852666,13852666,13839088,13852675,13852699,13852717,13852744,13852846,13852936,13848604,13836703,13853038,13852639,13853173,13853173,13853179,13853212,13853212,13853023,13853023,13853026,13853026,13853221,13853221,13853227,13788688,13853347,13853347,13853431,13853452,13853452,13853488,13853494,13853521,13853515,13853536,13853644,13853647,13853668,13853725,13853725,13853752,13853821,13853821,13853746,13853746,13853842,13853875,13853875,13853872,13853914,13853926,13853926,13852888,13803397,13854376,13854376,13837051,13853470,13854586,13854607,13854607,13854613,13854613,13854646,13854667,13854667,13854715,13854748,13854841,13839094,13854859,13854868,13854868,13854886,13854931,13855009,13855021,13855039,13854958,13837072,13855096,13855096,13839760,13855135,13855069,13855156,13855156,13855468,13855486,13855486,13788691,13855555,13855606,13855753,13855774,13855822,13855837,13855933,13855999,13856011,13856032,13856113,13856125,13856245,13856245,13856251,13856356,13856398,13856401,13856401,13856452,13855645,13803400,13842019,13856842,13856842,13837054,13856137,13857229,13857301,13857301,13857307,13857313,13857313,13857355,13857361,13857361,13857364,13857364,13857409,13857421,13857436,13857451,13857460,13839097,13857559,13857649,13855489,13857775,13857841,13857844,13858030,13858039,13858039,13857283,13857283,13857280,13857280,13858099,13788694,13858375,13858405,13858570,13858576,13858585,13858585,13858579,13858762,13858798,13858798,13858807,13858765,13858822,13858849,13858849,13858855,13858072,13803403,13821772,13859224,13859224,13859221,13859221,13859302,13837057,13859428,13859503,13859515,13859515,13859518,13859518,13859581,13859530,13859530,13859527,13859527,13859695,13839103,13859722,13859722,13859755,13859788,13859767,13859827,13859848,13859848,13859854,13859854,13860025,13860070,13860208,13860442,13860442,13860520,13860544,13860544,13860529,13860556,13860556,13860604,13860610,13860619,13860643,13860643,13788700,13860613,13859806,13860895,13860946,13861012,13861108,13861108,13861090,13861102,13861105,13861129,13861147,13861138,13861198,13861204,13861249,13861255,13861342,13861342,13861267,13861354,13861363,13861447,13861447,13861444,13861444,13861531,13861543,13861543,13861564,13861456,13861456,13861609,13861609,13861606,13861657,13803406,13861933,13837099,13862323,13862323,13862566,13837060,13861603,13861603,13861594,13861594,13861588,13861588,13861201,13862623,13862704,13862704,13844338,13862782,13862782,13861462,13863019,13863019,13862650,13862650,13862641,13862641,13862647,13862647,13863541,13863721,13863739,13863739,13863586,13863586,13863589,13863589,13863994,13863994,13863991,13863991,13864027,13859878,13864132,13864132,13864159,13864159,13859863,13857625,13864180,13864180,13864732,13864765,13864906,13864906,13855921,13865251,13865251,13865326,13865326,13865404,13865467,13865467,13860004,13865524,13865524,13859839,13865623,13865623,13865656,13865656,13865671,13865695,13865695,13865710,13865710,13859920,13865761,13865761,13865560,13865560,13859971,13865842,13865842,13865965,13865965,13865995,13836847,13858306,13866079,13866079,13859899,13865887,13865887,13865890,13865890,13866274,13866274,13866277,13866277,13818670,13866358,13866358,13866475,13866478,13866478,13866523,13866574,13866574,13867042,13867042,13808977,13812481,299134,3655351,10604508]

	listaDaFare = lista_ripasso_novembre
	listaDaFare = lista_ripasso_dicembre
	listaDaFare = lista_ripasso_gennaio

	lista_ripasso_marzo = [13867357,13867357,13871401,13871401,13855798,13855771,13874584,13874584,13880344,13880344,13884643,13884643,13887001,13887025,13887046,13887061,13887094,13887112,13887247,13887247,13887553,13887553,13896316,13896316,13896370,13896370,13896802,13896802,13898683,13898683,13902160,13902160,13902325,13902325,13903645,13903645,13911901,13911901,13919104,13919104,13938865,13938865,13948543,13948543,13867084,13867084,13867087,13867096,13867096,13867126,13867126,13867144,13867144,13867168,13867177,13867177,13867183,13867186,13867189,13867213,13867270,13867270,13867267,13867267,13867264,13867264,13867273,13867273,13867324,13867324,13867342,13859872,13867423,13867441,13867474,13867486,13867513,13867531,13867531,13867534,13867537,13867552,13867558,13867561,13867579,13867576,13867570,13867588,13867597,13867600,13867603,13867606,13867609,13867636,13867714,13867723,13867525,13860118,13867828,13867828,13867825,13867855,13867855,13867870,13867546,13867936,13867936,13867939,13867939,13868032,13868032,13868044,13868044,13868047,13868050,13868050,13868053,13868071,13868101,13868101,13788745,13868143,13868155,13868173,13868200,13868200,13868197,13868203,13868203,13868305,13868305,13868350,13868344,13868377,13869319,13869778,13867924,13867924,13867921,13867921,13867918,13867918,13869886,13869877,13869877,13869940,13869940,13869949,13869961,13869961,13869952,13869952,13869973,13869973,13869976,13869979,13869979,13869997,13816873,13870114,13857727,13861303,13870432,13870492,13870495,13870495,13870519,13870519,13870522,13870528,13870528,13870540,13870546,13870567,13870570,13870579,13870603,13870534,13870534,13870633,13870636,13870642,13870660,13870666,13870666,13870681,13860160,13870705,13871140,13868149,13871464,13871458,13871458,13867654,13871512,13871512,13871518,13871518,13860151,13860154,13871632,13871635,13871647,13871455,13871827,13871836,13871839,13871839,13871671,13871671,13788748,13872079,13872079,13872091,13872091,13872121,13872160,13872154,13872337,13872349,13872484,13872484,13872487,13872487,13872490,13872490,13872571,13872568,13872586,13872586,13872493,13872493,13872514,13872514,13872598,13872625,13872625,13872643,13872652,13872652,13872676,13871995,13816900,13822153,13873396,13857745,13873459,13873555,13873609,13873609,13873603,13873603,13873624,13873636,13873513,13873513,13873510,13873510,13873522,13873522,13873507,13873507,13873684,13873699,13873702,13873735,13873774,13873798,13873810,13873810,13860178,13873816,13873762,13873822,13873822,13873942,13873942,13873999,13873999,13874020,13874026,13874065,13860124,13874089,13868311,13874191,13874149,13874227,13874227,13874221,13874221,13874206,13874206,13874248,13874248,13874272,13874272,13874275,13874275,13874380,13874377,13874332,13874332,13874335,13874335,13874401,13788754,13874662,13874662,13874698,13874719,13874746,13861003,13874884,13874896,13874896,13875013,13875013,13875061,13875058,13875085,13875085,13875055,13875079,13875079,13875076,13875076,13875091,13875082,13875082,13875112,13875112,13875124,13875124,13875100,13875100,13875103,13875103,13875133,13875139,13875139,13875142,13874014,13816903,13875616,13875616,13861378,13857796,13874779,13876129,13876129,13876138,13876159,13876159,13876171,13876189,13876195,13876210,13876099,13876099,13876105,13876105,13876231,13876252,13876291,13876336,13876336,13876339,13876270,13876399,13876399,13876423,13802968,13876444,13876471,13876471,13876534,13874086,13876738,13876738,13876735,13876699,13876333,13876282,13876843,13876882,13876882,13876888,13876888,13876930,13876930,13877026,13877029,13877029,13877035,13877035,13877056,13788766,13877098,13877332,13877332,13877371,13877428,13877437,13877467,13877533,13877578,13877662,13877695,13877695,13877617,13877617,13877713,13877713,13877779,13877776,13877776,13877863,13877878,13877902,13877902,13816906,13822162,13861285,13857802,13878784,13878784,13878829,13878829,13878844,13878844,13878862,13878865,13878892,13878901,13878913,13878931,13878937,13878934,13878961,13868332,13878805,13878805,13878808,13878808,13878799,13878799,13879051,13879129,13879177,13879180,13879162,13879300,13879300,13879342,13879381,13879366,13879387,13879387,13879240,13879504,13879528,13879528,13879249,13879249,13879261,13879261,13879246,13879246,13879252,13879252,13879618,13879618,13879615,13879615,13879630,13879633,13879633,13879627,13879669,13879255,13879747,13879753,13878922,13878925,13879930,13879948,13880041,13880125,13880155,13880155,13880215,13880311,13880470,13880470,13880473,13880473,13880482,13880482,13880491,13880491,13880488,13880488,13880485,13880485,13880560,13880572,13880572,13880602,13880602,13880596,13880596,13880605,13880611,13880611,13880599,13880599,13880644,13880647,13816912,13822186,13877446,13881223,13881223,13878667,13881562,13857811,13879879,13881571,13881628,13881628,13881625,13881625,13881622,13881622,13881652,13881652,13880305,13879003,13882003,13882003,13882384,13882432,13882432,13882435,13882435,13882441,13882441,13882627,13882657,13882657,13868341,13882840,13877257,13877293,13877164,13877236,13877242,13882990,13882990,13880083,13883011,13883011,13883044,13883044,13878919,13883176,13883176,13883206,13883218,13883221,13883221,13883245,13883245,13879483,13882618,13882618,13882621,13882621,13882624,13882624,13883485,13883485,13883491,13883491,13883488,13883488,13883914,13883962,13883962,13878952,13883971,13883971,13878916,13883959,13883959,13884262,13884262,13884253,13884253,13884274,13884274,13884244,13884244,13884247,13884247,13884277,13884241,13884241,13884448,13884448,13879072,13884616,13884634,13884634,13884685,13884685,13884526,13884526,13884520,13884520,13873915,13818763,13885111,13885120,13885120,13885123,13885123,13876783,13885159,13885159,13885186,13885192,13885192,13885396,13885402,13885429,13885429,13885426,13885426,13885420,13885420,13885423,13885423,13885792,13885792,13885960,13808983,13886041,13886113,13886119,13886119,13886152,13886152,13886191,13886299,13886311,13879789,13886386,13886410,13886389,13886440,13886440,13886437,13886128,13886128,13886134,13886134,13886131,13886131,13886464,13886473,13886476,13886551,13886560,13886581,13886602,13886614,13874533,13886731,13886731,13886752,13886752,13879912,13886782,13886800,13886983,13886986,13886986,13886980,13886980,13886977,13886977,13879267,13887127,13887124,13886755,13887172,13887409,13887469,13887367,13887370,13816891,13874704,13887415,13879891,13876594,13886662,13877077,13890655,13890655,13879270,13887406,13887394,13816933,13874707,13886482,13892881,13892881,13877521,13877083,13893406,13879273,13816936,13874710,13895017,13886485,13879276,13896442,13896442,13896778,13896778,13816939,13822195,13877464,13874713,13897927,13879279,13816945,13874716,13900930,13900930,13854697,13818766,13879144,13907767,13907767,13816954,13908529,13908529,13909888,13822222,13816960,13911313,13912060,13912060,13912309,13912546,13912546,13913755,13913755,13816969,13877473,13816978,13854694,13818769,13923619,13923619,13925965,13925965,13821904,13821907,13930174,13930174,13821910,13933264,13821937,13821940,13937167,13937167,13938067,13938067,13874446,13939252,13939252,13818772,13941271,13941274,13941274,11595820]

	lista_ripasso_maggio = [14035690,14036485,14037001,14037979,14038000,14037994,14037991,14038309,14038318,14039128,14042755,14042899,14043721,14053060,14054287,14058424,14067928,14074657,14085076,14085082,14091409,14110597,14112433,14113501,14113717,14113750,14115748,14116351,14037973,14037688,14039932,14044120,14045194,14047975,14048740,14051608,14007295,14053273,14059630,14059405,14062627,14062633,14062666,14062693,14062708,14064364,14067118,14068321,14072680,14074501,14076910,14007739,14078458,14079502,14007745,14082589,14084485,14007748,14086972,14095765,14063803,14099293,14100691,14101228,14101243,14087839,14106190,14106649,14109295,14109352,14110654,14114041,13443553,14115382,14115571]

	lista_ripasso_giugno = [14116759,14117374,14117737,14129040,14129097,14147034,14170854,14180496,14180205,14180379,14180163,14184486,14189343,14118544,14120179,14123004,14087860,14113819,14126019,14124534,14129301,14136219,14132496,14139345,14142576,13803328,14143305,14142426,14144778,14147655,14152293,14132109,14116759,14117374,14117737,14129040,14129097,14147034,14170854,14180496,14180205,14180379,14180163,14184486,14189343,14118544,14120179,14123004,14087860,14113819,14126019,14124534,14129301,14136219,14132496,14139345,14142576,13803328,14143305,14142426,14144778,14147655,14152293,14132109,14155728,14152065,14132112,14132115,14162982,14152071,14132124,14175180,14175567,14132454,14175552,14177520,14177403,14177499,14180724,14181639,14181855,14182182,14182677,14182941,14185260,14185758,14189445,14188449,14190150,14132532,14194398,14195439,14132535,14175567]

	listaDaFare = []
	print len(lista_ripasso_giugno)
	for lis in lista_ripasso_giugno:
		if not lis in listaDaFare:
			listaDaFare.append(lis)
	print len(listaDaFare)

	countFiles = 1
	for audio in listaDaFare:
		logger.debug( '- ' + str(countFiles) + '/' + str(len(listaDaFare)) + ' --> ' +  str(audio))
		countFiles +=1
		Update( str(audio ))
		

	print '------------ END ---------- pyRipassoDuration.py ---------------------'
